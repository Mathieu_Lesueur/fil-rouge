/**
 * @file main_program.c
 * @author Ngoma-Mby Charvy (charvy.ngoma-mby@univ-tlse3.fr)
 * @brief  
 * @version 0.1
 * @date 31/01/2019
 * 
 * @copyright Copyright (c) 2019
 * Description : Permet l'affichage de l'interface pour l'utilisation du moteur de recherche
 */

#include <stdio.h>
#include <string.h>
#include  <stdlib.h>
#include  <libgen.h>

#include "./_OUTILS/affichage/outils_affichage.h"
#include "./_OUTILS/autre/element.h"
#include "./_OUTILS/autre/sorted_list.h"
#include "./_OUTILS/autre/ouverture.h"

#include "./TEXTE/Texte.h"
#include "./IMAGE/images.h"
#include "./SON/indexion.h"


#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define RESET "\x1B[0m"

char NOM_FICHIERS_REPERTOIRE_PATH[255];


/**
 * [Charvy]
 * @param s : char* 
 * verifie si la chaine contient un entier, retourne 1 si oui -1 sinon 
*/ 
int verifier(char* s)
{
	int t=1,i;
    // affecte -1 à la variable t si le code ascii d"un charactère de la chaîne n'est pas un entier
	for(i=0;s[i]!='\0';i++){
		if((s[i]>='a')&&(s[i]<='z')||(s[i]>='A')&&(s[i]<='Z')||!(s[i]>47&&s[i]<58))
			t=-1;
	}
	return t;
}

/**
 * [Charvy]
 * @param void
 * Description : Demande une saisie clavier à l'utilisateur. Permet de réduire la taille du code et d'éviter que le porgramme ne plante si l'utilisateur entre autre chose qu'un entier  
*/ 
int saisir(void){
    char s[255];
	do{
        printf("|");
		printf("\n'-> ");
		scanf("%s",s);
	}while(verifier(s)==-1); 

    return atoi(s);
}


/** 
 * [Charvy]
 * @param void
 * Description : Demande a l'utilisateur de saisir un chiffre pour continuer le programme. Bloque l'execution
 * 
 */
void continuer(void)
{
		int x;
		printf("\n Tappez "BOLDGREEN" 0  "RESET"pour continuer \n");
		fflush(stdout);
		fflush(stdin);
		saisir();
}




/**
 * [Charvy]
 * 
 * @param void
 * Description : Fenetre de log-in en mode utilisateur ou en mode user
 * retourne 1 si le mode choisi est admin
 * retourne 0 si le mode choisi est utilisateur
 */
int log_in(void)
{

    	strcpy(NOM_FICHIERS_REPERTOIRE_PATH,".");
	    strcat(NOM_FICHIERS_REPERTOIRE_PATH,"/nom_fichiers_du_repertoire_Textes.txt");

        int x;
        do
        {
            char *options[3] = {"USER", "ADMIN","QUITER"};
            afficher_options("FIL ROUGE","",50,options,3);
            
            x = saisir();

            if(x < 0 || x>=4 )
            {
                afficher_options("ERREUR","La valeur doit etre dans [0;2] ",50,NULL,3);
                continuer();
            }


        }while(x < 0 || x>=4);
        int nb_essais = 3;
        char chemin[200];
        int valide;
        switch (x)
        {
            case 0:
                return 0;
                break;
            case 1:
                do
                {
                    afficher_options("FIL ROUGE","Saisir le code qui est [1234]",50,NULL,0);
                    
                    printf("-> ");
                    scanf("%s",chemin);

                    valide = strcmp(chemin,"1234"); // hack proof 

                    if( 0!=valide)
                    {
                        afficher_options("ERREUR","Mauvais code, le code est 1234",50,NULL,3);
                        continuer();
                    }
                    nb_essais --;

                }while( 0!=valide && nb_essais!=0 );

                if(nb_essais ==0){return 0;} 

                return 1;
                break;
            default:
                exit(0);
                break;
        }
}

/**
 * [ALGO]
 * 
 * indexation de tout un repertoire
 * appelle la fonction indexation sur tous le repertoire
 */
int indexer_repertoire_Image(char* chemin_repertoire,int nb_bits_quant)
{
    // On recupere le nombre de fichiers qui correspondent 
	char commande[1000];
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande,"/*.txt");
	strcat(commande," | wc -l > ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH); // on save ca dans le fichier NOM_FICHIERS_REPERTOIRE_PATH
	system(commande);

    // Puis on recupere leurs chemins
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande,"/*.txt");
	strcat(commande,">> ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH); // on save ca a la suite 
	system(commande);

    //on ouvre le fichier on l'on a stocker tout ca
	FILE* f=fopen(NOM_FICHIERS_REPERTOIRE_PATH,"r");

	if(f != NULL)
	{


		char c[255];
		char chemin_fichier[255];	

		int nb_fichiers_indexation; // on recup le nombre de lignes
		fscanf(f,"%d",&nb_fichiers_indexation);


		int index = 0;
		while(fscanf(f,"%s",c)==1)//lecture du fichier lignes par lignes
		{
            
			index++;

			afficher_barre_progression(index,nb_fichiers_indexation,36);// c'est bow 

            //on creer un nouveau chemin pour y stocker l'image convertie et ne pas perdre l'originale
			strcpy(chemin_fichier,c);
            char nouveau_path[500];
            strcpy(nouveau_path,chemin_fichier);
            strcat(nouveau_path,"_converted");

            //on conviertie l'image 
            convertir_image(chemin_fichier,nouveau_path,nb_bits_quant);
            
            //ensuite on indexe l'image convertie
            int resultat = indexation_texte(nouveau_path);


            //et on remove le fichier maintenant useless
            remove(nouveau_path); 

            
            // le resultat de l'indexation est recupere et traité
			if(resultat != 0 && resultat != 2)
			{
				printf("\033[1m\033[31m %s","echec indexation du fichier : \n ");
				printf(" \x1B[0m %s \n",chemin_fichier);
				//return(1);
			}
            else if(resultat == 2)
            {
                printf("   \033[1m\033[34m %s","indexation deja faite : ");
				printf("\x1B[0m %s  \n",chemin_fichier);

            }

		}

        fclose(f);
        remove(NOM_FICHIERS_REPERTOIRE_PATH);
	
	}else
	{
		return -1;
	}


	return 0;
}


/**
 * [ALGO]
 * 
 * indexation de tout un repertoire
 * appelle la fonction indexation sur tous le repertoire
 */
int indexer_repertoire_texte(char* chemin_repertoire)
{

    // On recupere le nombre de fichiers qui correspondent 
	char commande[1000];
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande," | grep -v / ");
	strcat(commande," | wc -l > ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH);
	system(commande);

    // Puis on recupere leurs chemins
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande," | grep -v / ");
	strcat(commande,">> ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH);
	system(commande);

    //on ouvre le fichier on l'on a stocker tout ca
	FILE* f=fopen(NOM_FICHIERS_REPERTOIRE_PATH,"r");


	if(f != NULL)
	{
		char c[255];
		char chemin_fichier[255];	

		int nb_fichiers_indexation;
		fscanf(f,"%d",&nb_fichiers_indexation);// on recup le nombre de lignes

		int index = 0;

		while(fscanf(f,"%s",c)==1)//lecture du fichier lignes par lignes
		{
			index++;

			afficher_barre_progression(index,nb_fichiers_indexation,36);

            //recuperation du chemin 
			strcpy(chemin_fichier,chemin_repertoire);
			strcat(chemin_fichier,"/");
			strcat(chemin_fichier,c);

            //indexation
            int resultat = indexation_texte(chemin_fichier);

            //traitement du resultat de l'indexation
			if(resultat != 0 && resultat != 2)
			{
				printf("\033[1m\033[31m %s","echec indexation du fichier : \n ");
				printf(" \x1B[0m %s \n",chemin_fichier);
			}
            else if(resultat == 2)
            {
                printf("\n   \033[1m\033[34m %s","indexation deja faite : ");
				printf("\x1B[0m %s  \n",chemin_fichier);

            }
		}

        fclose(f);
        remove(NOM_FICHIERS_REPERTOIRE_PATH);
	
	}else
	{
		return -1;
	}

	
	return 0;
}

/**
 * [Charvy]
 * 
 * @param chemin_repertoire : char*
 * Description : Indexation de tout un repertoire son 
 * Appelle la fonction indexation sur tous le repertoire
 */
int indexer_repertoire_son(char* chemin_repertoire)
{




    // On recupere le nombre de fichiers qui correspondent 
	char commande[1000];
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande,"/*.bin");
	strcat(commande," | wc -l > ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH); // on save ca dans le fichier NOM_FICHIERS_REPERTOIRE_PATH
	system(commande);

    // Puis on recupere leurs chemins
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande,"/*.bin");
	strcat(commande,">> ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH); // on save ca a la suite 
	system(commande);


    //exit(0);

    //on ouvre le fichier où l'on a stocké tout ca
	FILE* f=fopen(NOM_FICHIERS_REPERTOIRE_PATH,"r");


	if(f != NULL)
	{



		char c[255];
		char chemin_fichier[255];	

		int nb_fichiers_indexation;
		fscanf(f,"%d",&nb_fichiers_indexation);// on recupère le nombre de lignes

		int index = 0;

		while(fscanf(f,"%s",c)==1)//lecture du fichier lignes par lignes
		{
			index++;

			afficher_barre_progression(index,nb_fichiers_indexation,36);

            //recuperation du chemin 

            //indexation
            int resultat = descripteur(c);


            printf("%d ; ",resultat);

            //traitement du resultat de l'indexation
			if(resultat != 0 && resultat != 2)
			{
				printf("\033[1m\033[31m %s","echec indexation du fichier : \n ");
				printf(" \x1B[0m %s \n",chemin_fichier);
			}
            else if(resultat == 2)
            {
                printf("\n   \033[1m\033[34m %s","indexation deja faite : ");
				printf("\x1B[0m %s  \n",chemin_fichier);

            }
		}

        fclose(f);
        remove(NOM_FICHIERS_REPERTOIRE_PATH);
	
	}else
	{
		return -1;
	}

	
	return 0;
}


/**
 * [Charvy]
 * @param void 
 * Description : User interface, permet d'afficher un guide pour configurer les paramètes d'indexation de fichiers texte ou image
 * 
 */ 
void UI_configuration(void)
{
    int VALEUR_LIMITE,TAILLE_MAX,TAILLE_MIN_MOT,TAILLE_BIT;


    do
    {
        afficher_options("CONFIGURATION","Saisir la valeur limite  ",50,NULL,3);

        VALEUR_LIMITE = saisir();
        /*printf("(tappez ici)-> ");
        scanf("%d",&VALEUR_LIMITE); */


        if(VALEUR_LIMITE <= 0)
        {
            afficher_options("ERREUR","La valeur limite doit etre ]0;+inf[ ",50,NULL,3);
            continuer();
        }
    }while(VALEUR_LIMITE <= 0);

    do
    {
        afficher_options("CONFIGURATION","Saisir la taille maximum  ",50,NULL,3);

        TAILLE_MAX = saisir();
        /*printf("(tappez ici)-> ");
        scanf("%d",&TAILLE_MAX);*/


        if(TAILLE_MAX <= 0)
        {
            afficher_options("ERREUR","La valeur limite doit etre ]0;+inf[ ",50,NULL,3);
            continuer();
        }
    }while(TAILLE_MAX <= 0);

    do
    {
        afficher_options("CONFIGURATION","Saisir la taille minimale  ",50,NULL,3);
        
        TAILLE_MIN_MOT = saisir();
        /*printf("(tappez ici)-> ");
        scanf("%d",&TAILLE_MIN_MOT);*/

        if(TAILLE_MIN_MOT <= 0)
        {
            afficher_options("ERREUR","La valeur limite doit etre ]0;+inf[ ",50,NULL,3);
            continuer();

        }
    }while(TAILLE_MIN_MOT <= 0);

    do
    {
        afficher_options("CONFIGURATION","Saisir la taille quantification (IMAGE) ",50,NULL,3);
        
        TAILLE_BIT = saisir();
         /*printf("(tappez ici)-> ");
        scanf("%d",&TAILLE_BIT);*/
        
        if(TAILLE_BIT <= 0)
        {
            afficher_options("ERREUR","La valeur limite doit etre ]1;8[ ",50,NULL,3);
            continuer();

        }
    }while(TAILLE_BIT <= 0);


    modifier_fic_config(VALEUR_LIMITE,TAILLE_MAX,TAILLE_MIN_MOT,TAILLE_BIT);

}

/******************************************UI INDEXER************************************************************/
/**
 * [Charvy]
 * @param void 
 * Description : User interface, permet d'afficher un guide d'indexation de fichiers textes
 * 
 */ 
void UI_Indexer_texte(void)
{

    int x;
    do
    {
        char *options2[3] = {"Indexer plusieurs fichiers","Indexer un repertoire","RETOUR"};
        afficher_options("INDEXER","Les options sont :",50,options2,3);

        x = saisir();
        /*scanf("%d",&x);*/
        
        if(x < 0 || x>=3)
        {
            afficher_options("ERREUR","La valeur doit etre dans [0;2] ",50,NULL,3);
            continuer();
        }
    }while(x < 0 || x>=3);

    int y;

    switch (x)
    {
    case 0:



        do
        {
            afficher_options("INDEXER PLUSIEURS FICHIERS"," Combien de fichiers voulez vous indexer ? ",50,NULL,3);

            y = saisir();
            /*printf("(tappez ici)-> ");
            scanf("%d",&y);*/

            if(y <= 0)
            {
                afficher_options("ERREUR","La valeur doit etre > 0 ",50,NULL,3);
                continuer();
            }
        }while(y <= 0);


        for (int i = 0; i < y; i++)
        {
            char number[12];
            sprintf(number, "%d", i+1);
            char total_number[12];
            sprintf(total_number, "%d", y);

            char total_string[200];

            strcpy(total_string,"Fichier [");
            strcat(total_string,number);
            strcat(total_string,"/");
            strcat(total_string,total_number);
            strcat(total_string,"]");

            afficher_options(total_string,"Merci de saisir le chemin du fichier ",50,NULL,3);
            char chemin[200];

            printf("-> ");
            scanf("%s",chemin);

            int resultat = indexation_texte(chemin);
                            
            if(resultat ==2 )
            {
                printf(BOLDGREEN"\n  Fichier deja indexé \n "RESET);
                fflush(stdout);

            }
            else if(resultat ==0 )
            {
                printf(BOLDGREEN"\n  OK \n "RESET);
                fflush(stdout);
            }
            else 
            {

                printf(BOLDRED"\n [ERREUR] N°"RESET"\n ");
                fflush(stdout);
                printf("%d",resultat);
                fflush(stdout);

            }

            if(i <= y)
            {
                continuer();
            }


        }
        



        break;
    case 1:


            afficher_options("INDEXER TOUT UN DOSSIER","Merci de saisir le chemin du dossier ",50,NULL,3);
            char chemin[200];
            printf("-> ");
            scanf("%s",chemin);
            indexer_repertoire_texte(chemin);
            continuer();

        break;

    default:
        break;
    }

}

/**
 * [Charvy]
 * @param void 
 * Description : User interface, permet d'afficher un guide d'indexation de fichiers son
 * 
 */ 
void UI_Indexer_son(void)
{

    int x;
    do
    {
        char *options2[3] = {"Indexer plusieurs fichiers","Indexer un repertoire","RETOUR"};
        afficher_options("INDEXER","Les options sont :",50,options2,3);

        x = saisir();
        /*scanf("%d",&x);*/
        
        if(x < 0 || x>=3)
        {
            afficher_options("ERREUR","La valeur doit etre dans [0;2] ",50,NULL,3);
            continuer();
        }
    }while(x < 0 || x>=3);

    int y;

    switch (x)
    {
    case 0:



        do
        {
            afficher_options("INDEXER PLUSIEURS FICHIERS"," Combien de fichiers voulez vous indexer ? ",50,NULL,3);

            y = saisir();
            /*printf("(tappez ici)-> ");
            scanf("%d",&y);*/

            if(y <= 0)
            {
                afficher_options("ERREUR","La valeur doit etre > 0 ",50,NULL,3);
                continuer();
            }
        }while(y <= 0);


        for (int i = 0; i < y; i++)
        {
            char number[12];
            sprintf(number, "%d", i+1);
            char total_number[12];
            sprintf(total_number, "%d", y);

            char total_string[200];

            strcpy(total_string,"Fichier [");
            strcat(total_string,number);
            strcat(total_string,"/");
            strcat(total_string,total_number);
            strcat(total_string,"]");

            afficher_options(total_string,"Merci de saisir le chemin du fichier ",50,NULL,3);
            char chemin[200];

            printf("-> ");
            scanf("%s",chemin);

            
            int resultat = descripteur(chemin);
                            
            if(resultat ==2 )
            {
                printf(BOLDGREEN"\n  Fichier deja indexé \n "RESET);
                fflush(stdout);

            }
            else if(resultat ==0 )
            {
                printf(BOLDGREEN"\n  OK \n "RESET);
                fflush(stdout);
            }
            else 
            {

                printf(BOLDRED"\n [ERREUR] N°"RESET"\n ");
                fflush(stdout);
                printf("%d",resultat);
                fflush(stdout);

            }
            
            if(i <= y)
            {
                continuer();
            }


        }
        



        break;
    case 1:


            afficher_options("INDEXER TOUT UN DOSSIER","Merci de saisir le chemin du dossier ",50,NULL,3);
            char chemin[200];
            printf("-> ");
            scanf("%s",chemin);


            int resultat = indexer_repertoire_son(chemin);
            continuer();

        break;

    default:
        break;
    }

}
/**
 * [Charvy]
 * @param TAILLE_BIT_IMAGE : int 
 * Description : User interface, permet d'afficher un guide d'indexation de fichiers image
 * 
 */ 
void UI_Indexer_image(int TAILLE_BIT_IMAGE)
{

    int x;
    do
    {
        char *options2[3] = {"Indexer plusieurs fichiers","Indexer un repertoire","RETOUR"};
        afficher_options("INDEXER","Les options sont :",50,options2,3);

        x = saisir();
        /*scanf("%d",&x);*/
        
        if(x < 0 || x>=3)
        {
            afficher_options("ERREUR","La valeur doit etre dans [0;2] ",50,NULL,3);
            continuer();
        }
    }while(x < 0 || x>=3);

    int y;

    switch (x)
    {
    case 0:



        do
        {
            afficher_options("INDEXER PLUSIEURS FICHIERS"," Combien de fichiers voulez vous indexer ? ",50,NULL,3);
            
            y = saisir();
            /*printf("(tappez ici)-> ");
            scanf("%d",&y);*/
            
            if(y <= 0)
            {
                afficher_options("ERREUR","La valeur doit etre > 0 ",50,NULL,3);
                continuer();
            }
        }while(y <= 0);






        for (int i = 0; i < y; i++)
        {
            char number[12];
            sprintf(number, "%d", i+1);
            char total_number[12];
            sprintf(total_number, "%d", y);

            char total_string[200];

            strcpy(total_string,"Fichier [");
            strcat(total_string,number);
            strcat(total_string,"/");
            strcat(total_string,total_number);
            strcat(total_string,"]");

            afficher_options(total_string,"Merci de saisir le chemin du fichier ",50,NULL,3);
            char chemin[200];

            printf("-> ");
            scanf("%s",chemin);

            char nouveau_path[500];
            strcpy(nouveau_path,chemin);
            strcat(nouveau_path,"_converted");
            convertir_image(chemin,nouveau_path,TAILLE_BIT_IMAGE);
            int resultat = indexation_texte(nouveau_path);
            remove(nouveau_path);

            if(resultat ==2 )
            {
                printf(BOLDGREEN"\n  Fichier deja indexé \n "RESET);
                fflush(stdout);

            }
            else if(resultat ==0 )
            {
                printf(BOLDGREEN"\n  OK \n "RESET);
                fflush(stdout);
            }
            else 
            {

                printf(BOLDRED"\n [ERREUR] N°"RESET"\n ");
                fflush(stdout);
                printf("%d",resultat);
                fflush(stdout);

            }

            if(i <= y)
            {
                continuer();
            }


        }
        



        break;
    case 1:


            afficher_options("INDEXER TOUT UN DOSSIER","Merci de saisir le chemin du dossier ",50,NULL,3);

            char chemin[200];
            printf("-> ");
            scanf("%s",chemin);
            
            indexer_repertoire_Image(chemin, TAILLE_BIT_IMAGE);


            continuer();

        break;

    default:
        break;
    }

}

/******************************************UI RECHERCHER*******************************************************/

/**
 * [Charvy]
 * @param void 
 * Description : User interface, permet d'afficher un guide de recherche de fichiers texte
 * 
 */ 
void UI_rechercher_texte(void)
{
    
    int x;

    do
    {
        char *options2[3] = {"Recherche par mots cle", "Recherche par similarite","RETOUR"};
        afficher_options("RECHERCHER","Les options sont :",50,options2,3);
        
        x = saisir();
        /*scanf("%d",&x);*/

        if(x < 0 || x>=3)
        {
            afficher_options("ERREUR","La valeur limite doit etre dans [0;2] ",50,NULL,3);
            continuer();
        }
    }while(x < 0 || x>=3);


    if(x == 0)
    {			
        afficher_options("RECHERCHER MOT CLE","Merci de saisir le mot cle ",50,NULL,3);
        char mot[200];
        printf("(tappez ici)-> ");
        scanf("%s",mot);

        SORTED_LIST liste = recherche_par_mot_cle(mot);

        int taille_liste = taille_FILE(liste);
        if(liste != NULL)
        {
            char**options = malloc(taille_liste*sizeof(char*));
            if(options != NULL)
            {
                for (int i = 0; i < taille_liste; i++)
                {
                    options[i] = malloc(sizeof(char)*1000);
                }
            }


            int index =0;
            SORTED_LIST aux = liste;
            while(aux!=NULL)
            {   
                ELEMENT* node = malloc(sizeof(ELEMENT));
                if(node != NULL)
                {
                    aux = dePILE(aux,node);

                    
                    char* nom_fichier = basename(node->path);
                    //printf("index %d / ",index);
                    strcpy(options[index] ,nom_fichier);
                    char float_char[50];
                    sprintf(float_char, "%d", (int)node->number);
                    strcat(options[index] ," -> ");
                    strcat(options[index] ,float_char);
                    strcat(options[index] ," occurrences");

                    index++;
                }
            }



            afficher_options("RESULTATS","nom_fichier | pourcentage similarite",100,options,taille_liste);
            continuer();

            int x;
            do
            {
                char *options2[2] = {"NON", "OUI"};
                afficher_options("FIL ROUGE","Voulez vous ouvrir le fichier qui correspond le mieux ?",50,options2,2);
                
                x = saisir();

                if(x < 0 || x>=2 )
                {
                    afficher_options("ERREUR","La valeur doit etre dans [0;1] ",50,NULL,3);
                    continuer();
                }


            }while(x < 0 || x>=2);
            if(x == 1)
            {
                ouvertureTexte(get(liste, taille_liste-1 ).path );
            }



        }else
        {
				printf("\033[1m\033[31m %s","Pas de fichiers avec ce terme de recherche \n ");
				printf(" \x1B[0m \n");
        }



        




        continuer();

        
    }else if(x == 1)
    {

        afficher_options("RECHERCHER SIMILARITE","Merci de saisir le chemin du fichier à comparer ",100,NULL,3);
        char chemin[200];
        printf("(tappez ici)-> ");
        

        scanf("%s",chemin);

        SORTED_LIST liste = recherche_par_similarite(chemin);


        int taille_liste = taille_FILE(liste);
        if(liste != NULL)
        {
            char**options = malloc(taille_liste*sizeof(char*));
            if(options != NULL)
            {
                for (int i = 0; i < taille_liste; i++)
                {
                    options[i] = malloc(sizeof(char)*1000);
                }
            }

            SORTED_LIST aux = liste;
            int index =0;
            while(aux!=NULL)
            {   
                ELEMENT* node = malloc(sizeof(ELEMENT));
                if(node != NULL)
                {
                    aux = dePILE(aux,node);

                    
                    char* nom_fichier = basename(node->path);
                    //printf("index %d / ",index);
                    strcpy(options[index] ,nom_fichier);
                    char float_char[50];
                    sprintf(float_char, "%f", node->number);
                    strcat(options[index] ," -> ");
                    strcat(options[index] ,float_char);
                    strcat(options[index] ,"%%");

                    index++;
                }
            }



            afficher_options("RESULTATS","nom_fichier | pourcentage similarite",100,options,taille_liste);

            continuer();

            int x;
            do
            {
                char *options2[2] = {"NON", "OUI"};
                afficher_options("FIL ROUGE","Voulez vous ouvrir le fichier qui correspond le mieux ?",100,options2,2);
                
                x = saisir();

                if(x < 0 || x>=2 )
                {
                    afficher_options("ERREUR","La valeur doit etre dans [0;1] ",50,NULL,3);
                    continuer();
                }


            }while(x < 0 || x>=2);
            if(x == 1)
            {
                ouvertureTexte(get(liste, taille_liste-2 ).path );
            }


        }else
        {
				printf("\033[1m\033[31m %s","Pas de fichiers similaires\n ");
				printf(" \x1B[0m \n");
        }
        
        continuer();

    }

}


/**
 * [Charvy]
 * @param TAILLE_BIT_IMAGE 
 * Description : User interface, permet d'afficher un guide de recherche de fichiers image
 * 
 */ 
void UI_rechercher_image(int TAILLE_BIT_IMAGE)
{
    
    int x;

    do
    {
        char *options2[3] = {"Recherche par mots cle", "Recherche par similarite","RETOUR"};
        afficher_options("RECHERCHER","Les options sont :",50,options2,3);
        
        x = saisir();
        /*scanf("%d",&x);*/

        if(x < 0 || x>=3)
        {
            afficher_options("ERREUR","La valeur limite doit etre dans [0;2] ",50,NULL,3);
            continuer();
        }
    }while(x < 0 || x>=3);


    if(x == 0)
    {			

        int R;
        int G;
        int B;
        do
        {
            afficher_options("RECHERCHER MOT CLE","Merci de saisir la composante ROUGE [0;255]",50,NULL,3);
            char mot[200];

            R = saisir();
            /*printf("(tappez ici)-> ");
            scanf("%d",&R);*/

        }while(x < 0 || x>255);    
        do
        {

            afficher_options("RECHERCHER MOT CLE","Merci de saisir  la composante VERTE  [0;255]",50,NULL,3);
            char mot[200];
            
            G = saisir();
            /*printf("(tappez ici)-> ");
            scanf("%d",&G);*/
        
        }while(x < 0 || x>255);    

        do
        {
            afficher_options("RECHERCHER MOT CLE","Merci de saisir  la composante BLEU  [0;255]",50,NULL,3);
            char mot[200];

            B = saisir();
            /*printf("(tappez ici)-> ");
            scanf("%d",&B);*/

        }while(x < 0 || x>255);    



        short converted_value = convertRGB(R,G,B,TAILLE_BIT_IMAGE);
        char mot[12];
        sprintf(mot, "%d", converted_value);
        SORTED_LIST liste = recherche_par_mot_cle(mot);

        int taille_liste = taille_FILE(liste);
        if(liste != NULL)
        {
            char**options = malloc(taille_liste*sizeof(char*));
            if(options != NULL)
            {
                for (int i = 0; i < taille_liste; i++)
                {
                    options[i] = malloc(sizeof(char)*1000);
                }
            }

            SORTED_LIST aux = liste;
            int index =0;
            while(aux!=NULL)
            {   
                ELEMENT* node = malloc(sizeof(ELEMENT));
                if(node != NULL)
                {
                    aux = dePILE(aux,node);

                    
                    char* nom_fichier = basename(node->path);
                    //printf("index %d / ",index);
                    strcpy(options[index] ,nom_fichier);
                    char float_char[50];
                    sprintf(float_char, "%d", (int)(node->number));
                    strcat(options[index] ," -> ");
                    strcat(options[index] ,float_char);
                    strcat(options[index] ," occurrences");

                    index++;
                }
            }


            afficher_options("RESULTATS","nom_fichier | pourcentage similarite",150,options,taille_liste);
            
            continuer();

            int x;
            do
            {
                char *options2[2] = {"NON", "OUI"};
                afficher_options("FIL ROUGE","Voulez vous ouvrir le fichier qui correspond le mieux ?",100,options2,2);
                
                x = saisir();

                if(x < 0 || x>=2 )
                {
                    afficher_options("ERREUR","La valeur doit etre dans [0;1] ",50,NULL,3);
                    continuer();
                }


            }while(x < 0 || x>=2);

            if(x == 1)
            {

                char path[1000];

                strcpy(path,get(liste, taille_liste-2 ).path);

                path[strlen(path)-strlen("_converted")] = '\0';
                path[strlen(path)-strlen(".txt")] = '\0';// on rebuild la chaine originale

                char* dossier = malloc(sizeof(char)*1000);
                strcpy(dossier,path);
                dossier = dirname(dossier);
                if(strcmp("/home/ugo/Desktop/git/fil-rouge/DATA_FIL_ROUGE_DEV/TEST_NB",dossier)==0)
                {
                    strcat(path,".bmp");
                }
                else if(strcmp("/home/ugo/Desktop/git/fil-rouge/DATA_FIL_ROUGE_DEV/TEST_RGB",dossier)==0)
                {
                    strcat(path,".jpg");
                }
                

                //printf("\n path -> %s",path);
                //printf("\n dossier -> %s", dossier);
                fflush(stdout);

                ouvertureImage(path);
            }
            
            


        }else
        {
				printf("\033[1m\033[31m %s","Pas de fichiers avec ce terme de recherche \n ");
				printf(" \x1B[0m \n");
        }
        continuer();

        
    }
        
        else if(x == 1)
        {

            afficher_options("RECHERCHER SIMILARITE","Merci de saisir le chemin du fichier à comparer ",50,NULL,3);
            char chemin[1000];
            printf("(tappez ici)-> ");
            scanf("%s",chemin);

            char nouveau_path[1000];

            strcpy(nouveau_path,chemin);
            strcat(nouveau_path,"_converted");

            convertir_image(chemin,nouveau_path,TAILLE_BIT_IMAGE);
            

            SORTED_LIST liste = recherche_par_similarite(nouveau_path);

            remove(nouveau_path);


            int taille_liste = taille_FILE(liste);
            if(liste != NULL)
            {
                char**options = malloc(taille_liste*sizeof(char*));
                if(options != NULL)
                {
                    for (int i = 0; i < taille_liste; i++)
                    {
                        options[i] = malloc(sizeof(char)*1000);
                    }
                }


                int index =0;
                SORTED_LIST aux = liste;
                while(aux!=NULL)
                {   
                    ELEMENT* node = malloc(sizeof(ELEMENT));
                    if(node != NULL)
                    {
                        aux = dePILE(aux,node);
                        char* nom_fichier = basename(node->path);
                        strcpy(options[index] ,nom_fichier);
                        options[index][strlen(options[index])-strlen("_converted")] = '\0';

                        char float_char[50];
                        sprintf(float_char, "%f", node->number);
                        strcat(options[index] ," -> ");
                        strcat(options[index] ,float_char);
                        strcat(options[index] ,"%%");

                        index++;
                    }
                    //printf("\n Fichier similaire à %f %% -> path %s \n",node->number,node->path);
                }



                afficher_options("RESULTATS","nom_fichier | pourcentage similarite",50,options,taille_liste);

                continuer();

                int x;
                do
                {
                    char *options2[2] = {"NON", "OUI"};
                    afficher_options("FIL ROUGE","Voulez vous ouvrir le fichier qui correspond le mieux ?",100,options2,2);
                    
                    x = saisir();

                    if(x < 0 || x>=2 )
                    {
                        afficher_options("ERREUR","La valeur doit etre dans [0;1] ",50,NULL,3);
                        continuer();
                    }


                }while(x < 0 || x>=2);

                if(x == 1)
                {


                char path[1000];

                strcpy(path,get(liste, taille_liste-2 ).path);

                path[strlen(path)-strlen("_converted")] = '\0';
                path[strlen(path)-strlen(".txt")] = '\0';// on rebuild la chaine originale

                char* dossier = malloc(sizeof(char)*1000);
                strcpy(dossier,path);
                dossier = dirname(dossier);
                if(strcmp("/home/ugo/Desktop/git/fil-rouge/DATA_FIL_ROUGE_DEV/TEST_NB",dossier)==0)
                {
                    strcat(path,".bmp");
                }
                else if(strcmp("/home/ugo/Desktop/git/fil-rouge/DATA_FIL_ROUGE_DEV/TEST_RGB",dossier)==0)
                {
                    strcat(path,".jpg");
                }
                

                //printf("\n path -> %s",path);
                //printf("\n dossier -> %s", dossier);
                fflush(stdout);

                ouvertureImage(path);

    }
            

        }else
        {
				printf("\033[1m\033[31m %s","Pas de fichiers similaires\n ");
				printf(" \x1B[0m \n");
        }
        

        continuer();

    }
}



/**
 * [Charvy]
 * @param void 
 * Description : User interface, permet d'afficher un guide de recherche de fichiers son
 * 
 */ 
void UI_rechercher_son(void)
{
    
    int x;

    do
    {
        char *options2[3] = {"Recherche contenance", "Recherche par similarite","RETOUR"};
        afficher_options("RECHERCHER","Les options sont :",50,options2,3);
        
        x = saisir();

        if(x < 0 || x>=3)
        {
            afficher_options("ERREUR","La valeur limite doit etre dans [0;2] ",50,NULL,3);
            continuer();
        }
    }while(x < 0 || x>=3);


    if(x == 0)
    {			
       
        afficher_options("RECHERCHER CONTENENCE","Merci de saisir le chemin du fichier à comparer ",50,NULL,3);
        char chemin[200];
        printf("(tappez ici)-> ");
        

        scanf("%s",chemin);

        SORTED_LIST liste = comparaison(chemin, 0);


        int taille_liste = taille_FILE(liste);
        if(liste != NULL)
        {
            char**options = malloc(taille_liste*sizeof(char*));
            if(options != NULL)
            {
                for (int i = 0; i < taille_liste; i++)
                {
                    options[i] = malloc(sizeof(char)*1000);
                }
            }

            SORTED_LIST aux = liste;
            int index =0;
            while(aux!=NULL)
            {   
                ELEMENT* node = malloc(sizeof(ELEMENT));
                if(node != NULL)
                {
                    aux = dePILE(aux,node);

                    
                    char* nom_fichier = basename(node->path);
                    //printf("index %d / ",index);
                    strcpy(options[index] ,nom_fichier);
                    char float_char[50];
                    sprintf(float_char, "%d", (int)(node->number));
                    strcat(options[index] ," -> ");
                    strcat(options[index] ,float_char);
                    strcat(options[index] ,"s");

                    index++;
                }
            }



            afficher_options("RESULTATS","nom_fichier | pourcentage similarite",100,options,taille_liste);

            continuer();

            int x;
            do
            {
                char *options2[2] = {"NON", "OUI"};
                afficher_options("FIL ROUGE","Voulez vous ouvrir le fichier qui correspond le mieux ?",100,options2,2);
                
                x = saisir();

                if(x < 0 || x>=2 )
                {
                    afficher_options("ERREUR","La valeur doit etre dans [0;1] ",50,NULL,3);
                    continuer();
                }


            }while(x < 0 || x>=2);
            if(x == 1)
            {
                ouvertureAudio(get(liste,taille_liste-1).path,(int)get(liste,taille_liste-1).number);
            }


        }else
        {
				printf("\033[1m\033[31m %s","Pas de fichiers similaires\n ");
				printf(" \x1B[0m \n");
        }
        
        continuer();


        
    }else if(x == 1)
    {

        afficher_options("RECHERCHER SIMILARITE","Merci de saisir le chemin du fichier à comparer ",50,NULL,3);
        char chemin[200];
        printf("(tappez ici)-> ");
        

        scanf("%s",chemin);

        SORTED_LIST liste = comparaison(chemin, 1);


        int taille_liste = taille_FILE(liste);
        if(liste != NULL)
        {
            char**options = malloc(taille_liste*sizeof(char*));
            if(options != NULL)
            {
                for (int i = 0; i < taille_liste; i++)
                {
                    options[i] = malloc(sizeof(char)*1000);
                }
            }

            SORTED_LIST aux = liste;
            int index =0;
            while(aux!=NULL)
            {   
                ELEMENT* node = malloc(sizeof(ELEMENT));
                if(node != NULL)
                {
                    aux = dePILE(aux,node);

                    
                    char* nom_fichier = basename(node->path);
                    //printf("index %d / ",index);
                    strcpy(options[index] ,nom_fichier);
                    char float_char[50];
                    sprintf(float_char, "%f", node->number);
                    strcat(options[index] ," -> ");
                    strcat(options[index] ,float_char);
                    strcat(options[index] ,"%%");

                    index++;
                }
            }


            afficher_options("RESULTATS","nom_fichier | pourcentage similarite",100,options,taille_liste);

            continuer();

            int x;
            do
            {
                char *options2[2] = {"NON", "OUI"};
                afficher_options("FIL ROUGE","Voulez vous ouvrir le fichier qui correspond le mieux ?",50,options2,2);
                
                x = saisir();

                if(x < 0 || x>=2 )
                {
                    afficher_options("ERREUR","La valeur doit etre dans [0;1] ",50,NULL,3);
                    continuer();
                }


            }while(x < 0 || x>=2);
            if(x == 1)
            {


                printf("path = %s ",get(liste, taille_liste-1 ).path );
                ouvertureAudio(get(liste, taille_liste-1 ).path,0);



            }


        }else
        {


				printf("\033[1m\033[31m %s","Pas de fichiers similaires\n ");
				printf(" \x1B[0m \n");
        
        }
        
        continuer();




    }

}



/*******************************************BASE UI******************************************************************/

/**
 * [Charvy]
 * @param admin_mode : int 
 * Description : User interface, permet d'afficher les options accessibles pour un utilisateur en mode admin pour la partie texte
 * Fait appel aux différentes fonctions vues précédemment
 */ 
void UI_texte(int admin_mode)
{
    int x=0;

    if(admin_mode)
    {


        do
        {
            char *options[4] = {"Configuration", "Indexer", "Rechercher","RETOUR"};

            afficher_options("FICHIERS TEXTE","Les options sont :",50,options,4);

            x = saisir();
            /*scanf("%d",&x);*/
            
            if(x < 0 || x>=4)
            {
                afficher_options("ERREUR","La valeur limite doit etre dans [0;4] ",50,NULL,3);
                continuer();
            }
        }while(x < 0 || x>=4);


        switch (x)
        {
        case 0:
        // l'utilisateur choisi Editer fichier configuration
            UI_configuration();

            break;
        case 1:
        // l'utilisateur choisi Indexer
            UI_Indexer_texte();

            break;
        case 2:
        
            UI_texte(0);
            break;

        default:

            break;
        }

    }
    else
    {
        UI_rechercher_texte();

    }
}


/**
 * [Charvy]
 * @param admin_mode : int 
 * Description : User interface, permet d'afficher les options accessibles pour un utilisateur en mode admin pour la partie image
 * Fait appel aux différentes fonctions vues précédemment
 */ 
void UI_image(int admin_mode)
{
    int x=0;
    int VALEUR_LIMITE,TAILLE_MAX,TAILLE_MIN_MOT,TAILLE_BIT_IMAGE;
    FILE* f;

    if(admin_mode)
    {
        do
        {
            char *options[4] = {"Configuration", "Indexer", "Rechercher","RETOUR"};

            afficher_options("IMAGE","Les options sont :",50,options,4);

            x = saisir();
            /*scanf("%d",&x);*/
            
            if(x < 0 || x>=4)
            {
                afficher_options("ERREUR","La valeur limite doit etre dans [0;4[ ",50,NULL,3);
                continuer();
            }
        }while(x < 0 || x>=4);


        switch (x)
        {
        case 0:
            UI_configuration();
            break;
        case 1:
            //recuperation du nombre de bits pour le fichier indexation 
            f=fopen("./IMAGE/Fichiers/config.txt","r");
            if(f == NULL)
            {
                printf("\n \033[1m\033[31m [ERROR] \x1B[0m UI_Indexer_image load file : %s FAILLED\n Merci de bien vouloir faire la configuration \n","./IMAGE/Fichiers/config.txt");
                fflush(stdout);
                exit(1);
            }
            fscanf(f,"%d %d %d %d",&VALEUR_LIMITE,&TAILLE_MAX,&TAILLE_MIN_MOT,&TAILLE_BIT_IMAGE);

            // l'utilisateur choisi Indexer
            UI_Indexer_image(TAILLE_BIT_IMAGE);

            break;
        case 2:
        
            UI_image(0);
            break;

        default:

            break;
        }

    }
    else
    {
        //

        f=fopen("./IMAGE/Fichiers/config.txt","r");
        if(f == NULL)
        {
            printf("\n \033[1m\033[31m [ERROR] \x1B[0m UI_Indexer_image load file : %s FAILLED\n Merci de bien vouloir faire la configuration \n","./IMAGE/Fichiers/config.txt");
            fflush(stdout);
            exit(1);
        }
        fscanf(f,"%d %d %d %d",&VALEUR_LIMITE,&TAILLE_MAX,&TAILLE_MIN_MOT,&TAILLE_BIT_IMAGE);

        UI_rechercher_image(TAILLE_BIT_IMAGE);

    }
}



/**
 * [Charvy]
 * @param flag_admin : int 
 * Description : User interface, permet d'afficher les options accessibles pour un utilisateur en mode admin pour la partie son
 * Fait appel aux différentes fonctions vues précédemment
 */ 
void UI_son(int flag_admin)
{
 
    int x;
    if(flag_admin)
    {
 
 
        do
        {
            char *options[3] = {"Indexer", "Rechercher","RETOUR"};

            afficher_options("SON","Les options sont :",50,options,3);

            x = saisir();
            /*scanf("%d",&x);*/
            
            if(x < 0 || x>=3)
            {
                afficher_options("ERREUR","La valeur limite doit etre dans [0;4[ ",50,NULL,3);
                continuer();
            }
        }while(x < 0 || x>=3);


        switch (x)
        {
        case 0:

            UI_Indexer_son();

           
            break;
        case 1:
            UI_son(0);

            break;
        default:

            break;
        }

    }
    else
    {
        
        UI_rechercher_son();


    }
    
  
}





int main()
{


    int flag_admin = log_in();
    int x=0;

/**
 * Affiche l'interface utilisateur afin de naviguer à travers le moteur de recherche
 *
 * */
    while(1)
    {

        do
        {
            char *options[4] = {"IMAGE", "TEXTE", "AUDIO","QUITTER"};
            afficher_options("FIL ROUGE","",50,options,4);

            x = saisir();

            if(x < 0 || x>=4)
            {
                afficher_options("ERREUR","La valeur doit etre dans [0;3] ",50,NULL,3);
                continuer();
            }
        }while(x < 0 || x>=4);




        switch (x)
        {
        case 0:
        // l'utilisateur choisi IMAGE
            _init_chemin("./IMAGE");
            UI_image(flag_admin);

            break;
        case 1:
        // l'utilisateur choisi TEXTE
            _init_chemin("./TEXTE");
            UI_texte(flag_admin);

            break;
        case 2:
        // l'utilisateur choisi SON
            _init_chemin("./");

            UI_son(flag_admin);


            break;

        default:

            exit(0);

            break;
        }

       // continuer();


    }



}
