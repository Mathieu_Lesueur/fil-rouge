
test_main :./_OUTILS/autre/ouverture.c ./_OUTILS/autre/ouverture.h ./_OUTILS/autre/sorted_list.c ./_OUTILS/autre/sorted_list.h ./_OUTILS/autre/element.c ./_OUTILS/autre/element.h ./SON/indexion.c ./SON/indexion.h ./IMAGE/images.c ./IMAGE/images.h ./_OUTILS/maths/maths_tools.c ./_OUTILS/maths/maths_tools.h ./TEXTE/Texte.c ./TEXTE/Texte.h ./_OUTILS/affichage/outils_affichage.c ./_OUTILS/affichage/outils_affichage.h main_program.c
	
	gcc -c ./_OUTILS/autre/ouverture.c
	gcc -c ./_OUTILS/autre/sorted_list.c
	gcc -c ./_OUTILS/autre/element.c
	gcc -c ./_OUTILS/affichage/outils_affichage.c
	gcc -c ./_OUTILS/maths/maths_tools.c
	gcc -c ./SON/indexion.c -lm
	gcc -c ./TEXTE/Texte.c
	gcc -c ./IMAGE/images.c
	gcc -c main_program.c
	gcc -o main_program.out ouverture.o sorted_list.o element.o outils_affichage.o maths_tools.o indexion.o images.o Texte.o main_program.o -lm
	./main_program.out


make_library :./_OUTILS/autre/sorted_list.c ./_OUTILS/autre/sorted_list.h ./_OUTILS/autre/element.c ./_OUTILS/autre/element.h ./SON/indexion.c ./SON/indexion.h ./IMAGE/images.c ./IMAGE/images.h ./_OUTILS/maths/maths_tools.c ./_OUTILS/maths/maths_tools.h ./TEXTE/Texte.c ./TEXTE/Texte.h ./_OUTILS/interface_java/interfaceJava.c
	
	gcc -fPIC -c ./_OUTILS/autre/element.c
	gcc -fPIC -c ./_OUTILS/autre/sorted_list.c
	gcc -fPIC -c ./_OUTILS/maths/maths_tools.c
	gcc -fPIC -c ./SON/indexion.c -lm
	gcc -fPIC -c ./TEXTE/Texte.c
	gcc -fPIC -c ./IMAGE/images.c

	gcc -c -Wall -Werror -fpic ./_OUTILS/interface_java/interfaceJava.c

	gcc -shared -o ./JAVA_WORKSPACE/Fil_Rouge/bin/libinterfaceJava.so sorted_list.o element.o maths_tools.o indexion.o Texte.o images.o interfaceJava.o
	rm *o

