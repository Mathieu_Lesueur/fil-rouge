#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<string.h>

#include "../_OUTILS/maths/maths_tools.h"
#include "images.h"



/** 
 * Ouvre le fichier image grace au path et renvoie le pointeur du FICHIER ouvert
 * [UGO ROUX]
 */
FILE * ouvir_fichier_r(char *path, int crashIfFail)
{
    FILE *f;
    f = fopen(path,"r");

    if(f==NULL)
    {
        printf("\n FICHIER %s N'EXSITE PAS ! \n",path);
        if(crashIfFail)
        {
            exit(1);
        }
    }


    return f;
}



/*
 * Lis une image a partir d'un fichier
 * 
 * Renvoie le tableau associé short[H][L][RGB]
 *                                         |_  avec R = 0, G = 1, B =2
 * 
 * Renvoie aussi la taille de ce tableau par parametres ptaillex  ptailley et pnbDim
 * [UGO ROUX]
 */
short ***ReadImages(char *path, short *ptaillex, short *ptailley, short *pnbDim)
{

    FILE *f = ouvir_fichier_r(path, 0);

    if(f != NULL)
    {
        //creation d'un String pour stockage du fscanf
        char cx[1024];

        //-------------------------Lecture de la taille de l'image

        fscanf(f, "%1023s", cx);
        short tailleY = atoi(cx);
        *ptailley = tailleY;

        fscanf(f, "%1023s", cx);
        short tailleX = atoi(cx);
        *ptaillex = tailleX;

        fscanf(f, "%1023s", cx);
        short nivRGB = atoi(cx);
        *pnbDim = nivRGB;
        
        //---------------------Creation du tableau 3D 

        //Malloc pour la dimention X *

        // returnedArray = [**][**]...[**]    
        short ***returnedArray = (short ***)malloc(tailleX * sizeof(short **));

        if (returnedArray == NULL)
        {
            printf("%s \n", "                       MALLOC returnedArray FAIL");
            exit(1);
        }

        for (short i = 0; i < tailleX; i++)
        {
            // Malloc de la dimention Y
            // returnedArray = [**][**]...[**]
            //                  |
            //                   ->[*]...[*][*]

            returnedArray[i] = (short **)malloc(tailleY * sizeof(short *));

            if (returnedArray[i] == NULL)
            {
                printf("%s \n", "                       MALLOC returnedArray FAIL");
                exit(1);
            }

            for (short y = 0; y < tailleY; y++)
            {
                // Malloc de la case
                // returnedArray = [**][**]...[**]
                //                  |
                //                   ->[*]...[*][*]
                //                      |
                //                       -> short

                returnedArray[i][y] = (short *)malloc(sizeof(short));

                if (returnedArray[i][y] == NULL)
                {
                    printf("%s \n", "                       MALLOC returnedArray FAIL");
                    exit(1);
                }
            }

    }


    //----------------- Remplissage du tableau
    char valTempo = ' ';
    
    //pour chaque niveau RGB 
    for (int r = 0; r < nivRGB; r++)
    {
        //on check chaque lignes
        for (int x = 0; x < tailleX; x++)
        {
            //on check chaques cols
            for (int y = 0; y < tailleY; y++)
            {   
                //si on est pas a la fin du fichier 
                if (fscanf(f, "%s", &valTempo) != EOF)
                {
                    short val = atoi(&valTempo); // convertion de la valeur en short
                    returnedArray[x][y][r] = val; //on la met dans le tableau
                }
                else
                {
                    printf("%s \n", "FIN");
                }
            }
        }
    }

    /*

    retour du tableau :
        
    
    nivRGB______________________________
    |                   tailleX
    |            ____________________________
    |           |short
    |           | ... short
    |           | ...  ...  short
    |   tailleY | ...  ...   ...  short
    |           |short ...   ...  ...  short
    |
    */
    return returnedArray;

    }
    return NULL;

}







/**
 * Prend un tableau 3D optenu grace a la fonction ReadImages de taille tailleX,  tailleY,  tailleRGB
 * et renvoie le tableau converti grace a la fonctio de quantification
 * nb_bits_quantif represente le nombre de bits sur lequel la quantification est faite
 * [UGO ROUX]
 */
int** quantification_all_image(short ***image, int tailleX, int tailleY, int tailleRGB, int nb_bits_quantif)
{
    // creation d'un tableau 2d de shorts 

    //allocation des lignes, qui sont des pointeurs de pointeurs vers les collones donc deux fois *
    int **returnedArray = (int **)malloc(tailleX * sizeof(int *));

    //on check si ca chie pas dans la colle 
    if (returnedArray == NULL)
    {
        printf("%s \n", "                       MALLOC returnedArray FAIL");
        exit(1);
    }

    // pour chaque ligne
    for (short x = 0; x < tailleX; x++)
    {
        //on fait un malloc de tailleY cases, ce qui nous donne nos cols 
        returnedArray[x] = (int *)malloc(tailleY * sizeof(int));

        if (returnedArray[x] == NULL)
        {
            printf("%s \n", "                       MALLOC returnedArray FAIL");
            exit(1);
        }

        //et pour chaque colone on fait le malloc de la case
        for (short y = 0; y < tailleY; y++)
        {
            

            if (tailleRGB == 1)
            {
                returnedArray[x][y] = convertRGB(image[x][y][0], image[x][y][0], image[x][y][0],nb_bits_quantif);
            }
            else if (tailleRGB == 3)
            {
                returnedArray[x][y] = convertRGB(image[x][y][0], image[x][y][1], image[x][y][2],nb_bits_quantif);
            }
            else
            {
                printf("%s \n", "                       Image dimention fail FAIL");
                exit(1);
            }
        }

        //printf("\n");
    }

    return returnedArray;
}

/**
 * Transforme un tableau 2D de int renvoye par quantification_all_image() en tableau 2D de char
 *[UGO ROUX]
 */
char*** image_to_string(int **image, int tailleX, int tailleY)
{

    //initialisation d'un tableau de string
    char ***returnedArray = (char ***)malloc(tailleX * sizeof(char** ));

    if (returnedArray == NULL)
    {
        printf("%s \n", "                       MALLOC returnedArray FAIL");
        exit(1);
    }

    //initialisation de chaque collone du tableau
    for (short x = 0; x < tailleX; x++)
    {
        //on fait un malloc de la taille de toute une ligne dans chaque collone
        returnedArray[x] = (char **)malloc(tailleY * sizeof(char *));

        if (returnedArray[x] == NULL)
        {
            printf("%s \n", "                       MALLOC returnedArray FAIL");
            exit(1);
        }
        

        //initalisation de chaque case de la ligne
        for (short y = 0; y < tailleY; y++)
        {

            //malloc d'un char[5] pour chaque case car j'ai pas besoin de plus
            returnedArray[x][y] = (char*)malloc(15 * sizeof(char));

            if (returnedArray[x][y] == NULL)
            {
                printf("%s \n", "                       MALLOC returnedArray FAIL");
                exit(1);
            }

            char* str = (char*)malloc(15 * sizeof(char));


            sprintf(str, "%d", image[x][y]);
            returnedArray[x][y] = str;
            
            //printf("%s  ",returnedArray[x][y]);
        }

        //printf("\n");
    }

    return returnedArray;
}


/**
 * Aplique la quantification R G B sur @nombre_bits et renvoie le @resultat
 * [UGO ROUX]
 */
int convertRGB(short R, short G, short B, int nombre_bits)
{
    //on utilise des shorts donc la quantification peut se faire sur max 8 bits
    if(nombre_bits > 8 || nombre_bits<=0)
    {
        printf("[ERREUR] Convertion RGB sur plus de 8 bits impossible");
        exit(1);
    }

    //on trqnforme nos valeurs rgb en tableau de bits
    // VOIR fil-rouge/_OUTILS/maths/maths_tools.c
    short *R_bit_Array = getbitarray(8, R);
    short *G_bit_Array = getbitarray(8, G);
    short *B_bit_Array = getbitarray(8, B);
 

    //on cree le tableau qui recoit le resultat
    short *arrayConversion = (short *)malloc( 3 * nombre_bits * sizeof(short));

    //on remplie le tableau
    int index = 0;
    for(int i = 0; i<nombre_bits  ;i++)
    {
        arrayConversion[index] = R_bit_Array[i];
        index++;
        arrayConversion[index] = G_bit_Array[i];

        index++;
        arrayConversion[index] = B_bit_Array[i];
        index++;



    }


    // on retourne le tableau transfrome en int
    return bit_to_int(arrayConversion, (3*nombre_bits) );

}




/**
 * Inutile
 * TODO : REMOVE
 * [UGO ROUX]
 */ 
char** linearisationMatrice(char*** image, int tailleX, int tailleY)
{


        char** returnedArray = (char **)malloc(tailleY * tailleY * sizeof(char *));

        if(returnedArray ==NULL)
        {
            printf("malloc Fail");
            exit(1);
        }

        int indice = 0;
        for (short x = 0; x < tailleX; x++)
        {
            for (short y = 0; y < tailleY; y++)
            {
                returnedArray[indice] = (char*)malloc(15 * sizeof(char));

                if (returnedArray[indice] == NULL)
                {
                    printf("%s \n", "                       MALLOC returnedArray FAIL");
                    exit(1);
                }

                returnedArray[indice]  = image[x][y];

                indice++;
            }
        }



    return returnedArray;
}





/**
 * Transforme le tableu @image de taille @taillex,@tailley en fichier @path
 * [UGO ROUX]
 */
void Image_to_file(char*** image,short taillex,short tailley, char* path)
{

    if(taillex>10000)
    {
        printf("\n [ERREUR] image trop grande \n");
        exit(1);
    }

    if( access( path, F_OK ) != -1 )
    {   
        char commande_rm[1000];
        strcpy(commande_rm,"rm ");
        strcat(commande_rm,path);
        system(commande_rm);

    }

        char commande_create[1000];
        strcpy(commande_create,"touch ");
        strcat(commande_create,path);
        system(commande_create);


    char commande2[1000];
    char one_ligne [10000];

    for(short x = 0; x<taillex; x++)
    {   
        strcpy(one_ligne,"\0");

        for(short y = 0; y<tailley; y++)
        {

            strcat(one_ligne,image[x][y]);
            strcat(one_ligne," ");


        }

        strcpy(commande2,"echo ");
        strcat(commande2,one_ligne);
        strcat(commande2,">>");
        strcat(commande2,path);
        system(commande2);


    }

}


/**
 * Aplique toutes les fonctions precedantes pour arriver a quantifier sur @nb_bits_convertion l'image donnee par @original_path
 * et enregistre la nouvelle image a @nouveau_path
 * [UGO ROUX]
 */
void convertir_image(char* original_path,char* nouveau_path, int nb_bits_convertion)
{

    static short*** returnedArray; // tableau qui va recuperer l'image originale
    
    //pour recuperer les dims de l'image
    short* taillex;
    short* tailley;
    short* nbDim;

    taillex = malloc(sizeof(short));
    tailley = malloc(sizeof(short));
    nbDim   = malloc(sizeof(short));

    if(nbDim == NULL || tailley == NULL || nbDim == NULL  )
    {
            printf("%s \n","[ERREUR] convertir_image -> MALLOC FAIL");
            exit(1);

    }

    //lecture de l'image
    returnedArray = ReadImages(original_path,taillex,tailley,nbDim);
    //quantification
    int** returned_coverted_Array = quantification_all_image(returnedArray,*taillex,*tailley,*nbDim,nb_bits_convertion);
    //transformation en char
    char ***  returned_String_Array = image_to_string(returned_coverted_Array,*taillex,*tailley);
    //enregistrement a nouveau_path
    Image_to_file(returned_String_Array,*taillex,*tailley,nouveau_path);


    //free
    free(returned_String_Array);
    free(returned_coverted_Array);
    free(taillex);
    free(tailley);
    free(nbDim);

}
