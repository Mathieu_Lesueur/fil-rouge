#include <stdio.h>
#include <stdlib.h>

#include "../_OUTILS/maths/maths_tools.h"
#include "images.h"


/*

Fichier de teste de la partie image et maths tools

Ugo

*/

int main()
{

    static short*** returnedArray;
    short* taillex;
    short* tailley;
    short* nbDim;

    taillex = malloc(sizeof(short));
    tailley = malloc(sizeof(short));
    nbDim   = malloc(sizeof(short));


    if(nbDim == NULL || tailley == NULL || nbDim == NULL  )
    {
            printf("%s \n","                       MALLOC FAIL");
            exit(1);

    }

    printf("TEST ouverture fichier inexistant : ");
    fflush(stdout);

    printf("%d \n", NULL == ouvir_fichier_r("TEST_NB/51.txt",0) );
    fflush(stdout);
    
    printf("TEST ouverture Image sous forme de tableau : \n");
    fflush(stdout);




    returnedArray = ReadImages("TEST_NB/51.txt",taillex,tailley,nbDim);


    printf("\n Taille X = %d",*taillex);
    printf("\n Taille Y =%d",*tailley);
    printf("\n nbDim = %d \n",*nbDim);
    //printf("%d",);

    printf("Affichage tableau apres remplissage : \n");
    fflush(stdout);

    

        for(short r = 0; r<*nbDim; r++)
        {
            printf("\n Dimention = %d \n",r);
            printf("\n taillex = %d \n",*taillex);
            printf("\n tailley = %d \n",*tailley);


            for(short x = 0; x<*taillex; x++)
            {   
                for(short y = 0; y<*tailley; y++)
                {
 
                    printf("%d      ", returnedArray[x][y][r]);
                }
                

                printf("\n");
            }

        }
    
    for(int i = 0;i<256;i++)
    {
        int toconvert = i;
        short* test = getbitarray(8,toconvert);
        
        printf("\n %d = ",toconvert);

        for(short y = 0; y<8; y++)
        {
            printf("%d", test[y]);
        }
        printf(" Convertion inverse = %d",bit_to_int(test, 8));
        printf(" RGB convertion -> %d", convertRGB(i,i,i,3));
    }
    

    printf("\n TEST RGB conversion R=2 G=2 B=2 donne :%d  \n", convertRGB(25,136,89,2));
    printf("\n TEST RGB conversion R=255 G=128 B=64 donne :%d  \n", convertRGB(255,128,64,2));
    fflush(stdout);

    
    short** returned_coverted_Array = quantification_all_image(returnedArray,*taillex,*tailley,*nbDim,2);


    printf("\n TEST convertion image : \n");
    fflush(stdout);

    for(short x = 0; x<*taillex; x++)
    {   
        for(short y = 0; y<*tailley; y++)
        {

            printf("%d  ", returned_coverted_Array[x][y]);
        }
        

        printf("\n");
    }

    char *** returned_String_Array;

    printf("\n");


    printf("\n TEST image to char : \n ");
    fflush(stdout);

    returned_String_Array = image_to_string(returned_coverted_Array,*taillex,*tailley);
    

    printf("\n");


    for(short x = 0; x<*taillex; x++)
    {   
        for(short y = 0; y<*tailley; y++)
        {
            printf("%s  ",returned_String_Array[x][y]);
        }
        printf("\n");
    }

    printf("\n TEST image to file.\n ");
    fflush(stdout);
    
    Image_to_file(returned_String_Array,*taillex,*tailley,"./imageTEST.txt");

    convertir_image("TEST_NB/51.txt","./imageTEST2.txt", 3);




    printf("\n test linearisation : \n ");
    fflush(stdout);

    char** stringlinearised = linearisationMatrice(returned_String_Array, *taillex,*tailley);


    for(short i = 0; i< (*tailley) * (*taillex); i++)
    {
        printf("%s|",stringlinearised[i]);
    }
    printf("\n ");





}





