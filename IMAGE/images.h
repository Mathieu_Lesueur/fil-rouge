FILE * ouvir_fichier_r(char *path, int crashIfFail);

short ***ReadImages(char *path, short *ptaillex, short *ptailley, short *pnbDim);

short* getbitarray(short nbBits,short number);

int convertRGB(short R, short G, short B, int nombre_bits);

int** quantification_all_image(short ***image, int tailleX, int tailleY, int tailleRGB, int nb_bits_quantif);

char*** image_to_string(int **image, int tailleX, int tailleY);

char** linearisationMatrice(char*** image, int tailleX, int tailleY);

void Image_to_file(char*** image,short taillex,short tailley, char* path);

void convertir_image(char* original_path,char* nouveau_path, int nb_bits_convertion);

