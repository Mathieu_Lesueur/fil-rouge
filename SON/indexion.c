#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include <libgen.h>
#include <math.h>
#include <errno.h>
#include "../_OUTILS/autre/element.h"
#include "../_OUTILS/autre/sorted_list.h"
#include"indexion.h"
#include <unistd.h>

/**
 * Auteur : Mathieu Lesueur
 * Fonction : Sert a indexer et rechercher les fichiers audio
**/
char CURRENT_PATH[255];

//Fonction qui va renvoyer le nom du fichier avec l'extension .txt (prend un chemin en parametre)
char* nomfic(char* fic){
   
   char* nom = malloc(sizeof(char)*1000);

    nom = basename(fic);

    nom[strlen(nom)-strlen("bin")] = '\0';

    //on rajoute .txt
    strcat(nom,"txt");
    
    return nom;
}

//ressemble beaucoup a la fonction precedente mais enleve egalment le "descripteur-" et met .wav en extension
char* nomfic_wav(char* fic){
    char* nom = malloc(sizeof(char)*1000);

    nom = basename(fic);

    nom[strlen(nom)-strlen("txt")] = '\0';

    chopN(nom, strlen("descripteur-"));

    //on rajoute .txt
    strcat(nom,"wav");
    
    return nom;
}

//fonction qui tronque une partie (taille de l'entier en parametre) de la chaine de caractere en parametre
void chopN(char *str, size_t n)
{
    if((n != 0 && str != 0))
    {
        size_t len = strlen(str);
        if (n > len)
            return;  // Or: n = len;
        memmove(str, str+n, len - n + 1);
    }
}


//renvoie un tableau de int en deux dimensions
int** tab_2dim(int tailleX, int tailleY){

    int **returnedArray = (int **)malloc(tailleX * sizeof(int **));

    if (returnedArray == NULL)
    {
        printf("%s \n", "                       MALLOC returnedArray FAIL");
        exit(1);
    }

    for (int x = 0; x < tailleX; x++)
    {
        returnedArray[x] = (int *)malloc(tailleY * sizeof(int*));

        if (returnedArray[x] == NULL)
        {
            printf("%s \n", "                       MALLOC returnedArray FAIL");
            exit(1);
        }
    }

    return returnedArray;
}


//indexe un fichier rentré en parametre (le fichier doit etre un .bin)
int descripteur(char* fic){

    //ouverture du fichier bin
    FILE *fic_bin = fopen(fic, "rb");

    if(fic_bin == NULL){
        return -1;
    }

    //variable pour la taille du fichier
    long size;

    //variable pour le nom du fichier
    char* nom = nomfic(fic);


    char descripteur[1024];
    strcpy(descripteur,CURRENT_PATH);
    strcat(descripteur, "Descripteur/descripteur-");

    //On supprime un éventuel ancien descripteur
    char comm1[1035] = "rm ";
    
    strcat(comm1,descripteur);

    strcat(comm1, nom);
    //on empeche le message d'erreur si il n'y a pas de descripteur
    strcat(comm1, " 2> /dev/null");
    system(comm1);
    //on crée le fichier descripteur
    char comm2[1035] = "touch ";
    strcat(comm2, descripteur);
    strcat(comm2, nom);
    system(comm2);

    //on recupere le path du fichier descripteur que l'on vient de créer
    char path[1035];
    strcpy(path,descripteur);
    strcat(path, nom);

    //ouverture du fichier descripteur
    FILE *fic_txt = fopen(path, "w");

    if(fic_txt == NULL)
    {
        printf("\n [ERREUR] descripteur ligne 114: ouverture du fichier descripteur FAILED \n");
        exit(1);
    }

    //on va a la fin du fichier
    fseek (fic_bin, 0, SEEK_END);  
    //on recupere sa taille
    size = ftell(fic_bin);
    //on la divise par 8 car chaque double est codé sur 8 bits
    size = size/8;
    //on revient au debut
    fseek (fic_bin, 0, SEEK_SET);

    //on rajoute une fenetre si on ne tombe pas sur un nombre rond de fenetre
    long taillex = size / WINDOW_SIZE;
    if (size % WINDOW_SIZE != 0){
        taillex++;
    }

    //variable qui va nous permettre de stocker temporairement nos doubles
    char bytes[8];

    //on va au debut du fichier descripteur
    fseek (fic_txt, 0, SEEK_SET);
    //on écrit le nombre de fenetre
    fprintf(fic_txt,"%ld \n", taillex);
    //on écrit la taille des fenetre
    fprintf(fic_txt,"%d \n", WINDOW_SIZE);

    //on initialise une variable pour chaque intervalle possible
    int nb_moins_1_09 = 0;
    int nb_moins_09_08 = 0;
    int nb_moins_08_07 = 0;
    int nb_moins_07_06 = 0;
    int nb_moins_06_05 = 0;
    int nb_moins_05_04 = 0;
    int nb_moins_04_03 = 0;
    int nb_moins_03_02 = 0;
    int nb_moins_02_01 = 0;
    int nb_moins_01_0 = 0;
    int nb_plus_0_01 = 0;
    int nb_plus_01_02 = 0;
    int nb_plus_02_03 = 0;
    int nb_plus_03_04 = 0;
    int nb_plus_04_05 = 0;
    int nb_plus_05_06 = 0;
    int nb_plus_06_07 = 0;
    int nb_plus_07_08 = 0;
    int nb_plus_08_09 = 0;
    int nb_plus_09_1 = 0;

    int indice = 0;

    //pour chaque double du fichier
    for(int i = 0; i < size + 100; i++){

        //on les recupere et on les met dans notre variable
        fread(&bytes, 8, 1, fic_bin);
        
        //selon sa valeur on incremente de 1 le nombre de valeur de cet intervalle
        if(-1 <= *((double*)bytes) && -0.9 > *((double*)bytes)){
            nb_moins_1_09++;
        }else if(-0.9 <= *((double*)bytes) && -0.8 > *((double*)bytes)){
            nb_moins_09_08++;
        }else if(-0.8 <= *((double*)bytes) && -0.7 > *((double*)bytes)){
            nb_moins_08_07++;
        }else if(-0.7 <= *((double*)bytes) && -0.6 > *((double*)bytes)){
            nb_moins_07_06++;
        }else if(-0.6 <= *((double*)bytes) && -0.5 > *((double*)bytes)){
            nb_moins_06_05++;
        }else if(-0.5 <= *((double*)bytes) && -0.4 > *((double*)bytes)){
            nb_moins_05_04++;
        }else if(-0.4 <= *((double*)bytes) && -0.3 > *((double*)bytes)){
            nb_moins_04_03++;
        }else if(-0.3 <= *((double*)bytes) && -0.2 > *((double*)bytes)){
            nb_moins_03_02++;
        }else if(-0.2 <= *((double*)bytes) && -0.1 > *((double*)bytes)){
            nb_moins_02_01++;
        }else if(-0.1 <= *((double*)bytes) && 0 > *((double*)bytes)){
            nb_moins_01_0++;
        }else if(0 <= *((double*)bytes) && 0.1 > *((double*)bytes)){
            nb_plus_0_01++;
        }else if(0.1 <= *((double*)bytes) && 0.2 > *((double*)bytes)){
            nb_plus_01_02++;
        }else if(0.2 <= *((double*)bytes) && 0.3 > *((double*)bytes)){
            nb_plus_02_03++;
        }else if(0.3 <= *((double*)bytes) && 0.4 > *((double*)bytes)){
            nb_plus_03_04++;
        }else if(0.4 <= *((double*)bytes) && 0.5 > *((double*)bytes)){
            nb_plus_04_05++;
        }else if(0.5 <= *((double*)bytes) && 0.6 > *((double*)bytes)){
            nb_plus_05_06++;
        }else if(0.6 <= *((double*)bytes) && 0.7 > *((double*)bytes)){
            nb_plus_06_07++;
        }else if(0.7 <= *((double*)bytes) && 0.8 > *((double*)bytes)){
            nb_plus_07_08++;
        }else if(0.8 <= *((double*)bytes) && 0.9 > *((double*)bytes)){
            nb_plus_08_09++;
        }else if(0.9 <= *((double*)bytes) && 1 >= *((double*)bytes)){
            nb_plus_09_1++;
        }

        indice++;

        //une fois une fenetre passée on écrit les valeurs de ses intervalles
        if (indice % WINDOW_SIZE == 0){
            indice = 0;
            fprintf(fic_txt,"%d \n", nb_moins_1_09);
            fprintf(fic_txt,"%d \n", nb_moins_09_08);
            fprintf(fic_txt,"%d \n", nb_moins_08_07);
            fprintf(fic_txt,"%d \n", nb_moins_07_06);
            fprintf(fic_txt,"%d \n", nb_moins_06_05);
            fprintf(fic_txt,"%d \n", nb_moins_05_04);
            fprintf(fic_txt,"%d \n", nb_moins_04_03);
            fprintf(fic_txt,"%d \n", nb_moins_03_02);
            fprintf(fic_txt,"%d \n", nb_moins_02_01);
            fprintf(fic_txt,"%d \n", nb_moins_01_0);
            fprintf(fic_txt,"%d \n", nb_plus_0_01);
            fprintf(fic_txt,"%d \n", nb_plus_01_02);
            fprintf(fic_txt,"%d \n", nb_plus_02_03);
            fprintf(fic_txt,"%d \n", nb_plus_03_04);
            fprintf(fic_txt,"%d \n", nb_plus_04_05);
            fprintf(fic_txt,"%d \n", nb_plus_05_06);
            fprintf(fic_txt,"%d \n", nb_plus_06_07);
            fprintf(fic_txt,"%d \n", nb_plus_07_08);
            fprintf(fic_txt,"%d \n", nb_plus_08_09);
            fprintf(fic_txt,"%d \n", nb_plus_09_1);

            //on remet les valeurs a 0;
            nb_moins_1_09 = 0;
            nb_moins_09_08 = 0;
            nb_moins_08_07 = 0;
            nb_moins_07_06 = 0;
            nb_moins_06_05 = 0;
            nb_moins_05_04 = 0;
            nb_moins_04_03 = 0;
            nb_moins_03_02 = 0;
            nb_moins_02_01 = 0;
            nb_moins_01_0 = 0;
            nb_plus_0_01 = 0;
            nb_plus_01_02 = 0;
            nb_plus_02_03 = 0;
            nb_plus_03_04 = 0;
            nb_plus_04_05 = 0;
            nb_plus_05_06 = 0;
            nb_plus_06_07 = 0;
            nb_plus_07_08 = 0;
            nb_plus_08_09 = 0;
            nb_plus_09_1 = 0;

        }
        
        
        
        
    }
    
    //on imprime les valeurs de la derniere fenetre
    fprintf(fic_txt,"%d \n", nb_moins_1_09);
    fprintf(fic_txt,"%d \n", nb_moins_09_08);
    fprintf(fic_txt,"%d \n", nb_moins_08_07);
    fprintf(fic_txt,"%d \n", nb_moins_07_06);
    fprintf(fic_txt,"%d \n", nb_moins_06_05);
    fprintf(fic_txt,"%d \n", nb_moins_05_04);
    fprintf(fic_txt,"%d \n", nb_moins_04_03);
    fprintf(fic_txt,"%d \n", nb_moins_03_02);
    fprintf(fic_txt,"%d \n", nb_moins_02_01);
    fprintf(fic_txt,"%d \n", nb_moins_01_0);
    fprintf(fic_txt,"%d \n", nb_plus_0_01);
    fprintf(fic_txt,"%d \n", nb_plus_01_02);
    fprintf(fic_txt,"%d \n", nb_plus_02_03);
    fprintf(fic_txt,"%d \n", nb_plus_03_04);
    fprintf(fic_txt,"%d \n", nb_plus_04_05);
    fprintf(fic_txt,"%d \n", nb_plus_05_06);
    fprintf(fic_txt,"%d \n", nb_plus_06_07);
    fprintf(fic_txt,"%d \n", nb_plus_07_08);
    fprintf(fic_txt,"%d \n", nb_plus_08_09);
    fprintf(fic_txt,"%d \n", nb_plus_09_1);
    
    //On ferme les fichiers
    fclose(fic_bin);
    fclose(fic_txt);
}



void init_chemin(char* path)
{
    strcpy(CURRENT_PATH,path);
}


//renvoie une liste des fichiers contenant le fichier en parametre ou bien les fichiers ressemblant selon le mode renseigné (0 = contenant, 1 = ressemablant)
SORTED_LIST comparaison(char* fic, int mode){

    if(mode != 0 && mode != 1){
        printf("Erreur le mode renseigné n'est pas valide \n");
        exit(1);
    }

    //la liste que l'on va renvoyer
    SORTED_LIST maliste = init_FILE();		
    
    //temps d'une fenetre quand on a 100 points par fenetre
    float sec_par_fen = 0.00616787342;

    //le nom du fichier a comparer
    char* temp = nomfic(fic);

    //on recupere le path du fichier descripteur que l'on vient de créer
    char path[1035];
    strcpy(path,CURRENT_PATH);
	strcat(path,"Descripteur/descripteur-");

    strcat(path, temp);

    //ouverture du fichier descripteur
    FILE *fic_org = fopen(path, "r");


    if (fic_org == NULL) {

        char cwd[4000];
        if (getcwd(cwd, sizeof(cwd)) != NULL) {
            printf("Current working dir: %s\n", cwd);
        } else {
            perror("getcwd() error");
        }

        printf("Failed to open the descriptor file %s   \n",path );
        exit(1);
    }



    //fichier pour comparer
    FILE *fic_temp = NULL;

    //nombre et taille des fenetres du fichier a comparer
    int nbfenetre_org;
    int sizefenetre_org;
    fscanf(fic_org, "%d", &nbfenetre_org);
    fscanf(fic_org, "%d", &sizefenetre_org);

    //tableau qui va stocker le descripteur du fichier a comparer
    int** desc_org = tab_2dim(nbfenetre_org, 20);

    //variable d'indice ou utile pour la comparaison
    int indice_fen = 0;
    int indice = 0;
    int lu = 0;

    //tant que on a pas fini de lire le descripteur
    while(lu == 0){
        //on lit les valeurs et on les stocke
        fscanf(fic_org, "%d", &desc_org[indice_fen][indice]);

        indice++;
        
        //on avance d'une fenetre
        if(indice == 20){
            indice = 0;
            indice_fen++;
        }

        //quand on a finit de lire on sort
        if(indice_fen == nbfenetre_org){
            lu = 1;
        }
    }

    //on ferme le fichier
    fclose(fic_org);

    //fichier pour la commande ls
    char result[1035];

    char commande[255];

    strcpy(commande,"/bin/ls ");
	strcat(commande,CURRENT_PATH);
	strcat(commande,"Descripteur/*");

    //on recupere la liste de tous les descripteurs audio
    FILE* fp = popen(commande, "r");



    if (fp == NULL) {
        printf("Failed to run command \n" );
        exit(1);
    }

    //variable pour verifier que l'on applique pas la comparaison a soi meme
    int comp = 0;
    
    //tant qu'il reste des resultat a comparer
    while (fgets(result, sizeof(result), fp) != NULL) { 


        comp = 0;
        long i = 0, caractere_actuel = 0;
 
        //on enleve le \n qui se trouve a la fin des lignes de resultat du ls
        do{
            caractere_actuel = result[i];
            i++;
        }
        while (caractere_actuel != '\n');
    
        i--;

        result[i] = '\0';

        for(int i = 0; i < strlen(result); i++){
            if(result[i] != path[i]){
                comp = 1;
            }
        }

        
        //si le resultat du ls est bien different du fichier a comparer
        if(comp == 1){

            //on ouvre le resultat
            fic_temp = fopen(result, "r");

            if (fic_temp == NULL) {
                printf("Failed to open file \n" );
                exit(1);
            }

            //taille et nombre de fenetres
            int nbfenetre_temp;
            int sizefenetre_temp;
            fscanf(fic_temp, "%d", &nbfenetre_temp);
            fscanf(fic_temp, "%d", &sizefenetre_temp);

            
    
            //on cree un tableau de taille necessaire
            int** desc_temp = tab_2dim(nbfenetre_temp, 20);
            

            //variable d'indice et de verification de lecture
            indice_fen = 0;
            indice = 0;
            lu = 0;

            //tant que on a pas fini de lire le resultat
            while(lu == 0){
                //on stocke ses valeurs
                fscanf(fic_temp, "%d", &desc_temp[indice_fen][indice]);

                indice++;

                //on change de fenetre
                if(indice == 20){
                    indice = 0;
                    indice_fen++;
                }

                //quand on a fini de lire on sort
                if(indice_fen == nbfenetre_temp){
                    lu = 1;
                }
                
            }

            //variable pour les boucles
            int indice_petit = 0;

            //variable pour calculer la ressemblance des fichiers
            double ressemblance_fen = 0;
            double proba;

            //si on est dans le mode contenant
            if(mode == 0){

                //si le fichier d'origine est plus petit que le fichier auquel on le compare
                if(nbfenetre_org < nbfenetre_temp){
                    
                    //pour toutes les fenetres du fichier d'origine
                    for(int i = 0; i < nbfenetre_temp; i++){

                        //pour tester toutes les sequences du fichier le plus grands de la même taille que le fichier le plus petit
                        for(int j = i; j < nbfenetre_org + i - 1; j++){

                            //si on arrive a la fin on sort
                            if(nbfenetre_org + i >= nbfenetre_temp){
                                break;
                            }

                            //pour chauqe intervalles
                            for (int k = 0; k < 20; k++){

                                //si les valeurs des deux fichiers sont differentes de 0
                                if (desc_temp[j][k] != 0 && desc_org[indice_petit][k] != 0){

                                    //on calcule la valeur de divergence grace a la formule de Kullback_Leibleir
                                    proba = (double)desc_temp[j][k] * log((double)((double)desc_temp[j][k] / (double)desc_org[indice_petit][k]));
                                    proba = (double)(proba /100);
                                    ressemblance_fen += proba;

                                }
                            }

                            //on increment l'indice de 1
                            indice_petit++;
                        }
                        
                        //on remet l'indice a 0
                        indice_petit = 0;

                        //on calcule la divergence sur l'ensemble des fenetres que l'on vient de passer
                        ressemblance_fen = (double)(ressemblance_fen / nbfenetre_org);

                        /*si la divergence est de 0 (a cause de valeurs nulles) on met le resultat a 1 car vu que l'on calcule la
                        divergence un resultat de 0 voudrait dire deux fichiers totalement identique or on empeche lacomparaison d'un fichier aveclui même
                        */
                        if (ressemblance_fen == 0)
                        {
                            ressemblance_fen = 1;
                        }

                        //si le fichier ressemble assez on ajoute son chemin et le timecode de la correspondance dans la liste
                        if(ressemblance_fen < 0.20){

                            //on calcule le timecode
                            int temps = i * sec_par_fen;

                            //on recupere son chemin
                            char* nom_temp = nomfic_wav(result);
                            nom_temp = nomfic_wav(result);
                            char chemin[1035] = "DATA_FIL_ROUGE_DEV/TEST_SON/";
                            strcat(chemin, nom_temp);

                            //on l'ajoute a la liste (si il y est deja il ne sera pas rajouté)
                            ELEMENT* element = malloc(sizeof(ELEMENT));
                            if(element != NULL){

                                affectELEMENT(element,temps,chemin);
                                maliste = emPILE(maliste, *element);
                            }
                        }
                        
                        //on reinitialise la ressemblance
                        ressemblance_fen = 0;
                    }
                    //on ferme le fichier avec lequel on comparait 
                    fclose(fic_temp);
                }

            //si on est dans le mode de comparaison
            }else if(mode == 1){

                //on cree le chemin de base de réponse
                char chemin[1035] = "DATA_FIL_ROUGE_DEV/TEST_SON/";

                //on initialise une variable a 0 pour savoir combien de ressemblance on a trouvée dans un fichier
                int c = 0;

                //si le fichier séléctionné est plus petit
                if(nbfenetre_org <= nbfenetre_temp){
                    
                    //pour chaque fenetre du fichier auquel on le compare
                    for(int i = 0; i < nbfenetre_temp; i++){

                        //pour tester toutes les sequences du fichier le plus grands de la même taille que le fichier le plus petit
                        for(int j= i; j < nbfenetre_org + i - 1; j++){

                            //si on arrive a la fin on sort
                            if(nbfenetre_org + i >= nbfenetre_temp){
                                break;
                            }

                            //pour chaque intervalle 
                            for (int k = 0; k < 20; k++){

                                //si les deux valeurs sont différentes de 0
                                if (desc_temp[j][k] != 0 && desc_org[indice_petit][k] != 0){

                                    //voir le mode 1
                                    proba = (double)desc_temp[j][k] * log((double)((double)desc_temp[j][k] / (double)desc_org[indice_petit][k]));
                                    proba = (double)(proba /100);
                                    ressemblance_fen += proba;
                                }
                            }

                            //on augmente l'indice
                            indice_petit++;
                        }
                        
                        //on reinitialise l'indice
                        indice_petit = 0;

                        //on calcule la divergence
                        ressemblance_fen = (double)(ressemblance_fen / nbfenetre_org);

                        //si c'est 0 (impossible normalement) on le poasse a 1 pour eviter d'avoir un 100%
                        if (ressemblance_fen == 0)
                        {
                            ressemblance_fen = 1;
                        }

                        //si cela ressemble assez
                        if(ressemblance_fen < 0.20){

                            //si c'est la premiere resseblance que l'on trouve dans ce fichier
                            if(c == 0){
                                //on recupere le chemin du fichier
                                char* nom_temp = nomfic_wav(result);
                                nom_temp = nomfic_wav(result);
                                strcat(chemin, nom_temp);
                                c++;
                            }
                        }
                        
                        //on reinitialise a 0
                        ressemblance_fen = 0;
                    }
                    //on ferme le fichier
                    fclose(fic_temp);

                    //si on a trouvé au moins une ressemblance
                    if(c > 0){
                        //on calcule quel est le pourcentage par ra pport a la taille totale du fichiers puis on l'ajoute a la liste
                        ressemblance_fen = (double)(c * nbfenetre_org)/nbfenetre_temp * 100;
                        maliste = init_FILE();
                        ELEMENT* element = malloc(sizeof(ELEMENT));
                        if(element != NULL){
                            affectELEMENT(element,ressemblance_fen,chemin);
                            maliste = emPILE(maliste, *element);
                        }
                    }
                //sinon on fait le meme traitement mais en inversant les fichiers donc cf le if precedent
                }else{

                    for(int i = 0; i < nbfenetre_org; i++){

                        for(int j = i; j < nbfenetre_temp + i - 1; j++){

                            if(nbfenetre_temp + i >= nbfenetre_org){
                                break;
                            }

                            for (int k = 0; k < 20; k++){

                                if (desc_org[j][k] != 0 && desc_temp[indice_petit][k] != 0){
                                    proba = (double)desc_temp[indice_petit][k] * log((double)((double)desc_temp[indice_petit][k] / (double)desc_org[j][k]));
                                    proba = (double)(proba /100);
                                    ressemblance_fen += proba;
                                }
                            }

                            indice_petit++;
                        }
                        

                        indice_petit = 0;

                        ressemblance_fen = (double)(ressemblance_fen / nbfenetre_temp);

                        if (ressemblance_fen == 0)
                        {
                            ressemblance_fen = 1;
                        }

                        if(ressemblance_fen < 0.20){
                            if(c == 0){
                                char* nom_temp = nomfic_wav(result);
                                nom_temp = nomfic_wav(result);
                                strcat(chemin, nom_temp);
                                c++;
                            }
                        }
                        

                        ressemblance_fen = 0;
                    }
                    fclose(fic_temp);

                    if(c > 0){
                        ressemblance_fen = (double)(c * nbfenetre_temp)/nbfenetre_org * 100;
                        maliste = init_FILE();
                        ELEMENT* element = malloc(sizeof(ELEMENT));
                        if(element != NULL){
                            affectELEMENT(element,ressemblance_fen,chemin);
                            maliste = emPILE(maliste, *element);
                        }
                    }
                }
            }
                
        }
    }
    //a la fin on retourne la liste des fichiers
    return maliste;
}