package test;

import controler.ControlerIdentificationAdministrateur;
import controler.ControlerModifierConfiguration;
import controler.ControlerRequeteSimilarite;
import controler.ControlerSauvegardeRequete;
import controler.ControlleurIndexation;
import controler.ControlleurRechercheCritereImage;
import controler.ControlleurRechercheCritereTexte;
import vue.BoundaryIdentificationAdministrateur;
import vue.BoundaryMenu;
import vue.BoundaryModifierConfiguration;
import vue.BoundaryRechercheCritereImage;
import vue.BoundaryRechercheCritereTexte;
import vue.BoundaryRequeteSimilarite;
import vue.BoundarySauvegardeRequete;
import vue.BoundarySelectionMoteur;

public class Main {

	public static void main(String[] args) {
		
		BoundarySauvegardeRequete bSauvegarde = new BoundarySauvegardeRequete(new ControlerSauvegardeRequete());
		BoundarySelectionMoteur bMoteur = new BoundarySelectionMoteur();
		BoundaryModifierConfiguration bConfig = new BoundaryModifierConfiguration(new ControlerModifierConfiguration());
		
		
		BoundaryRechercheCritereTexte bTexte = new BoundaryRechercheCritereTexte
				(new ControlleurIndexation(),new ControlleurRechercheCritereTexte(),
				bMoteur, bSauvegarde);
		
		BoundaryRechercheCritereImage bImage = new BoundaryRechercheCritereImage(new ControlleurRechercheCritereImage(),
				new ControlleurIndexation(),bMoteur,bSauvegarde);				
		
				
		BoundaryRequeteSimilarite bSimilarite = new BoundaryRequeteSimilarite(new ControlerRequeteSimilarite(),
				bMoteur, bSauvegarde);
		
		BoundaryIdentificationAdministrateur bAdmin = new BoundaryIdentificationAdministrateur(bConfig,new ControlerIdentificationAdministrateur());
		
		BoundaryMenu menu = new BoundaryMenu(bTexte, bImage,bSimilarite, bAdmin);
		menu.afficherMenu();

	}

}
