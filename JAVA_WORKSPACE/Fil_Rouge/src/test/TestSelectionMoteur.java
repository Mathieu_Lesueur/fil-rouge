package test;

import vue.BoundarySelectionMoteur;

public class TestSelectionMoteur {
	public static void main(String args[]) {
		BoundarySelectionMoteur boundarySelectionMoteur = new BoundarySelectionMoteur();
		int choix = boundarySelectionMoteur.selectionMoteur();
		if (choix==1 || choix ==2) {
			System.out.println("Le moteur choisi est le moteur num�ro " + choix);
		}else {
			System.out.println("Vous avez choisi le mode multi moteur");
		}
	}
}
