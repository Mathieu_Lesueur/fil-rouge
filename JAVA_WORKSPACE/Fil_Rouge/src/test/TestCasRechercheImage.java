package test;
import controler.ControlerSauvegardeRequete;
import controler.ControlleurIndexation;
import controler.ControlleurRechercheCritereImage;
import vue.BoundaryRechercheCritereImage;
import vue.BoundarySauvegardeRequete;
import vue.BoundarySelectionMoteur;

public class TestCasRechercheImage {

	public static void main(String[] args) {
		BoundaryRechercheCritereImage boundaryRechercheImage=new BoundaryRechercheCritereImage(new ControlleurRechercheCritereImage()
				,new ControlleurIndexation(),new BoundarySelectionMoteur()
				,new BoundarySauvegardeRequete(new ControlerSauvegardeRequete()));
		boundaryRechercheImage.rechercheParCritereComplexe();

	}

}
