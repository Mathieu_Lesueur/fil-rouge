package test;

import controler.ControlerRequeteSimilarite;
import controler.ControlerSauvegardeRequete;
import vue.BoundaryRequeteSimilarite;
import vue.BoundarySauvegardeRequete;
import vue.BoundarySelectionMoteur;

public class TestCasRequeteSimilarite {
	public static void main(String args[]) {
		BoundaryRequeteSimilarite boundaryRequeteSimilarite = 
				new BoundaryRequeteSimilarite(new ControlerRequeteSimilarite(), 
				new BoundarySelectionMoteur(),
				new BoundarySauvegardeRequete(new ControlerSauvegardeRequete()));
		boundaryRequeteSimilarite.requeteSimilarite();
	}
}
