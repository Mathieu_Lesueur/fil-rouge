package test;

import java.util.TreeSet;

import controler.ControlerSauvegardeRequete;
import model.Resultat;
import vue.BoundarySauvegardeRequete;

public class TestCasSauvegarderRequete {
	public static void main(String args[]) {
		ControlerSauvegardeRequete controlerSauvegardeRequete = new ControlerSauvegardeRequete();
		BoundarySauvegardeRequete boundarySauvegardeRequete=new BoundarySauvegardeRequete(controlerSauvegardeRequete);
		TreeSet<Resultat<String,Float>> fichiers = new TreeSet<>();
		Resultat<String, Float> resultat = new Resultat<String, Float>("Ficher1",35.0F);
		fichiers.add(resultat);
		boundarySauvegardeRequete.sauvegarderRequete("Requete",fichiers);
	}
}
