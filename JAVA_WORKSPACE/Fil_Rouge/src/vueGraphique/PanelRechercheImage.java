package vueGraphique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import com.sun.jna.platform.linux.LibC.Sysinfo;

import controler.ControlerSauvegardeRequete;
import controler.ControlleurIndexation;
import controler.ControlleurRechercheCritereImage;
import model.ComparateurPolarite;
import model.ComparateurResultat;
import model.CritereImage;
import model.Polarite;
import model.Resultat;
import model.ThreadIndexation;

public class PanelRechercheImage extends JPanel{
	private static final long serialVersionUID = 1L;
	
	//controller
	private ControlleurRechercheCritereImage ControlleurRechercheCritereImage = new ControlleurRechercheCritereImage();
	private ControlleurIndexation controlIndexation = new ControlleurIndexation();
	private ControlerSauvegardeRequete controlerSauvegardeRequete = new ControlerSauvegardeRequete();
	
	//polices
	private Font policeTitre = new Font("Calibri",Font.BOLD,24);
	private Font policeParagraphe = new Font("Calibri", Font.HANGING_BASELINE, 16);
	
	//JTextArea
	private JTextArea resultats = new JTextArea();
	//ComboBox
	private JComboBox<String> choixMoteur = new JComboBox<>();
	
	private JLabel titre;
	private JButton bouton_retour;
	
	public PanelRechercheImage() {
		this.set_home();
	}
	
	public void set_home() 
	{
		
		this.removeAll();
		this.setLayout(new BorderLayout());
		titre= new JLabel("Recherche image",SwingConstants.CENTER);
		titre.setFont(policeTitre);
		this.add(titre,BorderLayout.NORTH);
		//Panel central
		JPanel centre = new JPanel(new GridLayout(1,2));
		//Sous panel centraux
		JPanel centreLeft = new JPanel();
		JPanel centreRight = new JPanel();
		//Panel de gauche
		centreLeft.setLayout(new BorderLayout());
		JLabel requete = new JLabel("Requete :");
		requete.setFont(policeParagraphe);
		JTextArea saisie = new JTextArea();
		saisie.setText("+122 123 124 +124 123 213 -213 213 145");
		saisie.addMouseListener(new MouseListener() {			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(saisie.getText().contentEquals("+122 123 124 +124 123 213 -213 213 145")) {
					saisie.setText("");
				}				
			}
		});
		Border border = BorderFactory.createLineBorder(Color.RED);
		saisie.setBorder(border);
		JButton lancer = new JButton("Lancer recherche");
		lancer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//selection moteur
				int moteur = choixMoteur.getSelectedIndex()+1;
				
				//lancement indexation
				ThreadIndexation myThread = new ThreadIndexation("Image",controlIndexation);
				
				//Construction liste des criteres
				String requete = saisie.getText();
				List<String> mots = new ArrayList<>();
				
				
				String str[] = requete.split(" ");
				
				mots = Arrays.asList(str);
				
				
				int nbCritere = mots.size()/3;
				ArrayList<CritereImage> criteres = new ArrayList<>();
				
				System.out.println(mots);
				System.out.println(nbCritere);
				
				if(nbCritere ==0) 
				{
					AfficherErreur.error("IL FAUT PLUS DE CRITERES ");
				}
				else
				{

	
						for(int i=0;i<nbCritere;i++) {
							
							System.out.println("HEY");
	
							CritereImage critere = null;
							Polarite polarite=null;
							int r=-1;
							int g=-1;
							int b=-1;
							
							
							for(int j=0+i*3;j<=j+2;j++)
							{	
								
								if(j==0+i*3 && mots.get(j).startsWith("+"))
								{
									polarite = Polarite.PRESENT;
									r=Integer.valueOf(mots.get(j).substring(1));
									
								}else if(j==0+i*3 && mots.get(j).startsWith("-"))
								{
									
									polarite = Polarite.ABSENT;
									r=Integer.valueOf(mots.get(j).substring(1));		
									
								}
								if(j==1+i*3) 
								{
									g=Integer.valueOf(mots.get(j));							
								}
								if(j==2+i*3) 
								{
									b=Integer.valueOf(mots.get(j));							
								}
								
							}
							critere = new CritereImage(polarite, r, g, b);
							criteres.add(critere);
						}
						
						//lancement de la recherche
						myThread.start();
						//trier la liste des criteres pour mettre les critere dont leur polarite est + en avant
						// pour verifier si dans la liste des criteres y'a pas des polarité positives au debut  donc c'est pas la peine d'effectuer la recherche
						Collections.sort(criteres, new ComparateurPolarite());
						// si y'a pas de polarité + alors on fait pas la recherche
						// on sort du programme
						
						System.out.println("TEST1");
						
						for(CritereImage c : criteres)
						{
							System.out.println(c);
	
						}
						
						
						System.out.println(criteres);
						System.out.println("TEST01");
	
						
						
						if(criteres.get(0).getPolarite() != Polarite.PRESENT) {
							AfficherErreur.error("Aucune polarite positive presente : recherche annulee");
						}		
						
						System.out.println("TEST2");
	
						//tant que l'indexation n'est pas terminé , il faut attendre
						while(myThread.getState() != Thread.State.TERMINATED) {}
						TreeSet<Resultat<String,Float>> result = new TreeSet<>(new ComparateurResultat());
						if (!criteres.isEmpty()) {
							
							System.out.println("TEST 3");
	
							
							result=ControlleurRechercheCritereImage.rechercheImage(criteres,moteur);
							//result.add(new Resultat<String, Float>("test",0.05F));
							System.out.println("TEST 4");
							
							Boolean choix=AfficherSauvegarde.info("Requete critere image",result,controlerSauvegardeRequete);
							if(!choix) {
								if(result != null)
								{
									if (!result.isEmpty()) {
	//									String res = "";
	//									for(Resultat<String,Float> r : result){
	//										res+="Result : " + r.getNom() + " " + r.getNombre().intValue();								
	//									}
	//									resultats.setText(res);
										setResultatMenu(result);
									}else {
										AfficherErreur.error("Aucun document ne correspond a ces criteres !");							
									}
								}else {
									AfficherErreur.error("Aucun document ne correspond a ces criteres !");							
								}
							}
						}else {
							AfficherErreur.error("Liste de critere vide");					
						}
					
				}

				
			}});
		centreLeft.add(requete,BorderLayout.NORTH);
		centreLeft.add(saisie,BorderLayout.CENTER);
		centreLeft.add(lancer,BorderLayout.SOUTH);
		//Panel de droite
		centreRight.setLayout(new BorderLayout());
		JLabel resultat = new JLabel("Resultats :");
		resultat.setFont(policeParagraphe);
		resultats.setFocusable(false);
		//Choix moteur
		JPanel moteur = new JPanel();
		moteur.setLayout(new GridLayout(1,2));
		JLabel selectionMoteur = new JLabel("Selection moteur :");
		selectionMoteur.setFont(policeParagraphe);
		choixMoteur.addItem("1");
		choixMoteur.addItem("2");
		choixMoteur.addItem("Tous");
		moteur.add(selectionMoteur);
		moteur.add(choixMoteur);
		//ajout panel cente right
		centreRight.add(resultat,BorderLayout.NORTH);
		centreRight.add(resultats,BorderLayout.CENTER);
		centreRight.add(moteur,BorderLayout.SOUTH);
		//Ajout au panel central
		centre.add(centreLeft);
		centre.add(centreRight);
		//Ajout des sous panels au panel principal
		this.add(centre,BorderLayout.CENTER);
		this.setVisible(true);
	}
	
	public void setResultatMenu(TreeSet<Resultat<String,Float>> results)
	{
		this.removeAll();

		//-------------------
		titre = new JLabel("Recherche image",SwingConstants.CENTER);
		bouton_retour = new JButton("Retour");
		
		//--------------------
		titre.setFont(policeTitre);
		this.setLayout(new BorderLayout());

		//-------------------
		
		this.add(titre,BorderLayout.NORTH);
		this.add(new PanelResultats(results, false),BorderLayout.CENTER);
		this.add(bouton_retour,BorderLayout.SOUTH);

		
		
		//--------------------

		bouton_retour.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				set_home();
			}
		  }
		);
	}

}
