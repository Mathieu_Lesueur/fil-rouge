package vueGraphique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.TreeSet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import controler.ControlerRequeteSimilarite;
import controler.ControleurComparaisonSon;
import model.Resultat;

public class PanelRequeteSimilarite extends JPanel {
	
	
	private static final long serialVersionUID = 1L;

	private ControlerRequeteSimilarite controlerRequeteSimilarite;
	private ControleurComparaisonSon comparaisonSon;

	
	private Font policeTitre = new Font("Calibri",Font.BOLD,24);
	private Font policeParagraphe = new Font("Calibri", Font.HANGING_BASELINE, 16);

	private JLabel titre;
	
	private JButton bouton_image;
	private JButton bouton_texte;
	private JButton bouton_son;
	private JButton bouton_retour;


	private GridLayout boutons_layout;
	private JPanel centre;
	private JPanel moteur;
	private JLabel selectionMoteur;
	private JFileChooser fileChooser;
	private JComboBox<String> choixMoteur;


	public PanelRequeteSimilarite()
	{
		comparaisonSon= new ControleurComparaisonSon();
		controlerRequeteSimilarite = new ControlerRequeteSimilarite();
		set_home();
	}
	
	public void set_home()
	{
		

		this.removeAll();
		//-------------------
		titre = new JLabel("Recherche similarite",SwingConstants.CENTER);

		boutons_layout = new GridLayout(1,3);
		
		bouton_image = new JButton("IMAGE");
		bouton_texte = new JButton("TEXTE");
		bouton_son = new JButton("SON");
		
		centre = new JPanel(boutons_layout);
		choixMoteur = new JComboBox<>();
		selectionMoteur = new JLabel("Selection moteur :");
		moteur = new JPanel();

		//--------------------
		titre.setFont(policeTitre);
		this.setLayout(new BorderLayout());
		moteur.setLayout(new GridLayout(1,2));
		selectionMoteur.setFont(policeParagraphe);
		
		
		bouton_texte.setBackground(new Color(255,255,255));
		bouton_image.setBackground(new Color(255,255,255));
		bouton_son.setBackground(new Color(255,255,255));
		
		//-------------------
		
		this.add(titre,BorderLayout.NORTH);
		
		centre.add(bouton_texte);
		centre.add(bouton_image);
		centre.add(bouton_son);

		this.add(centre,BorderLayout.CENTER);
		
		choixMoteur.addItem("1");
		choixMoteur.addItem("2");
		choixMoteur.addItem("Tous");
		moteur.add(selectionMoteur);
		moteur.add(choixMoteur);

		this.add(moteur,BorderLayout.SOUTH);

		//--------------------
		
		bouton_texte.addActionListener( new ActionListener() {
																public void actionPerformed(ActionEvent e) 
																{
																	clickBoutonMenu(1);
																}
															  }
									   );

		bouton_image.addActionListener( new ActionListener() {
																public void actionPerformed(ActionEvent e) 
																{
																	clickBoutonMenu(2);
																}
															  }
										);


		bouton_son.addActionListener( new ActionListener() {
																public void actionPerformed(ActionEvent e) 
																{
																	clickBoutonMenu(3);
																}
															  }
										);


		
	}
	
	
	
	
	
	
	private void clickBoutonMenu(int bouton)
	{
		
		
		
		String directory_to_start;
		String curentDirectory = System.getProperty("user.dir");
		FileNameExtensionFilter filter;
		File file;
		TreeSet<Resultat<String,Float>> results = null;
		switch(bouton)
		{
			case 1:
				
				curentDirectory+= "/../../DATA_FIL_ROUGE_DEV/TEXTES/";
				filter = new FileNameExtensionFilter("TEXT FILES", "xml", "text");
				file = open_file_chooser(curentDirectory,filter);
				results = controlerRequeteSimilarite.requeteSimilariteTexte(choixMoteur.getSelectedIndex()+1,file);
	
				
			break;
			case 2:
				curentDirectory+= "/../../DATA_FIL_ROUGE_DEV/TEST_NB/";
				filter = new FileNameExtensionFilter("IAMGE FILES", "txt", "text");
				file = open_file_chooser(curentDirectory,filter);
				results = controlerRequeteSimilarite.requeteSimilariteImage(choixMoteur.getSelectedIndex()+1,file);


			break;
			case 3:
				
				curentDirectory+= "/../../DATA_FIL_ROUGE_DEV/TEST_SON/";
				filter = new FileNameExtensionFilter("SOUND FILES", "txt", "text");
				file = open_file_chooser(curentDirectory,filter);
				results = comparaisonSon.comparaisonSon(file.getAbsolutePath(), 1);

				
			break;
			default:
				System.out.println("Erreur requete similarite");				
				file = null;
		}
		
		
		if(file != null)
		{
			setResultatMenu(results);
		}
		
	}
	
	
	private File open_file_chooser(String path,FileNameExtensionFilter filter)
	{
				
		fileChooser = new JFileChooser(path);
		fileChooser.setFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(this); 
        
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try
            {
                return fileChooser.getSelectedFile();
            }catch (Exception e) {
				// TODO: handle exception
			}
        }
        
        
		return null;
	}
	
	
	
	
	public void setResultatMenu(TreeSet<Resultat<String,Float>> results)
	{
		this.removeAll();

		//-------------------
		titre = new JLabel("Recherche similarite",SwingConstants.CENTER);
		bouton_retour = new JButton("Retour");
		
		//--------------------
		titre.setFont(policeTitre);
		this.setLayout(new BorderLayout());

		//-------------------
		
		this.add(titre,BorderLayout.NORTH);
		this.add(new PanelResultats(results, true),BorderLayout.CENTER);
		this.add(bouton_retour,BorderLayout.SOUTH);

		
		
		//--------------------

		bouton_retour.addActionListener( new ActionListener() {
															public void actionPerformed(ActionEvent e) 
															{
																set_home();
															}
														  }
								);

	}


}
