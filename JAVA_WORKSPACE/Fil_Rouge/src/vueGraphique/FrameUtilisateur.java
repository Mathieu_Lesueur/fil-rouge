package vueGraphique;

import java.awt.GridLayout;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import testGraphique.OSValidator;

public class FrameUtilisateur extends JFrame{
	private static final long serialVersionUID = 1L;
	//panel du menu
	private PanelMenu panelMenu = new PanelMenu();
	private PanelRechercheTexte panelRechercheTexte = new PanelRechercheTexte();
	private PanelRechercheImage panelRechercheImage = new PanelRechercheImage();
	private PanelRequeteSimilarite panelRequeteSimilarite = new PanelRequeteSimilarite();
	private PanelAdministrateur panelAdministrateur =new PanelAdministrateur();
	public FrameUtilisateur() {
		
		text_system();
		
		
		this.getContentPane().setLayout(new GridLayout(1, 1));
		
		/**********BARRE ONGLETS *************************/
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Menu",null, panelMenu,
		                  "Menu");
		tabbedPane.addTab("Recherche texte", null, panelRechercheTexte,
		                  "Rechercher dans un texte");
		tabbedPane.addTab("Recherche image", null, panelRechercheImage,
		                  "Rechercher dans une image");
		tabbedPane.addTab("Requete similarite", null, panelRequeteSimilarite,
                "Comparer des fichiers texte/image/son");
		tabbedPane.addTab("Administrateur", null, panelAdministrateur,
                "Passere en mode admin");
		this.getContentPane().add(tabbedPane);
		this.setTitle("Moteur de recherche");
		this.setSize(900, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	
	private void text_system()
	{
		if(OSValidator.isUnix())
		{
			
			AfficherErreur.nice("Lancement de l'aplication sur syteme UNIX");							
			
		}
		else
		{
			AfficherErreur.error("Merci d'utiliser un OS sous Unix");							
			System.exit(1);

		}
			
		
		
	}
	
	
}
