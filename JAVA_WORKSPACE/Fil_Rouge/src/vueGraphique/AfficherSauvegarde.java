package vueGraphique;

import java.util.TreeSet;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controler.ControlerSauvegardeRequete;
import model.Resultat;

public class AfficherSauvegarde {
	
	public static boolean info(String requete,TreeSet<Resultat<String,Float>> resultat,ControlerSauvegardeRequete controlerSauvegardeRequete) {
	    JFrame frame = new JFrame("showMessageDialog");
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    if (JOptionPane.showConfirmDialog(frame, "Voulez-vous sauvegarder les resultats dans un fichier?", "Sauvegarde",
	            JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
	    	controlerSauvegardeRequete.sauvegarderRequete(requete, resultat);
	        return true;
	    } else {
	        return false;
	    }
	}
}
