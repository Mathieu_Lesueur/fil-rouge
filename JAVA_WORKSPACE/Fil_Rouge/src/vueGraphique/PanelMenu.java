package vueGraphique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class PanelMenu extends JPanel {
	private static final long serialVersionUID = 1L;
	
	//Font : police
	private Font policeTitre = new Font("Calibri",Font.BOLD,24);
	private Font policeParagraphe = new Font("Calibri", Font.HANGING_BASELINE, 16);
	
	//Ce panel est le panel menu afficher en premier
	public PanelMenu() {
		//BorderLayout permet de mettre des elements au nord sud est ouest et centre du panel
		this.setLayout(new BorderLayout());
		/*************PANEL NORD**********************/
		JPanel nord = new JPanel();
		//GridLayout divise le panel en une grille permettant de positionner les elements
		nord.setLayout(new GridLayout(1,2));
		//ajouter icone paul sabatier et upssitech au panel nord
		ImageIcon upssitech_icon = new ImageIcon("Docs\\logo_upssitech.png");
		ImageIcon ups_icon = new ImageIcon("Docs\\logo_UT3.jpg");
		JLabel upssitech = new JLabel(upssitech_icon);
		JLabel ups = new JLabel(ups_icon);
		nord.add(upssitech);
		nord.add(ups);		
		this.add(nord,BorderLayout.NORTH);
		this.setBackground(Color.WHITE);
		
		/*****************PANEL CENTRE*******************/
		JLabel titre = new JLabel("Bienvenue sur le moteur de recherche",SwingConstants.CENTER);
		titre.setFont(policeTitre);
		this.add(titre,BorderLayout.CENTER);
		
		/**************PANEL SUD********************/
		JLabel auteur = new JLabel("Realise par : ROUX Ugo - LLEDOS Guilhem - BAHROUN Hamza - LESUEUR Mathieu"
				+" - NGOMA-MBY Charvy",SwingConstants.CENTER);
		auteur.setFont(policeParagraphe);
		this.add(auteur,BorderLayout.SOUTH);
		this.setVisible(true);	
		
	}
}
