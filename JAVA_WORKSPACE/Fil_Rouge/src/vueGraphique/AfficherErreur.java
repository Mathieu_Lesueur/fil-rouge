package vueGraphique;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class AfficherErreur {
	
	public static void error(String message) {
	    JFrame frame = new JFrame(Messages.getString("AfficherErreur.0")); //$NON-NLS-1$
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    JOptionPane.showMessageDialog(frame,
	            message,
	            Messages.getString("AfficherErreur.1"), //$NON-NLS-1$
	            JOptionPane.ERROR_MESSAGE);
	}
	
	
	
	public static void nice(String message) {
	    JFrame frame = new JFrame(Messages.getString("AfficherErreur.0")); //$NON-NLS-1$
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    JOptionPane.showMessageDialog(frame,
	            message,
	            Messages.getString("NOTE"), //$NON-NLS-1$
	            JOptionPane.INFORMATION_MESSAGE);
	}

	
}
