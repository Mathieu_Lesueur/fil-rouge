package vueGraphique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import controler.ControlerIdentificationAdministrateur;
import controler.ControlerModifierConfiguration;

public class PanelAdministrateur extends JPanel {
	private static final long serialVersionUID = 1L;

	/* FIELDS */
	private int nbEssais = 0;

	/* Controller */
	private ControlerIdentificationAdministrateur controlerIdentificationAdministrateur;
	private ControlerModifierConfiguration controlerModifierConfiguration;

	/* polices */
	private Font policeTitre = new Font("Calibri", Font.BOLD, 24);
	private Font policeParagraphe = new Font("Calibri", Font.HANGING_BASELINE, 16);
	
	/* JLabels */
	JLabel titre;
	JLabel messageArea;
	JLabel countMessage;
	
	/* JPanel */
	JPanel textAreaPanel;
	
	// JTextArea
	JTextArea resultats = new JTextArea();
	JTextArea passwordTextArea;
	

	// ComboBox
	JComboBox<String> choixMoteur = new JComboBox<>();
	
	/*Button*/
	JButton validationButton;
	
	
	public PanelAdministrateur() {
		
		connexion();
	}

	public void connexion() {
		this.removeAll();

		/* Setting Border Layout */
		this.setLayout(new BorderLayout());

		/* Setting the title north of the main panel */
		titre = new JLabel("Connexion Administrateur", SwingConstants.CENTER);
		titre.setFont(policeTitre);
		this.add(titre, BorderLayout.NORTH);

		/* Create a panel centered to the main panel */
		JPanel centerPanel = new JPanel(new GridLayout(3, 1));

		/* Create the message (1st line of the centerPanel) */
		JLabel message = new JLabel("");
		message.setFont(policeParagraphe);

		/* Create the Panel containing the textArea (2nd Line of the centerPanel) */
		textAreaPanel = new JPanel(new GridLayout(1, 3));
		messageArea = new JLabel("Veuillez entrer votre mot de passe", SwingConstants.CENTER);
		messageArea.setFont(policeParagraphe);
		passwordTextArea = new JTextArea("1234");
		passwordTextArea.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
				if(passwordTextArea.getText().contentEquals("1234")) {
					passwordTextArea.setText("");
				}
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		/* Message at the 3rd line of the centerPanel */
		countMessage = new JLabel("");
		
		validationButton = new JButton("Valider");
		validationButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				verifierMotDePasse();
				
			}
		});
		
		
		
		/*Adding content to the textAreaPanel*/
		textAreaPanel.add(messageArea);
		textAreaPanel.add(passwordTextArea);
		textAreaPanel.add(validationButton);
		
		/* Adding content to the center Panel */
		centerPanel.add(message);
		centerPanel.add(textAreaPanel);
		centerPanel.add(countMessage);


		/* Setting the centerPanel at the center of the main Panel */
		this.add(centerPanel, BorderLayout.CENTER);

	}

	private void verifierMotDePasse() {
		
		controlerIdentificationAdministrateur = new ControlerIdentificationAdministrateur();
		
		if (!(nbEssais >= 3)) {

			String motDePasse = new String("");
			boolean motDePasseOK = false;
			motDePasse = passwordTextArea.getText();
			motDePasseOK = controlerIdentificationAdministrateur.verifMotDePasse(motDePasse);
			if (!motDePasseOK) {
				messageArea.setText("Mot de passe erroné");
				nbEssais++;
				countMessage.setText("Veuillez entrer de nouveau votre mot de passe - Essai " + nbEssais + "/3");
			}

			if (motDePasseOK) {
				/* boundaryModifierConfiguration.admin(); */
				
				modifierConfiguration();
			
			}
		}
		else {
			messageArea.setText("Vous n'avez plus d'essais");
			countMessage.setText("Echec de connexion");
		}

	}
	
	
	
	private void modifierConfiguration() {
		
		this.removeAll();
		
		/* Setting Border Layout (Main panel) */
		this.setLayout(new BorderLayout());
		
		/* Setting the title north of the main panel */
		titre.setText("Menu Administrateur");
		this.add(titre, BorderLayout.NORTH);
		
		/* Create a panel centered to the main panel */
		JPanel centerPanel = new JPanel(new GridLayout(1, 4));
		
		/* Create the center panel buttons */
		JButton pathButton = new JButton("Modifier chemin");
		JButton textButton = new JButton("Modifier paramètres texte");
		JButton imageButton = new JButton("Modifier paramètres image");
		JButton soundButton = new JButton("Modifier paramètres son");
		
		
		pathButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				buttonClick(1);
			}
		});
		textButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				buttonClick(2);
			}
		});
		imageButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				buttonClick(3);
			}
		});
		soundButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				buttonClick(4);
			}
		});
		
		
		/* Set the buttons color (white)*/
		pathButton.setBackground(new Color(255,255,255));
		textButton.setBackground(new Color(255,255,255));
		imageButton.setBackground(new Color(255,255,255));
		soundButton.setBackground(new Color(255,255,255));
		
		/*Add the buttons to the center panel */
		centerPanel.add(pathButton);
		centerPanel.add(textButton);
		centerPanel.add(imageButton);
		centerPanel.add(soundButton);

				
		
		/* Create the exit button */
		JButton exit = new JButton("Quitter");
		exit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0)
			{	
				nbEssais= 0;
				connexion();
			}
		});
		
		/* Add content to the main panel beside the title*/
		this.add(centerPanel, BorderLayout.CENTER);
		this.add(exit, BorderLayout.SOUTH);
			
	}
	
	
	
	
	
	private void buttonClick(int caseNumber) {
		
		controlerModifierConfiguration = new ControlerModifierConfiguration();
		this.removeAll();
		initialisationPanelSaisie();
		switch (caseNumber) {
		case 1:
			titre.setText("Modifier chemin");
			messageArea.setText("Veuillez entrer votre nouveau chemin");
			validationButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					String chemin = passwordTextArea.getText();
					controlerModifierConfiguration.modifierChemin(chemin);
					controlerModifierConfiguration.indexation();
					modifierConfiguration();
					
				}
			});
			break;
		case 2:
			titre.setText("Modifier paramètres texte");
			messageArea.setText("Veuillez entrer votre nouvelle valeur");
			validationButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					int nouvelleValeurTexte = Integer.parseInt(passwordTextArea.getText().trim());
					controlerModifierConfiguration.modifierParamTexte(nouvelleValeurTexte);
					controlerModifierConfiguration.indexationTexte();
					modifierConfiguration();
				}
			});
			break;
		case 3:
			titre.setText("Modifier paramètres image");
			messageArea.setText("Veuillez entrer votre nouvelle valeur");
			validationButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					int nouvelleValeurSon = Integer.parseInt(passwordTextArea.getText().trim());
					controlerModifierConfiguration.modifierParamSon(nouvelleValeurSon);
					controlerModifierConfiguration.indexationSon();
					modifierConfiguration();
				}
			});
			break;
		case 4:
			titre.setText("Modifier paramètres son");
			messageArea.setText("Veuillez entrer votre nouvelle valeur");
			validationButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					String motDePasse = passwordTextArea.getText();
					controlerModifierConfiguration.modifierMdp(motDePasse);
					modifierConfiguration();
				}
			});
			break;
		default:
			break;
		}
		
		
	}
	
	
	
	private void initialisationPanelSaisie() {
		/* Setting Border Layout */
		this.setLayout(new BorderLayout());

		/* Setting the title north of the main panel */
		titre = new JLabel("", SwingConstants.CENTER);
		titre.setFont(policeTitre);
		this.add(titre, BorderLayout.NORTH);

		/* Create a panel centered to the main panel */
		JPanel centerPanel = new JPanel(new GridLayout(3, 1));

		/* Create the message (1st line of the centerPanel) */
		JLabel message = new JLabel("");
		message.setFont(policeParagraphe);

		/* Create the Panel containing the textArea (2nd Line of the centerPanel) */
		textAreaPanel = new JPanel(new GridLayout(1, 3));
		messageArea = new JLabel("", SwingConstants.CENTER);
		messageArea.setFont(policeParagraphe);
		passwordTextArea = new JTextArea("2");
		validationButton = new JButton("Valider");
		
		/* Message at the 3rd line of the centerPanel */
		countMessage = new JLabel("");
		
		/*Adding content to the textAreaPanel*/
		textAreaPanel.add(messageArea);
		textAreaPanel.add(passwordTextArea);
		textAreaPanel.add(validationButton);
		
		/* Adding content to the center Panel */
		centerPanel.add(message);
		centerPanel.add(textAreaPanel);
		centerPanel.add(countMessage);


		/* Setting the centerPanel at the center of the main Panel */
		this.add(centerPanel, BorderLayout.CENTER);
		
	}
	

}
