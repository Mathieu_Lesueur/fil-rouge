package vueGraphique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;

import controler.ControlerRequeteSimilarite;
import model.Resultat;

public class PanelResultats extends JPanel{
	
	private Font policeTitre = new Font("Calibri",Font.BOLD,24);
	private Font policeParagraphe = new Font("Calibri", Font.HANGING_BASELINE, 16);

    private JPanel mainList;	
    private JScrollPane listScroll;
    
    Border emptyBorder = BorderFactory.createEmptyBorder();

	public PanelResultats(TreeSet<Resultat<String,Float>> results,boolean percentage)
	{
		
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        
		System.out.println(results);

        
        if(results != null)
        {
            mainList = new JPanel(new GridBagLayout());

			for(Resultat<String,Float> resultat : results)
			{
				JPanel panel = createResultatPanel(resultat, percentage);
				mainList.add(panel,gbc,-1);				
			}
        }
        else
        {
            mainList = new JPanel(new GridLayout(1,1));
        	mainList.add(new JLabel("No results"));
        }
        

        listScroll = new JScrollPane(mainList);
        listScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        listScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        listScroll.setPreferredSize(new Dimension(700,250));
        
		this.add(createPanellegende(percentage),BorderLayout.NORTH);
        this.add(listScroll,BorderLayout.CENTER);

        this.validate();
        this.repaint();
		
	}
	
	
	
	private JPanel createResultatPanel(Resultat<String,Float> resultat,boolean percentage)
	{
		
		
        Path path = Paths.get(resultat.getNom()); 
        Path fileName = path.getFileName(); 
  
		
		JPanel panel = new JPanel(new GridLayout(1,2));
        panel.setBorder(new MatteBorder(0, 0, 1, 0, Color.GRAY));
        
		JButton boutonFile = new JButton(fileName.toString());
		JButton boutonNumber;
		
		
		
		if(percentage)
		{
			 boutonNumber = new JButton(resultat.getNombre().toString() + " %");

		}
		else
		{
			 boutonNumber = new JButton(Math.round(resultat.getNombre()) + "Nb occ");
		}
		boutonFile.setBorder(emptyBorder);
		boutonNumber.setBorder(emptyBorder);
		boutonNumber.setEnabled(false);
		
		boutonNumber.setPreferredSize(new Dimension(200,50));
		boutonFile.setPreferredSize(new Dimension(200,50));
		
		panel.setMaximumSize(new Dimension(400,60));
		panel.setPreferredSize(new Dimension(400,60));

		panel.add(boutonFile);
		panel.add(boutonNumber);
		
		boutonFile.setBackground(new Color(255,255,255));
		boutonNumber.setBackground(new Color(255,255,255));

		return panel;

	}
	
	private JPanel createPanellegende(boolean percentage)
	{
		JPanel panelTitre = new JPanel(new GridLayout(1,2));
		panelTitre.setBorder(new MatteBorder(0, 0, 1, 0, Color.GRAY));
        
		JButton titre = new JButton("Fichier");
		JButton valeur;

		if(percentage)
		{
			valeur = new JButton("Pourcentage");
		}
		else
		{
			valeur = new JButton("Occurences");
		}
		
		titre.setBackground(new Color(255,255,255));
		valeur.setBackground(new Color(255,255,255));
		panelTitre.add(titre);
		panelTitre.add(valeur);
		titre.setEnabled(false);
		valeur.setEnabled(false);
		
		return panelTitre;

	}
	
	
	
    public Dimension getPreferredSize() {
        return new Dimension(900, 400);
    }

}
