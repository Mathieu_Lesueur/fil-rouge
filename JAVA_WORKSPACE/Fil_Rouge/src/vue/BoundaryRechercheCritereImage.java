 package vue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.TreeSet;
import controler.ControlleurIndexation;
import controler.ControlleurRechercheCritereImage;
import model.ComparateurResultat;
import model.CritereImage;
import model.Polarite;
import model.Resultat;
import model.ThreadIndexation;
import model.ComparateurPolarite;

public class BoundaryRechercheCritereImage {

	//controlleur propre de cette vue pour lancer la recherche par critere simple qui existe déjà dans le code c
	private ControlleurRechercheCritereImage ControlleurRechercheCritereImage;
	
	//controlleur pour lancer l'indexation automatique lors de l'appel du Thread
	private ControlleurIndexation controlIndexation;
			
	//boundary pour selectionner lequel parmis les moteurs qui va servir pour faire la recherche
	private BoundarySelectionMoteur boundarySelectionMoteur;
	
	//boundary pour savoir si on va garder une trace ecrite de la recherche dans un fichire
	private BoundarySauvegardeRequete boundarySauvegardeRequete;

	private Scanner scanner = new Scanner(System.in);
	
	public BoundaryRechercheCritereImage(ControlleurRechercheCritereImage control,
			ControlleurIndexation controlIndexation,BoundarySelectionMoteur boundarySelectionMoteur, BoundarySauvegardeRequete boundarySauvegardeRequete) {
		super();
		this.ControlleurRechercheCritereImage = control;
		this.controlIndexation = controlIndexation;
		this.boundarySelectionMoteur = boundarySelectionMoteur;
		this.boundarySauvegardeRequete = boundarySauvegardeRequete;
	}
	
	
	
	
	
	//Requete complexe utilisant plusieurs requetes simples
	//La premiere plage saisie DOIT etre PRESENT dans les resultats pour creer une base de comparaison initiale.
	public void rechercheParCritereComplexe() {
		//selectionner le quel des moteurs pour faire la recherche
		int moteur = boundarySelectionMoteur.selectionMoteur();
		
		//thread pour emêcher la recherche tant que l'indexation n'est pas terminé
		ThreadIndexation myThread = new ThreadIndexation("Image",controlIndexation);
		
		//tableau pour stocker la liste des Criteres : un critere = polarite + couleur (r + g + b)
		ArrayList<CritereImage> listeCrit = new ArrayList<>();
		
		//variable pour reboucler tant que l'utilisateur veut saisir un autre critere (polarite + couler (R+G+B))
		boolean newplage = true;				
		// Saisie des criteres soit une polarite + un mot par critere.
		//boucler tant que l'utilisateur veux saisir un autre critere
		while(newplage) {
			//pour tester la validite de la COULEUR
			boolean couleurValide = false;
			//par defaut dans le cahier d charge la polarite est +
			Polarite po = Polarite.PRESENT;
			//pour tester la validite de la polarite 1. ou 2.
			boolean povalide = false;			
			//lecture de la polarite d'un critere			
			while (!povalide) {
				System.out.println("Quelle est la polarite de la COULEUR que vous souhaitez rechercher ? 1. + 2. -");
				int polarite = scanner.nextInt();
				if(polarite == 1 || polarite == 2) {
					if(polarite == 1) {
						po = Polarite.PRESENT;
					}else {
						po = Polarite.ABSENT;
					}
				povalide = true;
				}else {
					System.out.println("Polarite invalide. Reessayez.");
				}
			}
			//fin de la lecture de la polarite d'un critere
			//lecture de la couleur a rechercher 
			System.out.println("Quelle est la COULEUR a rechercher ?");
			int r = -1; // initialisation a une valeur fausse
			int g = -1; // initialisation a une valeur fausse
			int b = -1; // initialisation a une valeur fausse
			while(!couleurValide) {
				System.out.println("veuillez saisir la couleur selon laquelle vous allez effectuer votre recherche");			
				System.out.println("<< Veuillez saisir la composante red >>");
				r = scanner.nextInt();
				System.out.println("<< Veuillez saisir la composante green >>");
				g=scanner.nextInt();
				System.out.println("<< Veuillez saisir la composante blue >>");
				b=scanner.nextInt();
				
				// verifier la validite des composante de la couleur
				couleurValide = (r >= 0 && r <= 255 && g >= 0 && g <= 255 && b >=0 && b <= 255);
				if(!couleurValide) {
					System.out.println("La plage saisie est invalide. Reessayez.");
				}
			}
			//fin de la saisie de la couleur a rechercher			
			// OK on a dejà un critere qui est prêt, on l'ajoute dans la liste des criteres
			CritereImage crit = new CritereImage(po,r,g,b);
			listeCrit.add(crit);			
			System.out.println("Voulez-vous saisir une nouvelle plage ? 1.Oui 2.Non");
			int reponse = scanner.nextInt();
			if (reponse == 2 ) {
				newplage = false;
			}
		}	
		//fin de la lecture de la liste des criteres :c'est parti pour la recherche
		//lancer l'indexation automatique avant d'effectuer la rechercher
		myThread.start();
		//trier la liste des criteres pour mettre les critere dont leur polarite est + en avant
		// pour verifier si dans la liste des criteres y'a pas des polarité positives au debut  donc c'est pas la peine d'effectuer la recherche
		Collections.sort(listeCrit, new ComparateurPolarite());
		// si y'a pas de polarité + alors on fait pas la recherche
		// on sort du programme
		if(listeCrit.get(0).getPolarite() != Polarite.PRESENT) {
			System.out.println("ERREUR : au moins UNE plage doit etre presente dans les resultats (polarite +).");
			System.exit(3);
		}		
		//tant que l'indexation n'est pas terminé , il faut attendre
		while(myThread.getState() != Thread.State.TERMINATED) {}		
		//resultat ou on va stocker la liste des resultat final : liste des fichiers avec leurs nbre d'occurance
		//trier selon les fichiers qui se trouve le plus ( dont leur nbre d'occ est élévé)
		// affichage du resultat de la recherche par critere complexe dans le cas ou la
		// liste des critere contient plus qu'un critere
		//sinon l'affichage se fait dans la methode juste avant
		TreeSet<Resultat<String,Float>> result = new TreeSet<>(new ComparateurResultat());
		if (!listeCrit.isEmpty()) {
			result=ControlleurRechercheCritereImage.rechercheImage(listeCrit,moteur);
			Boolean choix=boundarySauvegardeRequete.sauvegarderRequete("Requete de recherche image", result);
			if(!choix) {
				if (!result.isEmpty()) {
					for(Resultat<String,Float> r : result){
						System.out.println("Result : " + r.getNom() + " " + r.getNombre().intValue());
					}
				}else {
					System.out.println("Aucun document ne correspond a ces criteres !");
				}
			}else {
				System.out.println("Liste de critere vide.");
			}
		}
		scanner.close();
	}	
}
