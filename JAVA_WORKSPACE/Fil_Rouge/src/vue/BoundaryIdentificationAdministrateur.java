package vue;

import java.util.Scanner;

import controler.ControlerIdentificationAdministrateur;

public class BoundaryIdentificationAdministrateur {

	private BoundaryModifierConfiguration boundaryModifierConfiguration;
	private ControlerIdentificationAdministrateur controlerIdentificationAdministrateur;
	private Scanner clavier = new Scanner(System.in);

	public BoundaryIdentificationAdministrateur(BoundaryModifierConfiguration b,ControlerIdentificationAdministrateur controlerIdentificationAdministrateur) {
		this.boundaryModifierConfiguration=b;
		this.controlerIdentificationAdministrateur = controlerIdentificationAdministrateur;
	}

	public void connexion() {
		int nbEssais = 0;
		
		
		System.out.println("Mode administrateur\nVeuillez entrer votre mot de passe");
		
		
		String motDePasse=null;
		boolean motDePasseOK=false;
		do {
			motDePasse = clavier.nextLine();
			motDePasseOK = controlerIdentificationAdministrateur.verifMotDePasse(motDePasse);
			if (!motDePasseOK) {
				nbEssais++;
				System.out.println("Mot de passe erron�\nVeuillez entrer � nouveau votre mot de passe");
				System.out.println("Essai "+ nbEssais +"/3");
			}			
		}while(!motDePasseOK && nbEssais < 3);
		if(nbEssais>=3) {
			System.out.println("Vous n'avez plus d'essais, echec de connexion");
		}
		if(motDePasseOK) 
		{
			boundaryModifierConfiguration.admin();
		}
	}

}
