
package vue;

import java.lang.Thread.State;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;
import controler.ControlleurIndexation;
import controler.ControlleurRechercheCritereTexte;
import model.ComparateurPolarite;
import model.ComparateurResultat;
import model.CritereTexte;
import model.Polarite;
import model.Resultat;
import model.ThreadIndexation;

public class BoundaryRechercheCritereTexte {
	// controlleur pour lancer l'indexation automatique lors de l'appel du Thread
	private ControlleurIndexation controlleurIndexation;

	// controlleur propre de cette vue pour lancer la recherche par critere simple
	// qui existe déjà dans le code c
	private ControlleurRechercheCritereTexte controlleurRechercheCritereTexte;

	// boundary pour selectionner lequel parmis les moteurs qui va servir pour faire
	// la recherche
	private BoundarySelectionMoteur boundarySelectionMoteur;

	// boundary pour savoir si on va garder une trace ecrite de la recherche dans un
	// fichire
	private BoundarySauvegardeRequete boundarySauvegardeRequete;
	
	//liste interdits
	private String[] interdit = { "!", "?", "*", "&" };
	private Scanner scanner = new Scanner(System.in);
	// Constructeur
	public BoundaryRechercheCritereTexte(ControlleurIndexation controlleurIndexation,
			ControlleurRechercheCritereTexte controlleurRechercheCritereTexte, BoundarySelectionMoteur boundarySelectionMoteur, BoundarySauvegardeRequete boundarySauvegardeRequete) {
		super();
		this.controlleurIndexation = controlleurIndexation;
		this.controlleurRechercheCritereTexte = controlleurRechercheCritereTexte;
		this.boundarySelectionMoteur = boundarySelectionMoteur;
		this.boundarySauvegardeRequete = boundarySauvegardeRequete;
	}

	public void rechercheComplexe() {
		// selectionner le quel des moteurs pour faire la recherche
		int moteur = boundarySelectionMoteur.selectionMoteur();
		ThreadIndexation myThread = new ThreadIndexation("Texte", controlleurIndexation);

		List<CritereTexte> listeCritere = new ArrayList<CritereTexte>();
		boolean newWord = true;
		String motCle = "";
		
		Polarite polarite = Polarite.PRESENT;
		while (newWord) {

			boolean polValide = false;
			boolean motValide = false;

			System.out.println("veuillez saisir la polarite");
			System.out.println("1-PRESENT");
			System.out.println("2-ABSENT");
			// >> saisie de la polarite du motCle
			do {
				int po = scanner.nextInt();
				if (po == 1 || po == 2) {
					polValide = true;
					if (po == 2) {
						polarite = Polarite.ABSENT;
					}
				} else {
					System.out.println("veuillez entrer une polarite valide 1. ou 2.");
				}
			} while (!polValide);
			// >> fin de la saisie de la polarite

			// >> saisie du motCle
			System.out.println("veuillez saisir le motCle ");
			do {
				scanner.nextLine();
				motCle = scanner.nextLine();
				for (String ch : interdit) {
					if (motCle.contains(ch)) {
						motCle = null;
					}
				}
				motValide = (motCle != null);

				if (!motValide) {
					System.out.println("veuillez entrer un motCle valide");
				}
			} while (!motValide);
			// << fin de la saisie du motCle

			// creer une critere d'apres la polarite et le motCle qui existe
			CritereTexte c = new CritereTexte(polarite, motCle);
			listeCritere.add(c);
			// >> lecture de la reponse : si on veut ajouter un autre critere
			System.out.println("voulez vous saisir un autre motCle 1. Oui || 2. Non");
			int reponse = scanner.nextInt();
			if (reponse == 2) {
				newWord = false;
			}
		}
		// << fin de la lecture de la reponse

		// lancer l'indexation automatique
		myThread.start();

		// trier le tableau des criteres avec au debut les criteres avec la polarite
		// PRESENT et à la fin celle de la polarite ABSENT
		Collections.sort(listeCritere, new ComparateurPolarite());
		// si y'a pas de Polarite '+' => "Present" alors pas de recherche
		if (listeCritere.get(0).getPolarite() != Polarite.PRESENT) {
			System.out.println("ERREUR : au moins UN mot-cle doit etre present dans les resultats (polarite +).");
			System.exit(1);
		}

		myThread.getState();
		// !! faire la recherche apres avoir faire l'indexation automatique !!
		while (myThread.getState() != State.TERMINATED) {}

		// pour stocker le resultat finale de la recherche
		TreeSet<Resultat<String, Float>> result = new TreeSet<>(new ComparateurResultat());

		// >> *indexation automatique faite : allez c'est parti pour la recherche
		if (!listeCritere.isEmpty()) {
			// si il y un seul critere alors on fait un recherche simple suivant cet critere
			result = controlleurRechercheCritereTexte.rechercheTexte(listeCritere, moteur);
			// affichage du resultat de la recherche par critere complexe dans le cas ou la
			// liste des critere contient plus qu'un critere
			// sinon l'affichage se fait dans la methode juste avant
			Boolean choix = boundarySauvegardeRequete.sauvegarderRequete("Requete de recherche texte", result);
			if (!choix) {
				if (!result.isEmpty()) {
					for (Resultat<String, Float> r : result) {
						System.out.println("Result : " + r.getNom() + " " + r.getNombre().intValue());
					}
				} else {
					System.out.println("Aucun document ne correspond a ces criteres !");
				}
			}
		}else {
			System.out.println("Liste de critere vide.");
		}
		scanner.close();
	}	
}
