package vue;

import java.util.Scanner;

public class BoundarySelectionMoteur {
	private Scanner clavier = new Scanner(System.in);
	public BoundarySelectionMoteur() {
	
	}
	
	public int selectionMoteur() {
		System.out.println("Quel moteur souhaitez-vous utiliser ?" );
		int choix;
		do {
			System.out.println("1-Moteur 1");
			System.out.println("2-Moteur 2");
			System.out.println("3-Tous");
			choix = clavier.nextInt();
			if(choix !=1 && choix !=2 && choix !=3) {
				System.out.println("Veuillez entrer un numro correct");
			}
		}while(choix !=1 && choix!=2 && choix !=3);
		return choix;
	}
}
