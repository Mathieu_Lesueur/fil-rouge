package vue;

import java.util.Scanner;

import controler.ControlerModifierConfiguration;

public class BoundaryModifierConfiguration {

	private ControlerModifierConfiguration controlerModifierConfiguration;
	private Scanner clavier = new Scanner(System.in);

	public BoundaryModifierConfiguration(ControlerModifierConfiguration controlerModifierConfiguration) {
		this.controlerModifierConfiguration = controlerModifierConfiguration;
	}

	public void admin() {
		System.out.println(
				"MENU ADMIN\n1-Modifier chemin\n2-Modifier paramètres texte"
				+ "\n3-Modifier paramètres image\n4-Modifier paramètres son"
				+ "\n5-Changer le mot de passe\n6-Quitter");
		
		int choix;
		do {
			System.out.println("Veuillez entrer votre choix");
			choix = clavier.nextInt();
			if(choix != 1 && choix !=2 && choix !=3 && choix !=4 && choix !=5 && choix !=6) {
				System.out.println("Veuillez entrer un choix correct");
			}
		}while(choix != 1 && choix !=2 && choix !=3 && choix !=4 && choix !=5 && choix !=6);
					
		switch (choix) {
		case 1:
			System.out.println("Veuillez entrer votre nouveau chemin");
			String chemin = clavier.next();
			controlerModifierConfiguration.modifierChemin(chemin);
			controlerModifierConfiguration.indexation();
			break;
		case 2:
			System.out.println("Veuillez entrer votre nouvelle valeur");
			int nouvelleValeurTexte = clavier.nextInt();
			controlerModifierConfiguration.modifierParamTexte(nouvelleValeurTexte);
			controlerModifierConfiguration.indexationTexte();
			break;
		case 3:
			System.out.println("Veuillez entrer votre nouvelle valeur");
			int nouvelleValeurImage = clavier.nextInt();
			controlerModifierConfiguration.modifierParamImage(nouvelleValeurImage);
			controlerModifierConfiguration.indexationImage();
			break;
		case 4:
			System.out.println("Veuillez entrer votre nouvelle valeur");
			int nouvelleValeurSon = clavier.nextInt();
			controlerModifierConfiguration.modifierParamSon(nouvelleValeurSon);
			controlerModifierConfiguration.indexationSon();
			break;
		case 5:
			System.out.println("Veuillez entrer votre nouvelle valeur");
			String motDePasse = clavier.next();
			controlerModifierConfiguration.modifierMdp(motDePasse);
			break;
		case 6:
			break;
		default:
			System.out.println("Choix menu admin incorrect");
		}
	}

}
