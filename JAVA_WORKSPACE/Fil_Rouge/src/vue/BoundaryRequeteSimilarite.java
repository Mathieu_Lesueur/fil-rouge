package vue;

import java.io.File;
import java.util.Scanner;
import java.util.TreeSet;

import controler.ControlerRequeteSimilarite;
import controler.ControleurComparaisonSon;
import controler.ControlleurIndexation;
import model.Resultat;

public class BoundaryRequeteSimilarite {
	
	private ControlerRequeteSimilarite controlerRequeteSimilarite;
	private BoundarySelectionMoteur boundarySelectionMoteur;
	private BoundarySauvegardeRequete boundarySauvegardeRequete;
	private BoundaryComparaisonSon boundaryComparaisonSon;
	private Scanner clavier = new Scanner(System.in);
	
	public BoundaryRequeteSimilarite(ControlerRequeteSimilarite c, BoundarySelectionMoteur b,BoundarySauvegardeRequete bb) {
		controlerRequeteSimilarite=c;
		boundarySelectionMoteur=b;
		boundarySauvegardeRequete=bb;
		boundaryComparaisonSon =new BoundaryComparaisonSon(new ControleurComparaisonSon(),new ControlleurIndexation());
	}
	
	public void requeteSimilarite(){
		int moteur;
		System.out.println("Veuillez choisir le type de fichier");
		int choix;
		do {
			System.out.println("1-texte");
			System.out.println("2-image");
			System.out.println("3-son");
			choix = clavier.nextInt();
			if(choix != 1 && choix !=2 && choix!=3) {
				System.out.println("Veuillez entrer un num�ro correct");
			}
		}while(choix != 1 && choix !=2 && choix !=3);
		TreeSet<Resultat<String,Float>> resultat= new TreeSet<>();
		File file = null;
		switch(choix) {
		case 1:
			System.out.println("Entrer le chemin d'un fichier texte");
			clavier.nextLine();
			String cheminT = clavier.nextLine();
			file = new File(cheminT);
			moteur = boundarySelectionMoteur.selectionMoteur();
			resultat = controlerRequeteSimilarite.requeteSimilariteTexte(moteur,file);
			break;
		
		case 2:
			System.out.println("Entrer le chemin d'un fichier image");
			clavier.nextLine();
			String cheminI = clavier.nextLine();
			file = new File(cheminI);
			moteur = boundarySelectionMoteur.selectionMoteur();
			resultat = controlerRequeteSimilarite.requeteSimilariteImage(moteur,file);
			break;
		case 3:
			boundaryComparaisonSon.comparerSon();
			break;
		default:
			System.out.println("Erreur requete similarite");				
		}
		if (choix !=3) {
			Boolean sauvegarde = boundarySauvegardeRequete.sauvegarderRequete("Recherche par similarit� pour le fichier " +file.getName(), resultat);
			if(!sauvegarde) {
				for(Resultat<String, Float> r :resultat) {
					System.out.println(r.getNom() + " "+ r.getNombre());
				}
			}
		}
	}
}
