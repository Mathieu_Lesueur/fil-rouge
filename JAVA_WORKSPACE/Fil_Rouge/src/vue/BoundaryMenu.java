package vue;

import java.util.Scanner;

public class BoundaryMenu {
	private BoundaryRechercheCritereTexte boundaryRechercheTexte;
	private BoundaryRechercheCritereImage boundaryRechercheImage;
	private BoundaryRequeteSimilarite boundaryRequeteSimilarite;
	private BoundaryIdentificationAdministrateur boundaryConnexionAdmin;
	private Scanner clavier = new Scanner(System.in);
	public BoundaryMenu(BoundaryRechercheCritereTexte bTexte, BoundaryRechercheCritereImage bImage,
			BoundaryRequeteSimilarite bSimilarite, BoundaryIdentificationAdministrateur bAdmin ) {
		boundaryRechercheTexte=bTexte;
		boundaryRechercheImage=bImage;
		boundaryRequeteSimilarite=bSimilarite;
		boundaryConnexionAdmin=bAdmin;		
	}
	
	public void afficherMenu() {
		boolean continuer = true;
		while(continuer) {
			System.out.println();
			System.out.println("Bienvenu dans le moteur de recherche");
			System.out.println();
			System.out.println("------------------------- MENU -------------------------");
			System.out.println("\t1. Recherche texte par critere");
			System.out.println("\t2. Recherche image par critere");
			System.out.println("\t3. Recherche par similarite : texte/image/son");
			System.out.println("\t4. Se connecter en tant qu'administrateur");
			System.out.println("\t5. Quitter");
			System.out.println();
			int choix;		
			do {
				System.out.println("Veuillez entrer votre choix");
				choix = clavier.nextInt();
				if(choix != 1 && choix !=2 && choix !=3 && choix !=4 && choix !=5) {
					System.out.println("Veuillez entrer un choix correct");
				}
			}while(choix != 1 && choix !=2 && choix !=3 && choix !=4 && choix !=5);
			
			switch(choix) {
			case 1 :
				boundaryRechercheTexte.rechercheComplexe();
				System.exit(0);
				break;
			case 2 :
				boundaryRechercheImage.rechercheParCritereComplexe();
				System.exit(0);

				break;
			case 3 :
				boundaryRequeteSimilarite.requeteSimilarite();
				break;
			case 4:
				boundaryConnexionAdmin.connexion();
				break;
			case 5 :
				continuer=false;
				break;
			default:
				System.out.println("La fonctionnalite menu demandee n'existe pas");
			}
		}
	}
}
