package vue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//Permet de lire, ecrire et effacer le fichier mdp
//contenant le mot de passe du mode administrateur
// cas : BoundaryModifierConfiguration

public class Fichier {
	public static void ecrire(String texte) {
		try {
			String chemin ="Docs\\mdp.txt";
			File f = new File(chemin);
			f.createNewFile();
			FileWriter fw = new FileWriter(f, true);
			fw.write(texte+"\n");
			fw.flush();
			fw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void effacer() {
		try {
			String chemin ="Docs\\mdp.txt";
			File f = new File(chemin);
			f.createNewFile();
			FileWriter fw = new FileWriter(f, false);
			fw.write("");
			fw.flush();
			fw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
	}
	
	public static String get(){
		String chemin;
		if(System.getProperty("os.name").startsWith("Windows")){ chemin		 ="Docs\\mdp.txt";}
		else{ chemin		 ="./Docs/mdp.txt"; }
		
		File f = new File(chemin);
		try {
			BufferedReader r = new BufferedReader(new FileReader(f));
			String mdp = r.readLine();
			r.close();
			return mdp;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
