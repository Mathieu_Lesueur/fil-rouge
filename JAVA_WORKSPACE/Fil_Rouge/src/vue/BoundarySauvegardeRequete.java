package vue;

import java.util.Scanner;
import java.util.TreeSet;
import controler.ControlerSauvegardeRequete;
import model.Resultat;

public class BoundarySauvegardeRequete {
	private ControlerSauvegardeRequete controlerSauvegardeRequete;
	private Scanner clavier = new Scanner(System.in);
	
	public BoundarySauvegardeRequete(ControlerSauvegardeRequete c) {
		controlerSauvegardeRequete=c;
		
	}
	
	public boolean sauvegarderRequete(String requete,TreeSet<Resultat<String,Float>> resultat) {
		System.out.println("Voulez-vous sauvegarder les resultats de cette requete ?");
		int choix;
		do {
			System.out.println("1-OUI");
			System.out.println("2-NON");
			choix = clavier.nextInt();

			if(choix !=1 && choix !=2 ) {
				System.out.println("Veuillez entrer 1 ou 2");
			}
		}while(choix !=1 && choix !=2);
		clavier.close();
		if(choix==1) {
			controlerSauvegardeRequete.sauvegarderRequete(requete,resultat);
			return true;
		}
		return false;
	}
}
