package vue;

import java.lang.Thread.State;
import java.util.Scanner;
import java.util.TreeSet;

import controler.ControlerSauvegardeRequete;
import controler.ControleurComparaisonSon;
import controler.ControlleurIndexation;
import model.Resultat;
import model.ThreadIndexation;


public class BoundaryComparaisonSon {
	
	private ControleurComparaisonSon comparaisonSon;
	private Scanner clavier = new Scanner(System.in);
	private TreeSet<Resultat<String,Float>> res;
	private BoundarySelectionMoteur boundarySelectionMoteur;
	private BoundarySauvegardeRequete boundarySauvegardeRequete;
	private ControlleurIndexation controlleurIndexation;
	
	public BoundaryComparaisonSon(ControleurComparaisonSon comparaisonSon, ControlleurIndexation controlleurIndexation) {
		this.comparaisonSon = comparaisonSon;
		boundarySelectionMoteur = new BoundarySelectionMoteur();
		boundarySauvegardeRequete=new BoundarySauvegardeRequete(new ControlerSauvegardeRequete());
		this.controlleurIndexation= controlleurIndexation;
	}
	
	public void comparerSon() {
		
		//choix moteur
		//lancer l'indexation
		ThreadIndexation myThread = new ThreadIndexation("Son", controlleurIndexation);
		// lancer l'indexation automatique
		myThread.start();
		myThread.getState();
		// !! faire la recherche apres avoir faire l'indexation automatique !!
		while (myThread.getState() != State.TERMINATED) {}
		System.out.println("Veuillez entre le chemin du fichier a comparer :");
		String path = clavier.next();
		System.out.println("Veuillez entre le mode de recherche : \n-0 : contenant \n-1 ressemblant");
		int mode = clavier.nextInt();
		int moteur = boundarySelectionMoteur.selectionMoteur();
		try {
			res = comparaisonSon.comparaisonSon(path, mode);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		Resultat<String, Float> res_tempo;
		
		//choix sauvegarder requete ou affichage console
		boolean choix = boundarySauvegardeRequete.sauvegarderRequete("Comparaison son pour le fichier " + path, res);
		if(!choix) {
			if(mode == 0) {
				System.out.println("Les fichiers contenant le fichiers donn sont : ");
				for(int i = 0; i < res.size(); i++) {
					res_tempo = res.last();
					res.remove(res_tempo);
					System.out.println("-" + res_tempo.getNom() + " au timecode : " + res_tempo.getNombre() + "s");
				}
			}else {
				System.out.println("Les fichiers resemblant sont : ");
				for(int i = 0; i < res.size(); i++) {
					res_tempo = res.last();
					res.remove(res_tempo);
					System.out.println("-" + res_tempo.getNom() + " a : " + res_tempo.getNombre() + "%");
				}
			}
		}
	}
}
