package model;

//une paire pour sauvgarder avec chaque fichier son nombre d'occurance lors de la recherche
public class Resultat<S, N> implements Comparable<Resultat<S, N>>{
	
	//le nom, fin chemin du fichier
	private S nom;
	
	// le nombre d'occurance
	private N nombre;
	
	public Resultat() {
		nom = null;
		nombre = null;
	}
	
	public Resultat(S n, N no) {
		nom = n;
		nombre = no;
	}
	
	public S getNom() {
		return nom;
	}
	
	public N getNombre() {
		return nombre;
	}
	
	public void setNombre(N nb) {
		this.nombre = nb;
	}

	@Override
	public int compareTo(Resultat<S, N> o) {
		// TODO Auto-generated method stub
		if(o.getNom().equals(this.getNom()) && o.getNombre()==this.getNombre())
		{
			return 0;
		}
		return -1;
	}
	@Override
	public String toString()
	{
		return nom+"=>"+nombre;
		
	}
	
	
}
