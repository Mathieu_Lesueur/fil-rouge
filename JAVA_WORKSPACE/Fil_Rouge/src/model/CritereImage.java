package model;
public class CritereImage extends Critere {

	private int R;
	private int G;
	private int B;
	public CritereImage(Polarite polarite, int r, int g, int b) {
		super(polarite);
		R = r;
		G = g;
		B = b;
	}
	
	public int getR() {
		return R;
	}
	
	public int getG() {
		return G;
	}
	
	public int getB() {
		return B;
	}

	@Override
	public String toString() {
		return "CritereImage [ " +super.toString() + " R=" + R + ", G=" + G + ", B=" + B + "]";
	}
	
}
