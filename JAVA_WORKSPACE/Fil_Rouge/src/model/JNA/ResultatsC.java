package model.JNA;

import java.util.TreeMap;
import java.util.TreeSet;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

import model.Resultat;

public class ResultatsC 
{
	static InterfaceC_JAVA INSTANCE= Native.load("interfaceJava",    InterfaceC_JAVA.class);;

		
	//---------------------------------------RECHERCHE------------------------------------------------------------
	
	public static TreeSet<Resultat<String,Float>>  get_recherche_texte_result(String motCle) throws NoResultsException
	{
		
		TreeSet<Resultat<String, Float>> returned_map= new TreeSet<Resultat<String,Float>>();
		
		
	    final PointerByReference refFloatArray = new PointerByReference();
	    final PointerByReference refStringArray = new PointerByReference();
	    final IntByReference refNbVals = new IntByReference();

	    // call the C function
	    INSTANCE.recherche_texte(motCle, refNbVals, refFloatArray, refStringArray);
	    int nbVals = refNbVals.getValue();

	    // extract the number of values returned
	    if (0 < nbVals)
	    {
	    	
	    	
	    	// extract the pointed-to pointer
	    	final Pointer pointerFloatArray = refFloatArray.getValue();
		    final Pointer pointerStringArray = refStringArray.getValue();
		    final String[] stringArray = pointerStringArray.getStringArray(0, nbVals);

		    		    
	    	// look at each value
	    	for (int index=0; index<nbVals; index++) 
	    	{
	    		float val = pointerFloatArray.getFloat(index * Native.getNativeSize(Float.TYPE));
	    		//
	    		String string = stringArray[index];
	    		//put it in the map 
	    		returned_map.add(new Resultat<String, Float>(string,val));

	    	}

	    	
	    	return returned_map;
	    }
	    else
	    {
	    	throw new NoResultsException("No results from the C program");
	    }

	}
	
	public static TreeSet<Resultat<String,Float>> get_recherche_image_result(int r,int g,int b) throws NoResultsException
	{
		
		TreeSet<Resultat<String, Float>> returned_map= new TreeSet<Resultat<String,Float>>();
		
	    final PointerByReference refFloatArray = new PointerByReference();
	    final PointerByReference refStringArray = new PointerByReference();
	    final IntByReference refNbVals = new IntByReference();

	    // call the C function
	    INSTANCE.recherche_image(r,g,b, refNbVals, refFloatArray, refStringArray);
	    int nbVals = refNbVals.getValue();

	    // extract the number of values returned
	    if (0 < nbVals)
	    {
	    	
	    	
	    	// extract the pointed-to pointer
	    	final Pointer pointerFloatArray = refFloatArray.getValue();
		    final Pointer pointerStringArray = refStringArray.getValue();
		    final String[] stringArray = pointerStringArray.getStringArray(0, nbVals);

	    	// look at each value
	    	for (int index=0; index<nbVals; index++) 
	    	{
	    		float val = pointerFloatArray.getFloat(index * Native.getNativeSize(Float.TYPE));
	    		String string = stringArray[index];
	    		//put it in the map 
	    		returned_map.add(new Resultat<String, Float>(string,val));

	    	}

	    	
	    	return returned_map;
	    }
	    else
	    {
	    	throw new NoResultsException("No results from the C program");
	    }

	}

	public static TreeSet<Resultat<String,Float>> get_recherche_son_result(String path) throws NoResultsException
	{
		
		TreeSet<Resultat<String, Float>> returned_map= new TreeSet<Resultat<String,Float>>();
		
	    final PointerByReference refFloatArray = new PointerByReference();
	    final PointerByReference refStringArray = new PointerByReference();
	    final IntByReference refNbVals = new IntByReference();

	    // call the C function
	    INSTANCE.recherche_son(path, refNbVals, refFloatArray, refStringArray);
	    int nbVals = refNbVals.getValue();

	    // extract the number of values returned
	    if (0 < nbVals)
	    {
	    	
	    	
	    	// extract the pointed-to pointer
	    	final Pointer pointerFloatArray = refFloatArray.getValue();
		    final Pointer pointerStringArray = refStringArray.getValue();
		    final String[] stringArray = pointerStringArray.getStringArray(0, nbVals);

	    	// look at each value
	    	for (int index=0; index<nbVals; index++) 
	    	{
	    		float val = pointerFloatArray.getFloat(index * Native.getNativeSize(Float.TYPE));
	    		String string = stringArray[index];
	    		//put it in the map 
	    		returned_map.add(new Resultat<String, Float>(string,val));

	    	}

	    	
	    	return returned_map;
	    }
	    else
	    {
	    	throw new NoResultsException("No results from the C program");
	    }

	}

	
	//--------------------------------------COMPARAISON------------------------------------------------------------
	
	public static TreeSet<Resultat<String,Float>> get_comparaison_texte_result(String path) throws NoResultsException
	{
		
		TreeSet<Resultat<String, Float>> returned_map= new TreeSet<Resultat<String,Float>>();
		
	    final PointerByReference refFloatArray = new PointerByReference();
	    final PointerByReference refStringArray = new PointerByReference();
	    final IntByReference refNbVals = new IntByReference();

	    // call the C function
	    INSTANCE.comparaison_texte(path, refNbVals, refFloatArray, refStringArray);
	    int nbVals = refNbVals.getValue();

	    // extract the number of values returned
	    if (0 < nbVals)
	    {
	    	
	    	
	    	// extract the pointed-to pointer
	    	final Pointer pointerFloatArray = refFloatArray.getValue();
		    final Pointer pointerStringArray = refStringArray.getValue();
		    final String[] stringArray = pointerStringArray.getStringArray(0, nbVals);

	    	// look at each value
	    	for (int index=0; index<nbVals; index++) 
	    	{
	    		float val = pointerFloatArray.getFloat(index * Native.getNativeSize(Float.TYPE));
	    		String string = stringArray[index];
	    		//put it in the map 
	    		returned_map.add(new Resultat<String, Float>(string,val));

	    	}

	    	
	    	return returned_map;
	    }
	    else
	    {
	    	throw new NoResultsException("No results from the C program");
	    }

	}
	
	public static TreeSet<Resultat<String,Float>> get_comparaison_image_result(String path) throws NoResultsException
	{
		
		TreeSet<Resultat<String, Float>> returned_map= new TreeSet<Resultat<String,Float>>();
		
	    final PointerByReference refFloatArray = new PointerByReference();
	    final PointerByReference refStringArray = new PointerByReference();
	    final IntByReference refNbVals = new IntByReference();

	    // call the C function
	    INSTANCE.comparaison_image(path, refNbVals, refFloatArray, refStringArray);
	    int nbVals = refNbVals.getValue();

	    // extract the number of values returned
	    if (0 < nbVals)
	    {
	    	
	    	
	    	// extract the pointed-to pointer
	    	final Pointer pointerFloatArray = refFloatArray.getValue();
		    final Pointer pointerStringArray = refStringArray.getValue();
		    final String[] stringArray = pointerStringArray.getStringArray(0, nbVals);

	    	// look at each value
	    	for (int index=0; index<nbVals; index++) 
	    	{
	    		float val = pointerFloatArray.getFloat(index * Native.getNativeSize(Float.TYPE));
	    		String string = stringArray[index];
	    		//put it in the map 
	    		returned_map.add(new Resultat<String, Float>(string,val));

	    	}

	    	
	    	return returned_map;
	    }
	    else
	    {
	    	throw new NoResultsException("No results from the C program");
	    }

	}

	public static TreeSet<Resultat<String,Float>> get_comparaison_son_result(String path) throws NoResultsException
	{
		
		TreeSet<Resultat<String, Float>> returned_map= new TreeSet<Resultat<String,Float>>();
		
	    final PointerByReference refFloatArray = new PointerByReference();
	    final PointerByReference refStringArray = new PointerByReference();
	    final IntByReference refNbVals = new IntByReference();

	    // call the C function
	    INSTANCE.comparaison_son(path, refNbVals, refFloatArray, refStringArray);
	    int nbVals = refNbVals.getValue();

	    // extract the number of values returned
	    if (0 < nbVals)
	    {
	    	
	    	
	    	// extract the pointed-to pointer
	    	final Pointer pointerFloatArray = refFloatArray.getValue();
		    final Pointer pointerStringArray = refStringArray.getValue();
		    final String[] stringArray = pointerStringArray.getStringArray(0, nbVals);

	    	// look at each value
	    	for (int index=0; index<nbVals; index++) 
	    	{
	    		float val = pointerFloatArray.getFloat(index * Native.getNativeSize(Float.TYPE));
	    		String string = stringArray[index];
	    		//put it in the map 
	    		returned_map.add(new Resultat<String, Float>(string,val));

	    	}

	    	
	    	return returned_map;
	    }
	    else
	    {
	    	throw new NoResultsException("No results from the C program");
	    }

	}
	
	
	public static void indexer_repertoire_texte()
	{
		INSTANCE.indexer_repertoire_texte("../../DATA_FIL_ROUGE_DEV/TEXTES");
	}
	public static void indexer_repertoire_image()
	{
		INSTANCE.indexer_repertoire_image("../../DATA_FIL_ROUGE_DEV/TEST_RGB");
		INSTANCE.indexer_repertoire_image("../../DATA_FIL_ROUGE_DEV/TEST_NB");

	}
	public static void indexer_repertoire_son()
	{
		INSTANCE.indexer_repertoire_son("../../DATA_FIL_ROUGE_DEV/TEST_SON");
	}


}
