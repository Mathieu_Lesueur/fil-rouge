package model.JNA;

public class NoResultsException extends Exception{
	
    public NoResultsException(String errorMessage) {
        super(errorMessage);
    }

}
