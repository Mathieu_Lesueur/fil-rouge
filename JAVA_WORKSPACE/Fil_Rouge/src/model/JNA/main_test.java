package model.JNA;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import model.Resultat;

public class main_test {

	public static void main(String[] args)
	{
		
		//ResultatsC.indexer_repertoire_image();
		
		
		try {
			TreeSet<Resultat<String,Float>> results =  ResultatsC.get_recherche_texte_result("est");
			
			for(Resultat<String,Float> resultat : results)
			{
				System.out.println(resultat);
			}
			
		} catch (NoResultsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		try {
			
			
			
			System.out.println("\n\n---------TEXTE----------\n\n");
			
			
			ResultatsC.indexer_repertoire_texte();
			
			System.out.println("\n ============================ \n");

			
			TreeSet<Resultat<String,Float>> resultTexte_recherche = ResultatsC.get_recherche_texte_result("est");
			for(Resultat<String,Float> r : resultTexte_recherche)
			{
				System.out.println(r);
			}

			TreeSet<Resultat<String,Float>> resultTexte_comparaison = ResultatsC.get_comparaison_texte_result("/home/ugo/Desktop/Cours/git/fil-rouge/DATA_FIL_ROUGE_DEV/TEXTES/22-Grippe_aviaire___la_course.xml");
			System.out.println("\n ============================ \n");

			for(Resultat<String,Float> r : resultTexte_comparaison)
			{
				System.out.println(r);
			}
			
			System.out.println("\n\n---------IMAGE----------\n\n");
			ResultatsC.indexer_repertoire_image();
			
			System.out.println("\n ============================ \n");

			TreeSet<Resultat<String,Float>> resultImage_recherche = ResultatsC.get_recherche_image_result(200, 100, 30);
			
			for(Resultat<String,Float> r : resultImage_recherche)
			{
				System.out.println(r);
			}

			
			
			System.out.println("\n ============================ \n");

			TreeSet<Resultat<String,Float>> resultImage_comparaison = ResultatsC.get_comparaison_image_result("/home/ugo/Desktop/Cours/git/fil-rouge/DATA_FIL_ROUGE_DEV/TEST_RGB/10.txt");

			for(Resultat<String,Float> r : resultImage_comparaison)
			{
				System.out.println(r);
			}


			
			System.out.println("\n\n-----------SON---------\n\n-");
			
			ResultatsC.indexer_repertoire_son("/home/ugo/Desktop/Cours/git/fil-rouge/DATA_FIL_ROUGE_DEV/TEST_SON");

			System.out.println("\n ============================ \n");

			TreeSet<Resultat<String,Float>> resultSon_recherche = ResultatsC.get_recherche_son_result("/home/ugo/Desktop/Cours/git/fil-rouge/DATA_FIL_ROUGE_DEV/TEST_SON/jingle_fi.wav");

			for(Resultat<String,Float> r : resultSon_recherche)
			{
				System.out.println(r);
			}

			System.out.println("\n ============================ \n");

			
			TreeSet<Resultat<String,Float>> resultSon_comparaison = ResultatsC.get_comparaison_son_result("/home/ugo/Desktop/Cours/git/fil-rouge/DATA_FIL_ROUGE_DEV/TEST_SON/jingle_fi.wav");

			for(Resultat<String,Float> r : resultSon_comparaison)
			{
				System.out.println(r);
			}

			
			
		} catch (NoResultsException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		}
		*/		
		
	}

}
