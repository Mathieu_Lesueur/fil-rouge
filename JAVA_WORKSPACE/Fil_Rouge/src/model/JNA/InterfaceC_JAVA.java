package model.JNA;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

public interface InterfaceC_JAVA extends Library
{
	//------------------------------------------------- RECHERCHE
	public void recherche_texte(String mot,IntByReference numValsRef,PointerByReference refFloatArray,PointerByReference refStringArray);
	public void recherche_image(int r,int g,int b,IntByReference numValsRef,PointerByReference refFloatArray,PointerByReference refStringArray);
	public void recherche_son(String path,IntByReference numValsRef,PointerByReference refFloatArray,PointerByReference refStringArray);

	//------------------------------------------------- COMPARAISON
	public void comparaison_texte(String path,IntByReference numValsRef,PointerByReference refFloatArray,PointerByReference refStringArray);
	public void comparaison_image(String path,IntByReference numValsRef,PointerByReference refFloatArray,PointerByReference refStringArray);
	public void comparaison_son(String path,IntByReference numValsRef,PointerByReference refFloatArray,PointerByReference refStringArray);
	
	public void indexer_repertoire_texte(String chemin_repertoire);
	public void indexer_repertoire_image(String chemin_repertoire);
	public void indexer_repertoire_son(String chemin_repertoire);
}
