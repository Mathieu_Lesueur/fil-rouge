package model;

import java.util.TreeSet;

import model.JNA.NoResultsException;
import model.JNA.ResultatsC;

//pour appeler les methodes qu'on a fait avant sur c/Linux
public class Son {
	
	public static void indexer() {
		ResultatsC.indexer_repertoire_son();		
	}
	
	public static TreeSet<Resultat<String,Float>> comparaison(String fic, float mode){
		if(mode == 0) {
			try {
				return ResultatsC.get_recherche_son_result(fic);
			} catch (NoResultsException e) {
				System.out.println(e.getMessage());
			}
			
		}
		
		try {
			return ResultatsC.get_comparaison_son_result(fic);
		} catch (NoResultsException e) {
			System.out.println(e.getMessage());
		}
		return new TreeSet<Resultat<String,Float>>();
	}
}
