package model;
public enum Polarite {
	ABSENT("-"),
	PRESENT("+");
	
	private String abreviation;
	
	private Polarite(String s) {
		this.abreviation=s;
	}
	
}
