package model;

import controler.ControlleurIndexation;
// thread pour lancer l'indexation automatique des fichiers avant de recupere le resultat de recherche
// le plus important c'est quand on lance le start() y'a dans le run qu'on va lancer l'indexation
public class ThreadIndexation extends Thread {

		private ControlleurIndexation control;
		public ThreadIndexation(String s,ControlleurIndexation c) {
			super(s);
			this.control =c;
		}
		
		public void run() {
			switch(this.getName()) {
			case "Texte" : control.lancerIndexationTexte(); break;
			case "Image" : control.lancerIndexationImage(); break;
			case "Son" : control.lancerIndexationSon(); break;
			default : System.out.println("Non reconnu");
			}
		}
}
