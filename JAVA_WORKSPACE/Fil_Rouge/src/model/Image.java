package model;

import java.util.TreeSet;

import model.JNA.NoResultsException;
import model.JNA.ResultatsC;

public class Image {

	public static void indexation_image() {
		ResultatsC.indexer_repertoire_image();		
	}


	public static TreeSet<Resultat<String,Float>> rechercheImageParCritereCouleur(int r, int g, int b) {
		try {
			return ResultatsC.get_recherche_image_result(r, g, b);
		} catch (NoResultsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new TreeSet<Resultat<String,Float>>();
	}

}