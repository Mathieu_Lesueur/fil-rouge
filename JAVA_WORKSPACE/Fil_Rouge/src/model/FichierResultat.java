package model;

import java.io.File;

//Fichier ou sont enregistrer les requetes et les resultats lorsque
// l'utilisateur le veut : BoundaryCasSauvegarderRequete

public class FichierResultat{
	private String chemin;
	private File file;
	private FichierResultat() {
		chemin ="Docs\\fichier.txt";
		file = new File(chemin);
	}
	private static class FichierResultatholder{
		private static final FichierResultat instance = new FichierResultat();
	}
	
	public static FichierResultat getInstance() {
		return FichierResultatholder.instance;
	}
	
	public File getFile() {
		return this.file;
	}
}
