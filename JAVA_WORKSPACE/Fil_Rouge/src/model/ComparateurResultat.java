package model;

import java.util.Comparator;
//pour avoir des TreeSet qui contient des <fichiers,nombreD'ooccurance> tri�s selon les fichiers les plus occurants
// c'est pour les TreeSet<Resultat<String=chemin_fichier,nombreD'occ>>
public class ComparateurResultat implements Comparator<Resultat<String,Float>>{
	@Override
	public int compare(Resultat<String, Float> o1, Resultat<String, Float> o2) {
		if(o2.getNombre().compareTo(o1.getNombre()) == 0) {
			return (o2.getNom().compareTo(o1.getNom()));
		}
			return (o2.getNombre().compareTo(o1.getNombre()));
	}
}
