package controler;


import java.util.TreeSet;

import model.ComparateurResultat;
import model.Resultat;
import model.Son;

public class ControleurComparaisonSon {

		public TreeSet<Resultat<String,Float>> comparaisonSon(String path, int mode){
			TreeSet<Resultat<String,Float>> hash = new TreeSet<Resultat<String,Float>>(new ComparateurResultat());
			hash = Son.comparaison(path, mode);
			return hash;
		}
}

