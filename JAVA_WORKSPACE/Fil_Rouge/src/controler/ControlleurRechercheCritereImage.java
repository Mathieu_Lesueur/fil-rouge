package controler;

import java.util.ArrayList;
import java.util.TreeSet;

import model.ComparateurResultat;
import model.CritereImage;
import model.Resultat;
import model.Image;
import model.Polarite;

public class ControlleurRechercheCritereImage {
	
	private ControlleurCommun controlleurCommun = new ControlleurCommun();
	
	public TreeSet<Resultat<String,Float>> rechercheCritereImage(int r, int g,	int b, int moteur) {
		// pour simuler une plage de couleurs, on fait la moyenne des 2 valeurs
		TreeSet<Resultat<String,Float>> hash = new TreeSet<Resultat<String,Float>>(new ComparateurResultat());
		hash = Image.rechercheImageParCritereCouleur(r,g,b);
		return hash;
	}
	
	//Recherche simple par critere  de Couleur
		// recupere le resultat de la recherche par le controlleur et apres afficher le resultat directe
		// on appelle cette methode, fin cette méthode est utilisé dans un seul cas ou l'utilisateur fait une recherche selon un seul critere
		// comme le cas classique de la partie 1 du projet
		// on appelle cette methode dans la recherche complexe ou l'utilisateur saisie une recherche selon une seule couleur donc lorsque la taille de la liste des critere =1
		public TreeSet<Resultat<String,Float>> rechercheParCritere(int r, int g,int b,int moteur) {
			TreeSet<Resultat<String,Float>> hash = this.rechercheCritereImage(r,g,b,moteur);
			if(hash.isEmpty()) {
				System.out.println(" couleurs R= :" + r + "G = : " + g + "B = :" + b + " non trouvee !");
				return null;
			}else {
				return hash;
			}
//				Boolean choix=boundarySauvegardeRequete.sauvegarderRequete("Requete de recherche image", hash);
//				if(!choix) {
//					System.out.println(" couleurs R= :" + r + "G = : " + g + "B = :" + b + "  trouvee !");
//					
//					for(Resultat<String,Float> x : hash) {
//						System.out.println("Fichier : " + x.getNom()  + " nb occu : " + x.getNombre().intValue());
//					}				
//				}
		}
		
	
	public TreeSet<Resultat<String,Float>> rechercheImage(ArrayList<CritereImage> listeCrit,int moteur){
		TreeSet<Resultat<String,Float>> result = new TreeSet<>(new ComparateurResultat());
		if(listeCrit.size() == 1) {
			result = this.rechercheParCritere(listeCrit.get(0).getR(), listeCrit.get(0).getG(),listeCrit.get(0).getB(),moteur);
		}else {
			//Permet de savoir s'il faut faire un union entre deux polarites + ou entre une + et une -
			Polarite PolariteCritCourant;
			//TreeSet contenant les resultats temporaires des recherches simples
			TreeSet<Resultat<String,Float>> result1 = new TreeSet<>();
			TreeSet<Resultat<String,Float>> result2 = new TreeSet<>();
			for(int i = 0; i < listeCrit.size(); i++) {
				if(i == 0) {
					//Creation de la base initiale des documents qui va pouvoir ensuite etre affine grace aux autres criteres
					result1 = this.rechercheCritereImage(listeCrit.get(i).getR(), listeCrit.get(i).getG(),listeCrit.get(i).getB(),moteur);
					//Si la plage devant etre PRESENT n'est pas trouvee, �a ne sert a rien de poursuivre le traitement
					if(result1.isEmpty()) {
						System.out.println("Aucun document ne correspond a ces criteres !");
						System.exit(1);
					}				
				}else {
					//La base initiale est deja cree, on ne fait plus que comparer le resultat de l'union precedente avec le nouveau critere.
					//-> result1 est donc une copie de result.
					result1.clear();
					result1.addAll(result);
				}				
				result2.clear();
				result2=this.rechercheCritereImage(listeCrit.get(i).getR(), listeCrit.get(i).getG(),listeCrit.get(i).getB(),moteur);
				PolariteCritCourant = listeCrit.get(i).getPolarite();
				result.clear();
				//Si result2 ne donne rien et que result1 a donne un resultat, on ignore simplement ce critere et on poursuit le traitement
				if(!result2.isEmpty()) {
					//Choix de la fonction a utiliser en fonction de la polarite du critere courant a comparer
					if(PolariteCritCourant == Polarite.PRESENT) {
						result.addAll(controlleurCommun.rechercheCritereComplexePP(result1,result2));
					}else {
						result.addAll(controlleurCommun.rechercheCritereComplexePM(result1, result2));
					}
				}else {
					if(PolariteCritCourant == Polarite.ABSENT) {
						result.addAll(result1);
					}
				}
			}				
		}
		return result;
	}
}
