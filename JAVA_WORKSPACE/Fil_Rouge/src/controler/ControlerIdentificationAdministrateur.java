package controler;

import vue.Fichier;

public class ControlerIdentificationAdministrateur {
	
	
	private static String motDePasse;
	
	public ControlerIdentificationAdministrateur() {
		motDePasse = Fichier.get();
	}
	
	public boolean verifMotDePasse(String motDePasse) {
		boolean motDePasseOK = false;
		/* Renforcer la s�curit� du mot de passe */
		if(motDePasse.equals(ControlerIdentificationAdministrateur.motDePasse)) {
			motDePasseOK = true;
		}
		
		return motDePasseOK;		
	}
	
	public static void setMDP(String s) {
		motDePasse=s;
	}
	
}
