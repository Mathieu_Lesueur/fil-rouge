package controler;

import model.Image;
import model.Son;
import model.Texte;


public class ControlleurIndexation {

	public void lancerIndexationTexte() {
		Texte.indexer();
	}
	
	public void lancerIndexationImage() {
		Image.indexation_image(); 
	}
	
	public void lancerIndexationSon() {
		Son.indexer();
	}
}
