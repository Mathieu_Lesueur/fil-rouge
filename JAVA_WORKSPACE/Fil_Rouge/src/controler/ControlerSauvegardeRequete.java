package controler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.TreeSet;

import model.FichierResultat;
import model.Resultat;

public class ControlerSauvegardeRequete {

	public void sauvegarderRequete(String requete,TreeSet<Resultat<String,Float>> resultat) {
		FichierResultat fichierResultat = FichierResultat.getInstance();
		File file = fichierResultat.getFile();
		try {
			PrintWriter printer = new PrintWriter(new FileWriter(file.getAbsolutePath(),true));
			printer.println();
			printer.println(requete);
			for(Resultat<String, Float> r : resultat) {
				printer.println(r.getNom()+ " " + r.getNombre());				
			}
			printer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

}
