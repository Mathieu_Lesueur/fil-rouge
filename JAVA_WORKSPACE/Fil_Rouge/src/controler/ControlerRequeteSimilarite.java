package controler;

import java.io.File;
import java.util.TreeSet;

import model.Resultat;
import model.JNA.NoResultsException;
import model.JNA.ResultatsC;

public class ControlerRequeteSimilarite {
	
	public ControlerRequeteSimilarite() {
				
	}

	public TreeSet<Resultat<String,Float>> requeteSimilariteTexte(int moteur, File file) {
		try {
			return ResultatsC.get_comparaison_texte_result(file.getAbsolutePath());
		} catch (NoResultsException e) {
			System.out.println("No results ");
		}
		return null;		
	}

	public TreeSet<Resultat<String,Float>> requeteSimilariteImage(int moteur, File file) {
		try {
			return ResultatsC.get_comparaison_image_result(file.getAbsolutePath());
		} catch (NoResultsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;	
	}
}
