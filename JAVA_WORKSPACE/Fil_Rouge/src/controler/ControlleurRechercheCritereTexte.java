package controler;

import java.util.List;
import java.util.TreeSet;

import model.ComparateurResultat;
import model.CritereTexte;
import model.Polarite;
import model.Resultat;
import model.Texte;

public class ControlleurRechercheCritereTexte {
	
	private ControlleurCommun controlleurCommun = new ControlleurCommun();
	
	public TreeSet<Resultat<String,Float>> rechercheCritereTexte(String s,int moteur) {
		TreeSet<Resultat<String,Float>> hash = new TreeSet<Resultat<String,Float>>(new ComparateurResultat());
		hash = Texte.recherchecrit(s);
		return hash;
	}
	
	// recherche par un mot : recherche simple
	public TreeSet<Resultat<String, Float>> rechercheParCritere(String s, int moteur) {
				
		TreeSet<Resultat<String, Float>> resultat = this.rechercheCritereTexte(s, moteur);
		if (resultat.isEmpty()) {
			System.out.println("mot : " + s + " n'existe pas");
			return null;
		} else {
			return resultat;
//			Boolean choix = boundarySauvegardeRequete.sauvegarderRequete("Requete de recherche texte", resultat);
//			if (!choix) {
//				System.out.println("mot :" + s + " trouve");
//				for (Resultat<String, Float> r : resultat) {
//					System.out.println("le fichier :" + r.getNom() + " nbre d'occ : " + r.getNombre());
//				}
//			}
		}
	}
	
	public TreeSet<Resultat<String, Float>> rechercheTexte(List<CritereTexte> listeCritere, int moteur)
	{
		
		
		TreeSet<Resultat<String, Float>> result = new TreeSet<>(new ComparateurResultat());	
		if (listeCritere.size() == 1) 
		{
			
			result=rechercheParCritere(listeCritere.get(0).getMotCle(),moteur);				
			
		}else 
		{
			
			// si la listeCritere contient plus qu'un critere
			// >> traitement pour la recherche suivant une liste de critere +intialisation
			// Permet de savoir s'il faut faire un union entre deux polarites + ou entre une
			// + et une -
			Polarite PolariteCritCourant;
			
			// TreeSet contenant les resultats temporaires des recherches simples
			TreeSet<Resultat<String, Float>> result1 = new TreeSet<>();
			TreeSet<Resultat<String, Float>> result2 = new TreeSet<>();
	
			// >> traitement pour la recherche suivant une liste de critere
			
			System.out.println(listeCritere);
			
			for (int i = 0; i < listeCritere.size(); i++) {
				
				
				System.out.println(listeCritere.get(i).getMotCle());

				
				
				if (i == 0) {
					
					result1 = this.rechercheCritereTexte(listeCritere.get(0).getMotCle(), moteur);
					// Si le mot devant être PRESENT n'est pas trouve, ca ne sert a rien de
					// poursuivre le traitement
					if (result1.isEmpty()) {
						System.out.println("[E1] Aucun document ne correspond a ces criteres !");
						System.exit(1);
					}
					
					
				} else {
					// on fait une mise à jour du result1
	
					// La base initiale est deja cree, on ne fait plus que comparer le resultat de
					// l'union precedente avec le nouveau critere.
					// -> result1 est donc une copie de result.
					
					result1.clear();
					result1.addAll(result);
				}
				
				
				
		
				result2.clear();
				result2 = this.rechercheCritereTexte(listeCritere.get(i).getMotCle(),moteur);
				
				
				
				
				PolariteCritCourant = listeCritere.get(i).getPolarite();	
				
				
				
				result.clear();
				if (!result2.isEmpty()) 
				{
						// Choix de la fonction a utiliser en fonction de la polarite du critere courant
						// a comparer
		
						// si la polarite actuelle est '+' alors on va ajouter le resultat2 au resultat
						// 1
						if (PolariteCritCourant == Polarite.PRESENT)
						{
							result = controlleurCommun.rechercheCritereComplexePP(result1, result2);
						} 
						else 
						{
							// sinon on va mettre dans resultat1 : les fichiers de resultat1 - celle du
							// resultat2
							result = controlleurCommun.rechercheCritereComplexePM(result1, result2);
						}
						
				} else 
				{
					// resultat2 ne donne rien : juste mettre dans result result1 (fin c'est result
					// de i-1)
					result.addAll(result1);
				}
			}
			// << fin du traitement pour la recherche suivant une liste de critere
		}
		return result;						
	}
}

