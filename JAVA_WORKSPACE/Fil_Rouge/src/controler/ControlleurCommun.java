package controler;

import java.util.TreeSet;

import model.ComparateurResultat;
import model.Resultat;

public class ControlleurCommun {
	
	
	// garder les fichiers qui se trouve dans les deux Set
	//Requete complexe "+ +", on souhaite garder les fichiers pr�sents dans les deux Set.
	// Parcours des deux Set s'il y a �galit� entre les noms alors on ajoute le fichier � un troisieme set qui sera retourne.
	// PP = polarite + et +	
	public TreeSet<Resultat<String,Float>> rechercheCritereComplexePP(TreeSet<Resultat<String,Float>> resultat1, TreeSet<Resultat<String,Float>> resultat2){
		
		//Set � retourner � la fin du traitement : TreeSet contient une Paire de <nomFichier,nombreD'occurance>
		TreeSet<Resultat<String,Float>> set	=	new TreeSet<>(new ComparateurResultat());
		
		//parcourir le resultat de la recherche de l'etape i-1 (selon le mot cle num i-1)
		for(Resultat<String,Float> r1 :  resultat1) {
			//parcourir le resultat de la recherche de l'etape i (mot cle numero i)
			for(Resultat<String,Float> r2 : resultat2) {
				//si on retrouve le nom du fichier dans les deux resulat de recherche alors on le garde ( c'est l'union + et +)
				if(r2.getNom().equals(r1.getNom())) {
					set.add(new Resultat<String, Float>(r2.getNom(),r2.getNombre()));
				}
			}
		}
		return set;
		
	}
	
	
	//garder les fichiers qui se trouve dans le resultat de la premiere recherche et qui se trouve pas dans le resultat de la recherche suivante(selon le critere suivant vu qu'on a une liste de critere)
	//Requete complexe "+ -", on souhaite garder les fichiers presents dans le Set1 et absents dans le Set2. 
	//Parcours des deux Set avec un booleen permettant de savoir si le fichier est present dans les deux sets ou non.
	// PM = polarite  + et -
	public TreeSet<Resultat<String,Float>> rechercheCritereComplexePM(TreeSet<Resultat<String,Float>> result1, TreeSet<Resultat<String,Float>> result2){
		TreeSet<Resultat<String,Float>> set = new TreeSet<Resultat<String,Float>>(new ComparateurResultat());
		for(Resultat<String,Float> r1	:	result1) {
			boolean existe=false;
			for(Resultat<String,Float> r2 : result2) {
				if(r2.getNom().contains(r1.getNom())) {
					existe=true;
				}
			}
			if(!existe) {
				set.add(new Resultat<String, Float>(r1.getNom(),r1.getNombre()));
			}
		}
		return set;
	}
}
