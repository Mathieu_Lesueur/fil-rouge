#include <stdio.h>
#include <string.h>
#include  <stdlib.h>

#include "../_OUTILS/affichage/outils_affichage.h"
#include "../_OUTILS/autre/element.h"
#include "../_OUTILS/autre/sorted_list.h"

#include "Texte.h"


#define TAILLE 1000






/*
	Hamza Guilhem  
	(+Ugo pour adaptation image)
*/










//variables pour stocker le chemin des differents fichiers qu'on va les cr�er
char LISTE_BASE_TEXTE_PATH[255];
char RESULTAT_PATH[255];
char BASE_DESCRIPTEUR_PATH[255];
char CONFIG_PATH[255];
char NOM_FICHIERS_REPERTOIRE_PATH[255];
char TABLE_INDEX_PATH[255];
char TEXTE_PATH[255];
char LISTE_MOTS_INTERDITS[255];


//chaine contient les mots interdits qui vont �tre supprim�(on supprimes les articles,pronoms....) toute chaine dont on veut la supprimer on la mets dans chaine  mots_interdits
//dans notre cas on a choisi de supprimer 68mots si ils existent
#define nbmots_interdits 68
char* mots_interdits[nbmots_interdits] = {
"à",
"de",
"pour",
"sur",
"dans",
"avec",
"en",
"par",
"parmi",
"au",
"aux",
"du",
"des",
"l'",
"le",
"la",
"les",
"un",
"une",
"des",
"peu",
"certains",
"et",
"je",
"tu",
"il",
"elle",
"nous",
"vous",
"ils",
"elles",
"me",
"te",
"le",
"les",
"nous",
"vous",
"leur",
"lui",
"en",
"y",
"en",
"moi",
"toi",
"lui",
"celui",
"celle",
"ceux",
"celles",
"ceci",
"cela",
"mien",
"tien",
"sien",
"mienne",
"tienne",
"sienne",
"miens",
"tiens",
"siens",
"ton",
"mon",
"son",
"qui",
"que",
"quoi",
"quand",
"si"};



// fonction qui permet de choisir dans quel dossier @path tous les fichiers utilises vont se trouver
// cette fonction permet la partie Image et la partie texte de ne pas partager les memes fichiers de config 
// ou descripteurs
void _init_chemin(char* path)
{
	//TODO ajouter verif que les chemins existen bien 
	
	strcpy(LISTE_BASE_TEXTE_PATH,path);
	strcat(LISTE_BASE_TEXTE_PATH,"/Fichiers/Fichiers_exectution/liste_base_texte.txt");
	
	strcpy(RESULTAT_PATH,path);
	strcat(RESULTAT_PATH,"/Fichiers/Fichiers_exectution/resultat.txt");

	strcpy(BASE_DESCRIPTEUR_PATH,path);
	strcat(BASE_DESCRIPTEUR_PATH,"/Fichiers/Fichiers_exectution/base_descripteur_texte.txt");
	
	strcpy(CONFIG_PATH,path);
	strcat(CONFIG_PATH,"/Fichiers/config.txt");

	strcpy(LISTE_MOTS_INTERDITS,path);
	strcat(LISTE_MOTS_INTERDITS,"/Fichiers/liste_mots_interdits.txt");

	
	strcpy(NOM_FICHIERS_REPERTOIRE_PATH,path);
	strcat(NOM_FICHIERS_REPERTOIRE_PATH,"/Fichiers/Fichiers_exectution/nom_fichiers_du_repertoire_Textes.txt");

		
	strcpy(TABLE_INDEX_PATH,path);
	strcat(TABLE_INDEX_PATH,"/Fichiers/Fichiers_exectution/table_index_texte.txt");

	strcpy(TEXTE_PATH,path);
	strcat(TEXTE_PATH,"/Fichiers/Fichiers_exectution/texte.txt");

/*
	printf("\n %s \n",LISTE_BASE_TEXTE_PATH);
	printf("\n %s \n",RESULTAT_PATH);
	printf("\n %s \n",BASE_DESCRIPTEUR_PATH);
	printf("\n %s \n",LISTE_MOTS_INTERDITS);
	printf("\n %s \n",NOM_FICHIERS_REPERTOIRE_PATH);
	printf("\n %s \n",TABLE_INDEX_PATH);
	printf("\n %s \n",CONFIG_PATH);
	printf("\n %s \n",TEXTE_PATH);
*/
}



/*************************************** PARTIE POUR LA CREATION DU DESCRIPTEUR **************************************************************/

//Fonction permettant de retourner le fichier associe au chemin ( chemin : c'est le chemin du fichier)
FILE* loadFile(char* chemin)
{
	
	FILE* fichier = NULL;
	fichier=fopen(chemin,"r+");
	if(fichier!=NULL){
		return fichier;		
	}
	else
	{
		printf("\n [ERREUR] Erreur ouverture du fichier %s \n",chemin);
		return NULL;
	}	
}

//Fonction supprimant les balises d'une chaine  ( c: la chaine de caractere passe en parametre auquelle on va supprimer les balises)
void traitement_balise(char* c){
	int i=0;
	int j=0;
	char* s=c;
	
	while(c[i]!='\0'){
		if(c[i]=='<'){
			while(c[i]!='>'){
				i++;
			}
			if(c[i]=='>'){
				i++;
			}			
		}else{
			if(c[i]=='>'){
				i++;
			}
			s[j]=c[i];
			i++;
			j++;
		}
	}
	s[j]='\0';
}



//Fonction permettant de supprimer la ponctuation (conserve uniquement les chiffres -0 à 9-, l'alphabet -maj ou min-, l'apostrophe, les lettres accentu�s
//dans le cas d'une apostrophe on la remplace par un espace ' '
// ch pass� en parametre c'est la chaine auquelle on va supprimer tous les ponctuation
void traitement_ponctuation(char* ch){
	char* s=ch;
	int i=0,j=0;
	while(ch[i]!='\0'){
		int x=ch[i];
		// code ascii < 0 pour les lettre accentu�s
		if((x>64&&x<91)||(x>47&&x<58)||(x==32)||(x>96&&x<123)||(x==39)||(x<0)){
			if(x==39){
				s[j]=' ';
				i++;
				j++;
			}else{
			s[j]=ch[i];
			i++;
			j++;
			}
		}else{
		s[j]=' ';
		i++;
		j++;
		}
	}
	s[j]='\0';
}



//transforme la string 'thestring' en tableau de  mots contenant les 'nbMots' 
//en fait cette fonction transforme la chaine en un tableau de chaine
//on aura besoin de cette fonction dans la fonction traitement_article
//si on parcour le tableau de mots et on trouve un mots_interdits(un article) on le remplace ce mots par la chaine vide
char** string_to_tableau(char* thestring, int taille_max, int* nbMots)
{

	char** word_array = (char**) malloc(taille_max * sizeof(char*));
	if(word_array == NULL)
	{
		printf("[ERREUR] string_to_tableau : malloc fail");
	}

	for(int i =0; i<taille_max; i++)
	{
		word_array[i] = (char*) malloc(30 * sizeof(char));
	}

	int nb_words = 0;



	const char * separators = " ,.-!";
	char * strToken =malloc(sizeof(char)*255);
    // On cherche à récupérer, un à un, tous les mots (token) de la phrase
    // et on commence par le premier.
    strToken = strtok ( thestring, separators );

    while ( strToken != NULL ) {
		
        strToken = strtok ( NULL, separators );
		if(strToken != NULL)
		{
			strcpy(word_array[nb_words],strToken);
		}
		nb_words++;

    }

	*nbMots = nb_words;

	return word_array;

}


//retransforme le tableau de mots en string 
//c'est la fonction reciproque de string_to_tableau
char* tableau_to_string(char** tableau_mots,int tailletableau)
{
	char* string = (char*)malloc(TAILLE*sizeof(char));

	if(tailletableau>0)
	{
		strcpy(string,tableau_mots[0]);

		for (int i = 1; i < tailletableau; i++)
		{
			strcat(string," ");
			strcat(string,tableau_mots[i]);
		}
	}
	return string;
}




//Fonction permettant de supprimer les articles 
//on passe en parametre le tableau de mots et le nombre de mots (c'est la taille du tableau de mots)
void traitement_articles(char** tab_mots, int nb_mots) //maintenant il prend en param le tableau de mots directement
{


	for (int i = 0; i < nb_mots; i++)
	{

		if(0==strcmp(tab_mots[i]," ") || 0==strcmp(tab_mots[i],"\0"))
		{

			strcpy(tab_mots[i],"\0");

		}
		else
		{
		
			//on parcour le tableau de mots interdits
			for (int y = 0; y < nbmots_interdits; y++)
			{
				//si le mot fait parti du tableau 
				if(strcmp(mots_interdits[y],tab_mots[i])==0)
				{
					//on le suprime
					strcpy(tab_mots[i],"\0");
				}


			}
			
		}



	}
}




//Fonction permettant de garder que les mots de taille 3 ou plus
//on parcour le tableau de mots si on trouve un mots dont la taille est < � 3 on le remplace par la chaine vide ( "\0" )
void traitement_taille(char** tab_mots, int nb_mots,int min_len) 
{
	for (int i = 0; i < nb_mots; i++)
	{
		if (strlen(tab_mots[i])<min_len)   //une fonction tres simple qui parcour le tableau de mots et enleve si il est plus court que min_len 
		{
			//printf("\n on eleve le mot %s",tab_mots[i]);
			strcpy(tab_mots[i],"\0");
		}	
	}
}



//Fonction permettant de retourner un fichier sans les elements non importants
// on obtient a la fin un texte brut 
// on passe en parametre le fichier initale (xml) et la taille minimale des mots � garder a la fin => on obtient un texte brut
void processFile(FILE* f,int taille_min_mot)
{

	FILE* fichier=fopen(TEXTE_PATH,"w+");

	if(fichier == NULL)
	{
		printf("[ERREUR] processFile -> Probleme overture fichier");
	}
	
	char ligne[TAILLE];

	while(fgets(ligne,TAILLE,f)!=NULL){

		traitement_balise(ligne);

		traitement_ponctuation(ligne);

		//apres avoir fait ce traitement on transforme la string(ligne) en tableau de mot pour **plus de facilite**
		int taille_tableau_renvoye = -1;
		char** tableau_mots = string_to_tableau(ligne,TAILLE,&taille_tableau_renvoye);

		traitement_taille(tableau_mots,taille_tableau_renvoye,taille_min_mot);

		traitement_articles(tableau_mots,taille_tableau_renvoye);

		//on retransforme le tableau en string et on ecrit la chaine dans le fichier brut
		fputs(tableau_to_string(tableau_mots,taille_tableau_renvoye),fichier);

		free(tableau_mots);

	}

	if(fclose(fichier))
	{
		printf("[ERREUR] processFile -> Probleme fermeture fichier");
		exit(1);
	}
		
}

//verifier si un mots existe dans la cellule du base descripteur(dans le tableau de terme) : en effet,
//la fonction existe permet de verifier si le mots ch existe dans le tableau de terme t avec y la taille du tableau de terme t(pour connaitre la fin du parcours de la boucle for
//existe retour -1 si le mots n'existe pas sinon elle retourne l'indice de la premiere occurance de ce mots comme ca on a la position de ce mots dans le tableau de terme il suffit juste d'acceder à cette case et ajouté +1 dans le nombre d'occurance de ce mots
int existe(char *ch,terme *t,int y){

	int i;
	for(int i=0;i<=y;i++)
	{
		if(strcmp(ch,t[i].mot)==0)
		return(i);	
	}
	return(-1);
}


//trier le tableau de terme apres son remplissage par tous les mots qui se trouve dans le fichier texte brut
//le tri se fait selon le nombre d'occurance de la plus grande valeur a� la plus petite
// c'est un tri � bulle
void trie_selon_occ(terme* t,int taille_tab_de_mots){
	terme temp;
	int i,j;
	for(i=0;i<=taille_tab_de_mots;i++){
		for(j=i;j<=taille_tab_de_mots;j++){
			if(t[j].nbocc>t[i].nbocc)
			{
				temp=t[i];
				t[i]=t[j];
				t[j]=temp;
			}			
		}		
	}
}

//initalisation de la pile : en fait on va stocker dans une pile chaque cellule du descripteur 
PILE init_PILE(){
	return(NULL);		
}


//methode pour afficher la pile => afficher le descripteur qui contient des cellule descripteur(le descripteur de chaque fichier)
void affiche_p(PILE p){
	int i;
	if(p==NULL){
	printf("pile est vide\n");
	}
	while(p!=NULL){	
		printf("l id est %d\n",p->id);
		printf(" le nbre totale de mot est %d\n",p->nbre_totale_des_mots);
		printf("la val limites est %d\n",p->val_limite);
		for(i=0;i<=p->taille_tab;i++){
			printf("le mot est %s occurance est %d\n",p->mots_tab[i].mot,p->mots_tab[i].nbocc);
		}		
		printf("juste separation est %s\n","**");		
		p=p->cellule_suiv;
	}
}

//retourne la taille de la pile pour connaitre l'identifiant de chaque cellule descripteur
// en fait, l'identifiant de chaque fichier c'est la taille de la pile +1
int taille_pile(PILE p){
	int x=0;
	while(p!=NULL){
		x++;
		p=p->cellule_suiv;
	}
return(x);
}

//*****************c'est la fonction pricipale qui permet de creer le descripteur*********
//	explication : 
//TAILLE_MAX c'est la taille maximale des mots qu'on peux garder
//VALEUR_MAX  c'est le nombre de terme a garder dans chaque cellule descripteur
//la pile p contient deja des cellule descripteur
// cette fonction permet de creer une nouvelle cellule descripteur du fichier passe en parametre f
PILE base_descripteur(FILE* f,PILE p, int VALEUR_LIMITE,int TAILLE_MAX){
	
	if(f == NULL)
	{
		printf("\n[DEBUG] base_descripteur\n");
		fflush(stdout);
		exit(1);
	}
	
	
	char ch[TAILLE];
	char delim[]=",  \n";
	char* token;	
	//creation de la cellule descripteur c du fichier f
	PILE c;
	c=(PILE)malloc(sizeof(cellule));



	if(c==NULL)
	{
		printf("MALLOC FAIL\n");
		exit(1);
	}

	c->id=taille_pile(p)+1;
	c->taille_tab=-1;




	while(fgets(ch,TAILLE,f)!=NULL)
	{
	// ch contient chaque ligne du fichier texte brut f
		token=strtok(ch,delim);

		while(token!=NULL)
		{
			//token reprensente un mots de la chaine ch			
			//verifie si le mots tokens existe dans le tableau de terme			
			int x=existe(token,c->mots_tab,c->taille_tab);

			//x=-1 si il n existe pas sinon x=posiion de sa premier occurance
			if(x>=0)
			{
				//si le mots existe on incr�mente son nombre d'occurance dans le fichier
				c->mots_tab[x].nbocc++;
				//on recupere le mots suivant
				token=strtok(NULL,delim);
			}else
			{
				//si token (le mot) n existe pas mais sa taille est superieur a taille_max donc on incremente le nbre de mots totale dans le fichier comme même mais on ne l'ajoute pas dans le tableau de terme
				if(strlen(token)>TAILLE_MAX)
				{

					c->nbre_totale_des_mots++;
					token=strtok(NULL,delim);


				//token n'existe pas dans le tableau de terme et sa taille < taille_max donc on ajoute token dans le tableau de terme et on initialise son occurance à 1 puisque il n'existe pas dejà et on incremente le nbre totale de mots dans le fichier
				}
				else
				{
					c->taille_tab++;
					int taille_avant = c->taille_tab;

					char* mot  = c->mots_tab[c->taille_tab].mot;

					if(c->taille_tab >= 5000)
					{
						printf("\n[ERREUR] base_descripteur -> trop de mots  1 taille_tab %d and %d | mot = %s | token = %s \n",c->taille_tab,taille_avant,mot,token);
						fflush(stdout);
						exit(1);
					}
					
					//ajouter un nouveau terme dans le tableau des termes
					strcpy((c->mots_tab[c->taille_tab]).mot,token);
					c->mots_tab[c->taille_tab].nbocc=1;	
					
						
					c->nbre_totale_des_mots++;
					token=strtok(NULL,delim);
	
				}
			}
		}
	}



	//jusqu'a ici on a creer le tableau de terme contenant tous les mots dans le fichier avec leurs nbre d occurance
	//trier le tableau de terme selon les mots les plus occurant
	trie_selon_occ(c->mots_tab,c->taille_tab);
	//garder seulement les VALEUR_LIMITE de mots dans le tableau
	c->taille_tab=VALEUR_LIMITE-1;
	
	c->val_limite=VALEUR_LIMITE;

	//liee la cellule a� la liste des descripteur(la pile p)
	c->cellule_suiv=p;
	return(c);
}

//sauvegarde la pile dans un fichier base descripteur son chemin est path
//oomme ca meme si on ferme le programme on retrouve le descripteur des fichiers qu on a creer dans le fichier base descripteur
void sauvegardePile(PILE p,char* path){

	FILE* f=fopen(path,"a+");
	fclose(f);
	f=fopen(path,"w");

	if(f==NULL)
	{
		printf("\n [ERREUR] Erreur ouverture du fichier %s pour ecriture \n",path);
		fflush(stdout);
		exit(1);
	}	

	int i;
	while(p!=NULL){	
		fprintf(f,"%d\n",p->id);
		fprintf(f,"%d\n",p->nbre_totale_des_mots);
		fprintf(f,"%d\n",p->val_limite);
		for(i=0;i<=p->taille_tab;i++){
			

			fprintf(f,"%s %d\n",p->mots_tab[i].mot,p->mots_tab[i].nbocc);
		}		
		fprintf(f,"%s\n","**");		
		p=p->cellule_suiv;
	}
	fclose(f);
}



//Empile une cellule de descripteur dans la pile qui contient les cellules des descripteurs des fichiers dej� index�
// les parametres:
// p c'est la pile contenant des cellules de descripteurs
// tab c'est le tableau de terme qu'on va le stocker dans la cellule de descripteur et indice c'est la taille de ce tableau de terme
PILE empiler_pile(PILE p ,mots* tab,int indice){
	int i;
	PILE paux=(PILE)malloc(sizeof(cellule));
	if(paux==NULL){
	printf("ici erreur in function empiler_pile\n");
	}
	int x=atoi(tab[2].un_mots);
	paux->taille_tab=-1;
	paux->id=atoi(tab[0].un_mots);
	paux->nbre_totale_des_mots=atoi(tab[1].un_mots);

	paux->val_limite=x;
	for(i=3;i<indice;i=i+2){
		paux->taille_tab++;
		strcpy(paux->mots_tab[paux->taille_tab].mot,tab[i].un_mots);
		paux->mots_tab[paux->taille_tab].nbocc=atoi(tab[i+1].un_mots);
		
	}
	paux->cellule_suiv=p;
	return(paux);
}

/*charger la pile a partir du fichier base_descripteur_texte
en parametre : 
la pile p c'est la pile vide pour le momenet
path c'est bien le chemin du fichier texte brut
le retour : 
on retourne la pile qui contient les descripteurs des fichiers
*/
PILE chargerPile(PILE p,char *path){

	//Charger le fichier base_descripteur_texte, si il n'existe pas il sera cree


	FILE* base_descripteur_texte=fopen(path,"a+");


	fclose(base_descripteur_texte);


	base_descripteur_texte = loadFile(path);
	if(base_descripteur_texte == NULL)
	{
		printf("\n [ERREUR] chargerPile :impossible a ouvrir");
		fflush(stdout);
		exit(1);
	}	
	

	char ch[256];
	char* piece;
	int indice=-1;
	mots tab[255];

	while(fgets(ch,255,base_descripteur_texte)!=NULL){
		piece=strtok(ch," \n");
		if(strcmp(piece,"**")==0){

			p=empiler_pile(p,tab,indice);

			fgets(ch,255,base_descripteur_texte);
			indice=-1;
		}
		//on recupere tous ce qu'il y'a dans une cellule de descripteur(l id , le nbre de mots, la valeur limites et le tableau de termes/
		//on distingue chaque fin de cellule par deux ** dans la lignes suivantes
		while(piece!=NULL){
			indice++;
			strcpy(tab[indice].un_mots,piece);
			piece=strtok(NULL," \n");
		}
	}


	fclose(base_descripteur_texte);
	return(p);
	
}

/* ************************************ PARTIE POUR LA CREATION DE LA TABLE INDEXATION *****************************************************/


//initialisation du table d indexation
TABLE init_table(){
	
	return(NULL);
}

//ecrire la table d'indexation cree dans le fichier f qui est le fichier table d indexation
//table c'est la table d'indexation
// f c'est le fichier auquel on va stocker la table d'indexation => c'est bien le fichier table d'indexation
void sauvegardeTable(TABLE t,FILE* f){
	while(t!=NULL){
		fprintf(f,"\t%s\n",t->mot);
		for(int i=0;i<=t->taille_tab_fic;i++){
			fprintf(f,"%d \t\t",t->tab[i][0]);
			fprintf(f,"%d \n",t->tab[i][1]);
		}
		t=t->cellule_suiv;
	}	
}

//verifie si le mots s existe dans la Table d indexation t (TABLE est comme etant une pile qui contient des cellules du table d indexation
// s est un mots
// ta c'est bien la pile contenant la table d'indexation
int existe_mots_dans_table(char*s, TABLE ta){
	while(ta!=NULL){
		if(strcmp(ta->mot,s)==0){
			return(1);
		}
		ta=ta->cellule_suiv;
	}
	return(0);
}


//empiler un mot dans la table d'indexation
//en parametre il y'a la pile p qui contient la liste des descripteur deja cree, ta c est la table d indexation,s c est le le mots a empiler s il n existe pas dans la table ta
TABLE empiler_un_mots_dans_table(PILE p,TABLE ta,char* s){	
	PILE ppaux=p;	
	
	//si le mots existe deja on retourne la tale et rien a ejouter
	if(existe_mots_dans_table(s,ta)==1){
		return(ta);
	}
	//si le mots n existe pas
	//partie empiler un mots
	TABLE t=(TABLE)malloc(sizeof(cellule_table_index));
	if(t==NULL){
	printf("erreur ici dans empiler_un_mots_dans_table\n");
	}
	t->taille_tab_fic=-1;
	strcpy(t->mot,s);
	//cherche si le mots existe ajouter l identifiant du fichier auquel le mots se trouve avec son nombre d'occurence
	while(ppaux!=NULL){
		int x=existe(s,ppaux->mots_tab,ppaux->taille_tab);
		if(x>=0){
			t->taille_tab_fic++;
			t->tab[t->taille_tab_fic][0]=ppaux->id;
			t->tab[t->taille_tab_fic][1]=ppaux->mots_tab[x].nbocc;					
		}
		ppaux=ppaux->cellule_suiv;
	}	
	t->cellule_suiv=ta;
	//ajout d'un seul mots terminé
	return(t);
}


//creation de la table d'indexation à partir de la pile de descripteur
//p c'est la pile contenant les descripteurs des fichiers indexes
//le retour c'est la pile contenant la table d'indexation cr�e
TABLE creation_table(PILE p){
	TABLE t=init_table();
	PILE paux=p;
	//parcourir toute la pile, pour chaque cellule du base_descripteur empiler tous les mots s'ils  n'existent pas dejà dans la table
	while(paux!=NULL){
		for(int i=0;i<=paux->taille_tab;i++){
			t=empiler_un_mots_dans_table(p,t,paux->mots_tab[i].mot);
		}
		paux=paux->cellule_suiv;
	}
	return(t);
}


//trie de la table d'indexation
//en parametre
// t c'est la table d'indexation non tri�es
//p c'est la pile contenant les descripteurs des fichiers dej� index�s
//le retour
//cette fonction retourne la pile contenant la table d'indexation tri�s
TABLE tri_TABLE(TABLE t,PILE p){
	TABLE aux=init_table();
	TABLE tt=t;
//aux contient a la fin la table trie

	while(tt!=NULL){
		int x=tt->mot[0];
		// on empiler au debuts les cellules de la table d indexation dont leur mot commence par un chiffres
		if(x>47&&x<58){
			aux=empiler_un_mots_dans_table(p,aux,tt->mot);
		}
		tt=tt->cellule_suiv;	
	}
	TABLE ppaux=t;
	while(ppaux!=NULL){
		int x=ppaux->mot[0];
		if(x<=64 || x>=123){
			//on empiler apres dans aux les mots dont leurs mot commence par une lettre non alphabetique et pas un chiffre(caratere special par exemple)
			aux=empiler_un_mots_dans_table(p,aux,ppaux->mot);
		}
		ppaux=ppaux->cellule_suiv;	
	}	
	for(int i=122;i>96;i--){
		TABLE paux=t;
		while(paux!=NULL){
			int x=paux->mot[0];
			if((x==i)||(x==i-32)){
				//et finalement on empiler dans la table d indexation aux les cellules de la table d indexation dont leur mot commence par une lettre alphabetique mais cette fois on empiler dans l'ordre alphabtique on empiler les mots par ordre alphabetique descroissant(de Z/z a A/a)
				aux=empiler_un_mots_dans_table(p,aux,paux->mot);
			}
			paux=paux->cellule_suiv;
		}
	}
	return(aux);
}


//verifie si le fichier de chemin ch est dejà indexé ou pas
// retourne l'id du fichier si il est deja indexe
//sinon retourne 0
//=> verifie si le fichier de chemin ch est deja indexe ou pas
int indexation_deja_faite(char* ch){

	FILE* f = fopen(LISTE_BASE_TEXTE_PATH,"r+");
	char* c = (char*)malloc(255*sizeof(char));
	if(f == NULL)
	{
		printf("\n [ERREUR] indexation_deja_faite : fichier 'liste_base_texte' impossible a ouvrir -> %s",LISTE_BASE_TEXTE_PATH);
		fflush(stdout);
		exit(1);
	}
	
//dans ce fichier f (liste_base_texte) : il contient le chemin du fichier indexe suivi de son identifiant(qui se trouve dans la ligne juste apres)
//donc on verifie dans chaque ligne du fichier si on trouve le chemin ch(passe en parametre) on retourne l'identifiant de ce fichier(qui se trouve dans la ligne juste apres)

	while(fscanf(f,"%s",c)==1)
	{
		if(strcmp(ch,c)==0)
		{
			char* identifiant = (char*)malloc(255*sizeof(char));;
			fscanf(f,"%s",identifiant);
			fflush(stdout);
			int x = atoi(identifiant);
			free(identifiant);
			return(x);
		}
	}

	fclose(f);	
	return(0);
}

/*
retourne 
-1 si ouverture foire.
l'id du fichier deja indexé si deja indexe 
chemin_fichier c'est comme son nom l'indique le chemin du fichier
*/
int indexation_texte(char* chemin_fichier){




	//initaliser la pile � vide
	PILE p=init_PILE();


	//Charger la pile à partir de ce fichier

	printf(".");
	fflush(stdout);


	p = chargerPile(p,BASE_DESCRIPTEUR_PATH);



	//fichier liste_base_texte
	FILE* liste_base_texte=fopen(LISTE_BASE_TEXTE_PATH,"a");
	int VALEUR_LIMITE,TAILLE_MAX,TAILLE_MIN_MOT,TAILLE_BIT_IMAGE;
	FILE* f=fopen(CONFIG_PATH,"r");



	if(f == NULL)
	{
		return -1;
	}
	fscanf(f,"%d %d %d %d",&VALEUR_LIMITE,&TAILLE_MAX,&TAILLE_MIN_MOT,&TAILLE_BIT_IMAGE);


	if(!indexation_deja_faite(chemin_fichier))
	{
		FILE* f=loadFile(chemin_fichier);
		if(f ==NULL)
		{
			return -1;
		}



		char commande2[1000];
		strcpy(commande2,"echo ");
		strcat(commande2,chemin_fichier);
		strcat(commande2," >> ");
		strcat(commande2,LISTE_BASE_TEXTE_PATH);
		system(commande2);
		


		FILE* fic_brut=NULL;
		

		if(f==NULL)
		{

			return -1;
		}
		else
		{
			processFile(f,TAILLE_MIN_MOT);
			fclose(f);


			fic_brut=fopen(TEXTE_PATH,"r+");
			//créer la cellule descripteur du fichier f et l'empiler dans la pile p			
			

			p=base_descripteur(fic_brut,p,VALEUR_LIMITE,TAILLE_MAX);
			

			char ff[3];	
			char commande3[1000];
				
			strcpy(commande3,"echo ");
			sprintf(ff,"%d",taille_pile(p));
			strcat(commande3,ff);
			strcat(commande3," >> ");
			strcat(commande3,LISTE_BASE_TEXTE_PATH);
			strcat(commande3,"\n");



			system(commande3);
			


			fclose(fic_brut);	
			

		}


		remove(TEXTE_PATH);


	}
	else
	{

		return 2;
	}
	



	fclose(liste_base_texte);
	//fclose(base_descripteur_texte);
	//sauvegarde de la pile dans le fichier base_descripteur_texte


	sauvegardePile(p,BASE_DESCRIPTEUR_PATH);
	

	TABLE t=creation_table(p);
	t=tri_TABLE(t,p);
	FILE* table_index_texte=fopen(TABLE_INDEX_PATH,"w+");
	

	sauvegardeTable(t,table_index_texte);

	fclose(table_index_texte);

	free(p);


	return 0;

}


//fonction pour charger ou mis a jour le fichier config par les valeur passe en parametre
//VALEUR_LIMITE : le nbre de mots a garder dans le tableau de terme de chaque cellule du descripteur
//TAILLE_MAX : garder seulement les mots qui ont une taille inferieur ou egale a cette taille
//TAILLE_MIN_MOT: garder seulement les mots qui ont une taille superieur ou egale a cette taille
//NB_BITS_IMAGE : pour le traitement des images
void modifier_fic_config(int VALEUR_LIMITE,int TAILLE_MAX,int TAILLE_MIN_MOT,int NB_BITS_IMAGE){
	//chargement du fichier configuration
	
	FILE* config=fopen(CONFIG_PATH,"a+");
	fclose(config);
	config=fopen(CONFIG_PATH,"w");
	
	if(config==NULL)
	{
		printf("\n [ERREUR] Erreur ouverture du fichier %s pour ecriture \n",CONFIG_PATH);
		fflush(stdout);
		exit(1);
	}	

	fprintf(config,"%d %d %d %d",VALEUR_LIMITE,TAILLE_MAX,TAILLE_MIN_MOT,NB_BITS_IMAGE);
	fclose(config);

}


//recherche par mot cle
// en parametre : mots_cle le mots cl� � saisir
SORTED_LIST recherche_par_mot_cle(char *mot_cle){

	PILE p=init_PILE();
	p = chargerPile(p,BASE_DESCRIPTEUR_PATH);
	PILE paux=p;
	
	SORTED_LIST maliste = init_FILE();
	while(paux != NULL )
	{


			int mot_index =cellule_contient_mot(mot_cle, *paux);
			if(mot_index >= 0) // si le mot existe dans la cellule
			{
			
				ELEMENT* element = malloc(sizeof(ELEMENT));
				if(element != NULL)
				{

					//on ajoute le nombre d'occurence du mot
					affectELEMENT(element,paux->mots_tab[mot_index].nbocc,get_path_from_id(paux->id));
					maliste = emPILE(maliste, *element);



				}else
				{
					printf("[ERREUR]recherche_par_similarite : element malloc failled ");
					fflush(stdout);
					exit(1);
				}
			}
		paux=paux->cellule_suiv;
	}

	return maliste;

}


//retourne 1 si le mot ch existe dans le tab_de_mot , x c'est la taille du tableau de mots
int existe_dans_tab_de_terme(char* ch, mots* tab_de_mot,int x){
	int i;
		for(int i=0;i<x;i++){
			if(strcmp(ch,tab_de_mot[i].un_mots)==0){
			return(1);
			}
		}

return(0);
}


/* Fonction qui retounre la similarite en % de la cellule avec l'id1 et celle de l'id2 
la pile p contient les cellules de descripteurs des ficheirs index�s
*/
float get_similarite_percent(PILE p,int id1, int id2)
{


	PILE paux=p;

	//recuperation des cellules
	cellule* cel1 = (cellule* )malloc(sizeof(cellule));
	cellule* cel2 = (cellule* )malloc(sizeof(cellule));


	if(cel1 == NULL || cel2 == NULL)
	{
		printf("[ERREUR] get_similarite_percent : malloc fail");
		fflush(stdout);
		exit(1);
	}

	while(paux!=NULL)
	{
		if(paux->id==id1)
		{
			cel1 = paux;
			//printf("\n P1 trouvee");
		}
		if(paux->id==id2)
		{
			cel2 = paux;
			//printf("\n P2 trouvee");

		}
		paux=paux->cellule_suiv;
	}

	//une fois qu'on a fait la recuperation
	// on check que c'est pas fucked
	if(cel1 == NULL || cel2 == NULL)
	{
		printf("[ERREUR] get_similarite_percent : Recuperation cellules failled");
		fflush(stdout);
		exit(1);
	}



	// si les cellues sont recuperées alors on peut comparer leurs termes
	//pour tous le tableau de termes de cellule 1 

	int total_mots_similaires = 0;

	for (int i = 0; i < cel1->nbre_totale_des_mots ; i++)
	{

		int contient = cellule_contient_mot(cel1->mots_tab[i].mot, *cel2);

		if(contient != -1)
		{

			int nb_mots_similaires = get_similarite_termes(cel1->mots_tab[i], cel2->mots_tab[contient]);
			total_mots_similaires += nb_mots_similaires;

		}
	}


	int nb_mots = get_nb_mots(*cel1);
	if(nb_mots)
	{  //pour eviter le div /0



		float percentage = (float)total_mots_similaires/(float)nb_mots;


		return percentage;

	}
	else
	{
		return 0;
	}
	


}



//retourne le nombre de mots total d'une cellule de descripteur
// fonction qui va etre utilis� dans la partie texte mais vu qu'on partage la majorit� du code elle implemnt� dans le fichier TEXTE_Texte.c
int get_nb_mots(cellule cel)
{

	int nb_mots = 0;

	for (int i = 0; i < cel.nbre_totale_des_mots; i++)
	{

		if( strcmp(cel.mots_tab[i].mot,"") != 0)
		{
			nb_mots += cel.mots_tab[i].nbocc;
		}
	}
	return nb_mots;
}


// retourne -1 si cel ne contient pas le mot
// retourne i, l'indice du mot dans le tableau si il existe
int cellule_contient_mot(char* mot, cellule cel)
{
	//printf("\n [DEBUG] cellule_contient_mot %d \n",cel.val_limite);

	for (int i = 0; i < cel.nbre_totale_des_mots; i++)
	{
		//printf("\n [DEBUG] le mot %s et le mot %s \n",mot, cel.mots_tab[i].mot);

		if( strcmp(cel.mots_tab[i].mot,mot) ==0)
		{
			return i;
		}
	}
	return -1;
}

//fonction qui permet d'avoir le pourcentage de similarite entre deux termes 
// 0 si rien en commun 
// sinon renvoie le plus nombre de termes similaires
int get_similarite_termes(terme terme1, terme terme2)
{
	// si les deux mots sont pas les memes, la difference est -1
	if( strcmp(terme1.mot,terme2.mot) != 0 )
	{
		return 0;
	}

	if(terme1.nbocc < terme2.nbocc)
	{
		return terme1.nbocc;
	}
	else
	{
		return terme2.nbocc;
	}
	

}


/*retourne le nom du chemin a partir de l'id
on cherche le nom du chemin dans le fichier liste base texte � partir de l'identifiant
retourne NULL si on trouve pas le chemin qui correspond a cet id
*/
char* get_path_from_id(int id)
{
	FILE* liste_base_texte=fopen(LISTE_BASE_TEXTE_PATH,"r");	
	
	if(liste_base_texte != NULL)
	{
		char* chemin = malloc(sizeof(char)*255);
		if(chemin != NULL)
		{
			char char_id[255];
			while(fscanf(liste_base_texte,"%s",chemin)==1)
			{
				fscanf(liste_base_texte,"%s",char_id);
				int index = atoi(char_id);
				if(index == id)
				{
					break;
				}
			}
		}
		fclose(liste_base_texte);
		return chemin;
	}
	return NULL;
}


/* fonction pour la recherche par similarite
en parametre :
chemin_fichier c'est bien le chemin du fichier qu'on veut comparer avec les fichiers indexer
*/
SORTED_LIST recherche_par_similarite(char* chemin_fichier)
{
	int resultat_deja_fait = indexation_deja_faite(chemin_fichier);

	if(resultat_deja_fait!=0)
	{
		//printf("Fichier existe deja dans la base, indice = %d",resultat_deja_fait);
	}else
	{

		int resultat = indexation_texte(chemin_fichier);

	}

	int index_fichier = indexation_deja_faite(chemin_fichier);
	mots tableau_de_mot[3];
	PILE p=init_PILE();
	p = chargerPile(p,BASE_DESCRIPTEUR_PATH);
	PILE paux=p;
	SORTED_LIST maliste = init_FILE();
	while(paux != NULL )
	{

			float pourcentage = get_similarite_percent(p,index_fichier,paux->id)*100 ;

			if(pourcentage >= 1)
			{
				ELEMENT* element = malloc(sizeof(ELEMENT));
				if(element != NULL)
				{
					affectELEMENT(element,pourcentage,get_path_from_id(paux->id));
					maliste = emPILE(maliste, *element);
				}else
				{
					printf("[ERREUR]recherche_par_similarite : element malloc failled ");
					fflush(stdout);
					exit(1);

				}
				
			}


		paux=paux->cellule_suiv;
	}
	return maliste;
}



