/****************************************** DESCRIPTEUR ***********************************************/
/*
le descripteur des ficheirs est stock� dans une pile
chaque cellule de cette pile est compos� de
-un entier qui represente l'identifiant du fichier a indexer
-un entier contient le nombre totales de motq
un entier contient la valeur limite des mots � garder dans chaque cellule de descripteur
-un tableau de terme : avec un terme c'est un mot suivi de son nombre d'occurance
-un pointeur vers la cellule suivantes
*/

typedef struct terme{
	char mot[50];
	int nbocc;
}terme;

typedef struct cellule{
	int id;
	terme mots_tab[5000];
	int taille_tab;
	int val_limite;
	int nbre_totale_des_mots;
	struct cellule* cellule_suiv;
}cellule;

typedef struct cellule* PILE;

struct mot{
	char un_mots[50];
};
typedef struct mot mots;

/****************************************** TABLE INDEXATION ********************************************************/
/*
la table d'indexation est caussi stock� dans une autre pile
chaque cellule de cette pile est compos� de
-un mots qui contient un mot-cl�
-un tableau de deux dimension contient dans la premiere colonnes les identifiant des ficheirs et dans la 2�me colonne le nombre d'occurance du mots-cl� dans ce fichier
-un pointeur vers la cellule suivantes
*/
typedef struct cellule_table_index{
	char mot[50];
	//on connait pas le nombre de ligne mais il y'a deux colonnes seuelement	
	int tab[200][2];
	int taille_tab_fic;
	struct cellule_table_index* cellule_suiv;
}cellule_table_index;

typedef struct cellule_table_index* TABLE;


/*************************************** FONCTIONS ************************************************************************/

//Fonction permettant de récupérer le chemin entré par l'utilisateur
//void lirePath(char* c);
//Fonction permettant de retourner le fichier associe au chemin
FILE* loadFile(char* c);
//Fonction supprimant les balises
void traitement_balise(char* c);
//Fonction permettant de supprimer la ponctuation
void traitement_ponctuation(char* ch);
//Fonction permettant de garder que les mots de taille 3 ou plus
void traitement_taille(char** tab_mots, int nb_mots,int min_len); 
//Fonction permettant de retourner un fichier sans les éléments non importants
void processFile(FILE* f,int taille_min_mot);
//fonction existe permet de verifier si le mots ch existe dans le tableau de terme t avec y la taille du tableau de terme t(pour connaitre la fin du parcours de la boucle for
//existe retour -1 si le mots n'existe pas sinon elle retourne l'indice de la premiere occurance de ce mots comme ça on a la position de ce mots dans le tableau de terme il suffit juste d'acceder à cette case et ajouté +1 dans le nombre d'occurance de ce mots
int existe(char *ch,terme *t,int y);
//trier le tableau de terme aprés son remplissage par tous les mots
//le tri se fait selon le nombre d'occurance de la plus grande valeur à la plus petite
void trie_selon_occ(terme* t,int taille_tab_de_mots);
//initalisation de la pile
PILE init_PILE();
//fonction pour afficher la pile de descripteurs
void affiche_p(PILE p);
//retourne la taille de la pile pour connaitre l'identifiant de chaque cellule descripteur
// en gros, l'identifiant c'est la taille +1
int taille_pile(PILE p);
//creation de la cellule descripteur pour le fichier f et l'empiler à la pile p
PILE base_descripteur(FILE* f,PILE p, int VALEUR_LIMITE,int TAILLE_MAX);
//sauvegarde la pile dans un fichier
void sauvegardePile(PILE p, char *path);
//Empile un descripteur dans la pile
PILE empiler_pile(PILE p ,mots* tab,int indice);
//charger la pile a partir du fichier base_descripteur_texte
PILE chargerPile(PILE p,char *path);
/* ************************************ TABLE INDEXATION *****************************************************/
TABLE init_table();
int existe_mots_dans_table(char*s, TABLE ta);
//empiler un mot dans la table d'indexation
TABLE empiler_un_mots_dans_table(PILE p,TABLE ta,char* s);
//creation de la table d'indexation à partir de la pile de descripteur
TABLE creation_table(PILE p);
//trie de la table d'indexation
TABLE tri_TABLE(TABLE t,PILE p);
//verifie si le fichier de chemin ch est dejà indexé ou pas
int indexation_deja_faite(char* ch);
//fonction pour l'indexation d un fichier texte
int indexation_texte(char* ch);
//fonction pour parametre le fichier de configuration
void modifier_fic_config(int VALEUR_LIMITE,int TAILLE_MAX,int TAILLE_MIN_MOT,int NB_BITS_IMAGE);
//recherche par mot cle
SORTED_LIST recherche_par_mot_cle(char *mot_cle);
//suie de la fonction rechercher par similarite
//on a tous les mots du fichier saisie on comparer chaque cellule et on cacul combien y a t il de mots en commun
void affiche_fichier_similaire(mots* tab_de_mot,PILE p,int idd);
//retourne 1 si le mot ch existe dans le tab_de_mot
int existe_dans_tab_de_terme(char* ch, mots* tab_de_mot,int x);
//fonction pour la recherche par similarit� � partir du fichier dont son chemin est chemin_fichier
SORTED_LIST recherche_par_similarite(char* chemin_fichier);
//retourne la similarite entre deux terme ( un terme est un mots avec sn nombre d'occurence dans le fichier
int get_similarite_termes(terme terme1, terme terme2);
//retourne 1 si dans le cellule cel on trouve le mot
int cellule_contient_mot(char* mot, cellule cel);
//retourne le pourcentage de similarite entre les deux fichiers dont leurs identifiant est id1 et id2
float get_similarite_percent(PILE p,int id1, int id2);
//retourne le nombre de mots qui se trouve dans une cellule de descripteur cel
int get_nb_mots(cellule cel);
// fonction qui permet de choisir dans quel dossier @path tous les fichiers utilises vont se trouver
// cette fonction permet la partie Image et la partie texte de ne pas partager les memes fichiers de config 
// ou descripteurs
void _init_chemin(char* path);
//retourne le chemin du fichier a partir de son identifiant
char* get_path_from_id(int id);
