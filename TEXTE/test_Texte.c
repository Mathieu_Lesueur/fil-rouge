#include <stdio.h>
#include <string.h>
#include  <stdlib.h>

#include "../_OUTILS/affichage/outils_affichage.h"
#include "Texte.h"

#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */

#define RESET "\x1B[0m"



/*
	Hamza Guilhem  

*/



//saisie de la varible x, cette fonction va etre appeler a chaque saisie d 'une variable de type int
void continuer()
{
		int x;
		printf("\n Tappez "BOLDGREEN" 0  "RESET"pour continuer \n");
		fflush(stdout);
		fflush(stdin);
		scanf("%d",&x);

}




//programme de test
int main(){
	int x=0;
	
	while(x!=-1)
	{
		/* dans cette partie il y'a la configuration
		l indexation et la recherche des fichiers textes
		*/
		char *options[5] = {"Editer fichier configuration", "Indexer", "Rechercher","TESTS_UGO","QUITTER"};
		// Titre , Nombre de # ,tableau d'options, taille du tableau 
		afficher_options("FICHIERS TEXTE","Les options sont :",50,options,5);
		
		
		printf("(tappez ici)-> ");
		scanf("%d",&x);

		//si x=0 alors c est la partie de configuration
		if(x == 0)
		{
			
			//les valeurs qui vont etre stocker dans le fichier configuration
			int VALEUR_LIMITE,TAILLE_MAX,TAILLE_MIN_MOT;

			//saisie de la valeur limite
			do
			{
				afficher_options("CONFIGURATION","Saisir la valeur limite  ",50,NULL,3);
				printf("(tappez ici)-> ");
				scanf("%d",&VALEUR_LIMITE);
				if(VALEUR_LIMITE <= 0)
				{
					afficher_options("ERREUR","La valeur limite doit etre ]0;+inf[ ",50,NULL,3);
					continuer();
				}
			}while(VALEUR_LIMITE <= 0);


			//saisie de la taille maximale d un mot pour qu'il soit pris en compte dans l indexation
			do
			{
				afficher_options("CONFIGURATION","Saisir la taille maximum  ",50,NULL,3);
				printf("(tappez ici)-> ");
				scanf("%d",&TAILLE_MAX);
				if(TAILLE_MAX <= 0)
				{
					afficher_options("ERREUR","La valeur limite doit etre ]0;+inf[ ",50,NULL,3);
					continuer();
				}
			}while(TAILLE_MAX <= 0);


			//saisie de la taille minimale d'un mot
			do
			{
				afficher_options("CONFIGURATION","Saisir la taille minimale  ",50,NULL,3);
				printf("(tappez ici)-> ");
				scanf("%d",&TAILLE_MIN_MOT);
				if(TAILLE_MIN_MOT <= 0)
				{
					afficher_options("ERREUR","La valeur limite doit etre ]0;+inf[ ",50,NULL,3);
					continuer();

				}
			}while(TAILLE_MIN_MOT <= 0);


			//stocker ces valeurs dans le fichier configuration
			modifier_fic_config(VALEUR_LIMITE,TAILLE_MAX,TAILLE_MIN_MOT);

		// si x)1 alors c'est la partie d indexation
		}else if(x ==1)
		{
			char *options2[4] = {"Indexer 1 fichier", "Indexer plusieurs fichiers","Indexer un repertoire","RETOUR"};
			afficher_options("INDEXER FICHIERS TEXTE","Les options sont :",50,options2,4);

			int y=0;
			printf("(tappez ici)-> ");
			scanf("%d",&y);
			
			//si y=0 alors indexaer un seul fichier texte
			if(y == 0)
			{

				afficher_options("INDEXER 1 FICHIERS TEXTE","Merci de saisir le chemin du fichier ",50,NULL,3);
				
				char chemin[200];
				printf("(tappez ici)-> ");
				scanf("%s",chemin);
				int resultat = indexation_texte(chemin);
				
				
				//dans resultat : 0 si le fichier n est pas dej� indexe, 2 si le fichier est deja indexe sinon n'importe quel valeur retourn�
				if(resultat ==2 )
				{
					printf(BOLDGREEN"\n Fichier deja indexé \n "RESET);
					fflush(stdout);
				}
				else if(resultat ==0 )
				{
					printf(BOLDGREEN"\n  OK \n "RESET);
					fflush(stdout);

				// y!=0 et y!=2 donc vous etes tromp� dans le chemin
				}else 
				{

					printf(BOLDRED"\n [ERREUR] N°"RESET"\n ");
					fflush(stdout);
					printf("%d",resultat);
					fflush(stdout);

				}

		
			}
			//si y=1 alors il s'agit d'indexer une liste des fichiers textes
			else if(y == 1)
			{


					afficher_options("INDEXER PLUSIEURS FICHIERS TEXTE","Combien de fichiers voulez vous indexer ? ",50,NULL,3);
					
					int y=0;
					printf("(tappez ici)-> ");
					scanf("%d",&y);


					for (int i = 0; i < y; i++)
					{
						char number[12];
						sprintf(number, "%d", i+1);
						char total_number[12];
						sprintf(total_number, "%d", y);

						char total_string[200];

						strcpy(total_string,"Fichier [");
						strcat(total_string,number);
						strcat(total_string,"/");
						strcat(total_string,total_number);
						strcat(total_string,"]");

						afficher_options(total_string,"Merci de saisir le chemin du fichier ",50,NULL,3);
						char chemin[200];
						printf("(tappez ici)-> ");
						scanf("%s",chemin);

						int resultat = indexation_texte(chemin);
										
						if(resultat ==2 )
						{
							printf(BOLDGREEN"\n  Fichier deja indexé \n "RESET);
							fflush(stdout);

						}
						else if(resultat ==0 )
						{
							printf(BOLDGREEN"\n  OK \n "RESET);
							fflush(stdout);
						}
						else 
						{

							printf(BOLDRED"\n [ERREUR] N°"RESET"\n ");
							fflush(stdout);
							printf("%d",resultat);
							fflush(stdout);

						}

						if(i <y-1)
						{
							continuer();
						}


					}
					



			}
			//si y=2 alors il s'agit d'indexer tout un repertoire
			else if(y==2)
			{			
				afficher_options("INDEXER TOUT UN DOSSIER","Merci de saisir le chemin du dossier ",50,NULL,3);
				char chemin[200];
				printf("(tappez ici)-> ");
				scanf("%s",chemin);

				indexation_repertoire(chemin);
				
			}
			


		}
		//si x=2 alors c'est la partie de la recherche
		else if(x == 2)
		{
			char *options2[3] = {"Recherche par mots cle", "Recherche par similarite","RETOUR"};
			afficher_options("RECHERCHER FICHIERS TEXTE","Les options sont :",50,options2,3);
			
			int y=0;
			printf("(tappez ici)-> ");
			scanf("%d",&y);
			if(y == 0)
			//si y=0 alors c'est la recherche par mot cle
			{			
				afficher_options("RECHERCHER MOT CLE","Merci de saisir le mot cle ",50,NULL,3);
				char chemin[200];
				printf("(tappez ici)-> ");
				scanf("%s",chemin);

				recherche_par_mot_cle(chemin);
				
				//si y=1 alors c'est la recherche par similarit�
			}else if(y == 1)
			{

				afficher_options("RECHERCHER SIMILARITE","Merci de saisir le chemin du fichier à comparer ",50,NULL,3);
				char chemin[200];
				printf("(tappez ici)-> ");
				

				scanf("%s",chemin);
				recherche_par_similarite(chemin);
			}

		}
		else if(x == 3)
		{
				afficher_options("TESTS UGO","Merci de saisir le chemin du fichier à comparer ",50,NULL,3);
				char chemin[200];
				printf("(tappez ici)-> ");
				

				scanf("%s",chemin);
				recherche_par_similarite(chemin);


		}else
		{
			return(0);

		}


		continuer();


	}

	return(0);

}
