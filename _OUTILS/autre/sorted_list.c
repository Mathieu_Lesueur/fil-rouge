#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sorted_list.h"


/**
 *[UGO ROUX]
*
*   Liste chainee un peu speciale, elle empile les elements de maniere a les trier dans l'ordre decroissant 
*
*/


/**
 * Constructeur de la liste
 *[UGO ROUX]
 */
SORTED_LIST init_FILE()
{
    return NULL;
}


/**
 * retourne la taille de la liste
 * [UGO ROUX]
 */
int taille_FILE(SORTED_LIST mapile)
{
    int taille = 0;
    while(mapile!=NULL)
    {   
        mapile = (*mapile).suivant;
        taille ++;
    }
    return taille;

}

/**
 * teste si l'element existe deja dans la liste 
 * renvoie 1 si oui 00 si non
 * [UGO ROUX]
 */
int existe_ds_liste(SORTED_LIST head, ELEMENT element)
{
    while(head!=NULL)
    {   
        if( strcmp(element.path,(*head).element.path)==0 )
        {
            return 1;
        }
        head =  head->suivant;

    }
    return 0;

}


/**
 * Affiche la pille 
 * [UGO ROUX]
 */
void affiche_FILE(SORTED_LIST mapile)
{
    printf("\n------------------------------ \n Pile \n [");

    while(mapile!=NULL)
    {   

        printf("\n  > %f \n",/*(*mapile).element.path,*/(*mapile).element.number);
        mapile = (*mapile).suivant;

    }
    printf("]\n------------------------------ \n");

}

/**
 * check si la liste est vide
 * [UGO ROUX]
 */
int FILE_estVide(SORTED_LIST mapile)
{   
    return mapile == NULL;
}

/**
 * Emplie de facon a garder l'ordre dans la liste
  * [UGO ROUX]
*/
SORTED_LIST emPILE(SORTED_LIST head, ELEMENT element)
{

    //si l'element est deja dans la liste 
    
    if(existe_ds_liste(head,element))
    {
        return head;
    }
    


    CELLULE *temp, *prev, *next;
    temp = (CELLULE*)malloc(sizeof(CELLULE));
    temp->element = element;
    temp->suivant = NULL;
    if(!head)
    {
        head=temp;
    } 
    else
    {
        prev = NULL;
        next = head;
        while(next && next->element.number<=element.number)
        {
            prev = next;
            next = next->suivant;
        }
        if(!next)
        {
            prev->suivant = temp;
        }
        else
        {
            if(prev)
            {
                temp->suivant = prev->suivant;
                prev-> suivant = temp;
            }
            else 
            {
                temp->suivant = head;
                head = temp;
            }            
        }   
    }
    return head;
}


/**
 * Depile 
  * [UGO ROUX]
*/
SORTED_LIST dePILE(SORTED_LIST mapile,ELEMENT* element)
{
    if(mapile != NULL)
    {
        *element = mapile->element;
        mapile =  mapile->suivant;
    }
    return mapile;
}



/**
 * Retourne l'element a l'index pindex
  * [UGO ROUX]
*/
ELEMENT get(SORTED_LIST mapile, int pindex)
{
    int index = 0;
    ELEMENT element_returned;

    while(mapile!=NULL)
    {   
        if(index == pindex)
        {
            element_returned = (*mapile).element;
        }
        index++;
        mapile =  mapile->suivant;

    }
    return element_returned;
} 




void getAsArrays(int* pNumVals,float** floatArray,char*** stringArray, SORTED_LIST mapile)
{
    int listSize = 0;

    listSize = taille_FILE(mapile);
    *pNumVals = listSize;
    //init of the arrays 
	*floatArray = (float*)malloc(sizeof(float) * listSize);
	memset(*floatArray, 0, sizeof(float) * listSize);

    //init of the arrays
	*stringArray = (char**)malloc(sizeof(char*) * listSize);
	memset(*stringArray, 0, sizeof(char*) * listSize);

    //filling the arrays
    int i = 0;
    while(mapile!=NULL)
    {   

        (*floatArray)[i] = (*mapile).element.number;
        
        (*stringArray)[i] = (char*)malloc(sizeof(char) * 1024);
        
	    strcpy((*stringArray)[i], (*mapile).element.path);

        mapile = (*mapile).suivant;

        i++;
    }

}


void clean_memory(char** ppszVals,float* pVals,SORTED_LIST mapile)
{

    free(pVals);

    int numVals = taille_FILE(mapile);

	for (int i=0; i<numVals; i++)
	{
		free(ppszVals[i]);
	}
	free(ppszVals);
}