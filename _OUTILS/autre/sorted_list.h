#ifndef SOME_HEADER_GUARD_WITH_UNIQUE_NAME_2
#define SOME_HEADER_GUARD_WITH_UNIQUE_NAME_2

/********************************************SORTED LIST RECHERCHE***************************************************/
#include "element.h"

typedef struct _CELLULE{

    ELEMENT element;
	struct _CELLULE* suivant;

}CELLULE;

typedef CELLULE* SORTED_LIST;


SORTED_LIST init_FILE();

int taille_FILE(SORTED_LIST mapile);

void affiche_FILE(SORTED_LIST);

int FILE_estVide(SORTED_LIST );

int existe_ds_liste(SORTED_LIST head, ELEMENT element);

SORTED_LIST emPILE(SORTED_LIST , ELEMENT );

SORTED_LIST dePILE(SORTED_LIST ,ELEMENT* );

ELEMENT get(SORTED_LIST mapile, int pindex);

void getAsArrays(int* pNumVals,float** floatArray,char*** stringArray, SORTED_LIST mapile);

void clean_memory(char** ppszVals,float* pVals,SORTED_LIST mapile);

#endif