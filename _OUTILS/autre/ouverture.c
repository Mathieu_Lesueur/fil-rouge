#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "ouverture.h"

/*
    Ouverture des fichiers dans tous les editeurs
    Guilhem

*/

void ouvertureTexte(char* path){
    char* arg[] = {path, NULL};
    if (execlp("gedit", "gedit", path, NULL) == -1) {
        perror("error gedit");
        exit(1);
    }
 }

void ouvertureImage(char* path){
    char* arg[] = {path, NULL};
    if (execlp("eog", "eog", path, NULL) == -1) {
        perror("error eog");
        exit(1);
    }
}

void ouvertureAudio(char* path,int time){
    char* arg[] = {path, NULL};
    char commande[256];
    char t[3];
    sprintf(t,"%d",time);
    strcpy(commande,"play ");
    strcat(commande,path);
    strcat(commande, " trim ");
    strcat(commande, t);
    system(commande);
}
