#include <stdio.h>
#include <stdlib.h>

#include "maths_tools.h"

/*

Liste d'outils mayhs customs pour Ugo par Ugo

*/





/**
 * retourne @x puissance @n
 * [UGO ROUX]
 */
int power(int x,int n)
{
    if(n == 0)
    {
        return 1;
    }
    if(n == 1)
    {
        return x;
    }

    int result =x;

    for(int i =0; i<n -1; i++)
    {
        result=x*result;
    }

    return result;
}

/**
 * Convertit un tableau de bits retourne par getbitarray() @bitarray de taille  @bitarraysize bits
 * en int
 * 
 * le bit de poid fort doit se trouver à 0
 * 
 *[UGO ROUX]
 */
int bit_to_int(short* bitarray, short bitarraysize)
{
    int returned_int = 0;

    for(int i = 1; i<=bitarraysize; i++)
    {
        returned_int = returned_int + power(2, bitarraysize - i ) *bitarray[i-1];
    }

    return returned_int;
}




/**
 * retourne un tableau de taille @nbBits qui contient le nombre @number mis sous forme binaire
 * [UGO ROUX]
 */
short* getbitarray(short nbBits,short number)
{
    short* returnedArray = (short*)malloc(nbBits*sizeof(short));

    if(number > 0)
    {
    
        for(int i = 1; i<=nbBits ; i++)
        {
            returnedArray[nbBits-i] = number%2;
            number = number/2;
        }
    }
    return returnedArray;
}
