#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interfaceJava.h"


#include "../../_OUTILS/autre/element.h"
#include "../../_OUTILS/autre/sorted_list.h"

#include "../../TEXTE/Texte.h"
#include "../../IMAGE/images.h"
#include "../../SON/indexion.h"


char NOM_FICHIERS_REPERTOIRE_PATH[255];





//------------------------------------------------ RECHERCHE


void recherche_texte(char* mot,int* pNumVals,float** floatArray,char*** stringArray)
{
	
    _init_chemin("../../TEXTE");
    SORTED_LIST liste = recherche_par_mot_cle(mot);
    getAsArrays(pNumVals,floatArray,stringArray, liste);
}




void recherche_image(int r,int g,int b,int* pNumVals,float** floatArray,char*** stringArray)
{


    _init_chemin("../../IMAGE");
    short converted_value = convertRGB(r,g,b,2);
    char mot[12];
    sprintf(mot, "%d", converted_value);

    SORTED_LIST liste = recherche_par_mot_cle(mot);
    getAsArrays(pNumVals,floatArray,stringArray, liste);

}

void recherche_son(char* path,int* pNumVals,float** floatArray,char*** stringArray)
{

    init_chemin("../../");

    SORTED_LIST liste = comparaison(path, 0);
    getAsArrays(pNumVals,floatArray,stringArray, liste);

}


//------------------------------------------------- COMPARAISON
void comparaison_texte(char* path,int* pNumVals,float** floatArray,char*** stringArray)
{
    _init_chemin("../../TEXTE");
    SORTED_LIST liste = recherche_par_similarite(path);
    getAsArrays(pNumVals,floatArray,stringArray, liste);

}


void comparaison_image(char* path,int* pNumVals,float** floatArray,char*** stringArray)
{

    _init_chemin("../../IMAGE");

    char nouveau_path[1000];

    strcpy(nouveau_path,path);
    strcat(nouveau_path,"_converted");
    printf("convertion \n");

    convertir_image(path,nouveau_path,2);
    

    SORTED_LIST liste = recherche_par_similarite(nouveau_path);


    remove(nouveau_path);


    getAsArrays(pNumVals,floatArray,stringArray, liste);

}

void comparaison_son(char* path,int* pNumVals,float** floatArray,char*** stringArray)
{
    init_chemin("../../");

    SORTED_LIST liste = comparaison(path, 1);
    getAsArrays(pNumVals,floatArray,stringArray, liste);

}


//-------------------------------------------INDEXER---------------------------------------

void indexer_repertoire_texte(char* chemin_repertoire)
{
    strcpy(NOM_FICHIERS_REPERTOIRE_PATH,".");
	strcat(NOM_FICHIERS_REPERTOIRE_PATH,"/nom_fichiers_du_repertoire_Textes.txt");

    _init_chemin("../../TEXTE");

    // On recupere le nombre de fichiers qui correspondent 
	char commande[1000];
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande," | grep -v / ");
	strcat(commande," | wc -l > ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH);
	system(commande);

    // Puis on recupere leurs chemins
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande," | grep -v / ");
	strcat(commande,">> ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH);
	system(commande);

    //on ouvre le fichier on l'on a stocker tout ca
	FILE* f=fopen(NOM_FICHIERS_REPERTOIRE_PATH,"r");


	if(f != NULL)
	{
		char c[255];
		char chemin_fichier[255];	

		int nb_fichiers_indexation;
		fscanf(f,"%d",&nb_fichiers_indexation);// on recup le nombre de lignes

		int index = 0;

		while(fscanf(f,"%s",c)==1)//lecture du fichier lignes par lignes
		{
			index++;

            //recuperation du chemin 
			strcpy(chemin_fichier,chemin_repertoire);
			strcat(chemin_fichier,"/");
			strcat(chemin_fichier,c);

            //indexation
            int resultat = indexation_texte(chemin_fichier);

            //traitement du resultat de l'indexation
			if(resultat != 0 && resultat != 2)
			{
				printf("\033[1m\033[31m %s","echec indexation du fichier : \n ");
				printf(" \x1B[0m %s \n",chemin_fichier);
			}
			/*
            else if(resultat == 2)
            {
                printf("\n   \033[1m\033[34m %s","indexation deja faite : ");
				printf("\x1B[0m %s  \n",chemin_fichier);

            }
			*/
		}

        fclose(f);
        remove(NOM_FICHIERS_REPERTOIRE_PATH);
	
	}

}


void indexer_repertoire_image(char* chemin_repertoire)
{
    _init_chemin("../../IMAGE");


    strcpy(NOM_FICHIERS_REPERTOIRE_PATH,".");
	strcat(NOM_FICHIERS_REPERTOIRE_PATH,"/nom_fichiers_du_repertoire_image.txt");

    // On recupere le nombre de fichiers qui correspondent 
	char commande[1000];
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande,"/*.txt");
	strcat(commande," | wc -l > ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH); // on save ca dans le fichier NOM_FICHIERS_REPERTOIRE_PATH
	system(commande);

    // Puis on recupere leurs chemins
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande,"/*.txt");
	strcat(commande,">> ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH); // on save ca a la suite 
	system(commande);

    //on ouvre le fichier on l'on a stocker tout ca
	FILE* f=fopen(NOM_FICHIERS_REPERTOIRE_PATH,"r");

	if(f != NULL)
	{

		char c[255];
		char chemin_fichier[255];	

		int nb_fichiers_indexation; // on recup le nombre de lignes
		fscanf(f,"%d",&nb_fichiers_indexation);


		int index = 0;
		while(fscanf(f,"%s",c)==1)//lecture du fichier lignes par lignes
		{
            
			index++;

            //on creer un nouveau chemin pour y stocker l'image convertie et ne pas perdre l'originale
			strcpy(chemin_fichier,c);
            char* nouveau_path = malloc(sizeof(char)*250);
            strcpy(nouveau_path,chemin_fichier);
            strcat(nouveau_path,"_converted");

            //on conviertie l'image 
            convertir_image(chemin_fichier,nouveau_path,2);


            //ensuite on indexe l'image convertie
            int resultat = indexation_texte(nouveau_path);


            //et on remove le fichier maintenant useless
            remove(nouveau_path); 

            // le resultat de l'indexation est recupere et traité
			if(resultat != 0 && resultat != 2)
			{
				printf("\033[1m\033[31m %s","echec indexation du fichier : \n ");
				printf(" \x1B[0m %s \n",chemin_fichier);
				//return(1);
			}
			/*
            else if(resultat == 2)
            {
                printf("   \033[1m\033[34m %s","indexation deja faite : ");
				printf("\x1B[0m %s  \n",chemin_fichier);

            }
			*/


		}

        fclose(f);
        remove(NOM_FICHIERS_REPERTOIRE_PATH);

    }
}


void indexer_repertoire_son(char* chemin_repertoire)
{
    strcpy(NOM_FICHIERS_REPERTOIRE_PATH,".");
	strcat(NOM_FICHIERS_REPERTOIRE_PATH,"/nom_fichiers_du_repertoire_son.txt");

    init_chemin("../../");



    // On recupere le nombre de fichiers qui correspondent 
	char commande[1000];
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande,"/*.bin");
	strcat(commande," | wc -l > ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH); // on save ca dans le fichier NOM_FICHIERS_REPERTOIRE_PATH
	system(commande);

    // Puis on recupere leurs chemins
	strcpy(commande,"ls -p ");
	strcat(commande,chemin_repertoire);
	strcat(commande,"/*.bin");
	strcat(commande,">> ");
	strcat(commande,NOM_FICHIERS_REPERTOIRE_PATH); // on save ca a la suite 
	system(commande);


    //exit(0);

    //on ouvre le fichier où l'on a stocké tout ca
	FILE* f=fopen(NOM_FICHIERS_REPERTOIRE_PATH,"r");


	if(f != NULL)
	{

		char c[255];
		char chemin_fichier[255];	

		int nb_fichiers_indexation;
		fscanf(f,"%d",&nb_fichiers_indexation);// on recupère le nombre de lignes

		int index = 0;

		while(fscanf(f,"%s",c)==1)//lecture du fichier lignes par lignes
		{
			index++;


            //recuperation du chemin 

            //indexation
            int resultat = descripteur(c);

            //traitement du resultat de l'indexation
			if(resultat != 0 && resultat != 2)
			{
				printf("\033[1m\033[31m %s","echec indexation du fichier : \n ");
				printf(" \x1B[0m %s \n",chemin_fichier);
			}
			/*
            else if(resultat == 2)
            {
                printf("\n   \033[1m\033[34m %s","indexation deja faite : ");
				printf("\x1B[0m %s  \n",chemin_fichier);

            }
			*/
		}

        fclose(f);
        remove(NOM_FICHIERS_REPERTOIRE_PATH);
	
	}
}
