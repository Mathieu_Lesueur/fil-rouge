
//---------------------------------------------RECHERCHE-----------------------------------

void recherche_texte(char* mot,int* pNumVals,float** floatArray,char*** stringArray);
void recherche_image(int r,int g,int b,int* pNumVals,float** floatArray,char*** stringArray);
void recherche_son(char* path,int* pNumVals,float** floatArray,char*** stringArray);

//---------------------------------------------COMPARAISON---------------------------------

void comparaison_texte(char* path,int* pNumVals,float** floatArray,char*** stringArray);
void comparaison_image(char* path,int* pNumVals,float** floatArray,char*** stringArray);
void comparaison_son(char* path,int* pNumVals,float** floatArray,char*** stringArray);

//---------------------------------------------INDEXER---------------------------------------

void indexer_repertoire_texte(char* chemin_repertoire);
void indexer_repertoire_image(char* chemin_repertoire);
void indexer_repertoire_son(char* chemin_repertoire);