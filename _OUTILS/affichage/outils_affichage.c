/**
 * @file outils_affichage.c
 * @author Ngoma-Mby Charvy (charvy.ngoma-mby@univ-tlse3.fr)
 * @brief  
 * @version 0.1
 * @date 31/01/2019
 * 
 * @copyright Copyright (c) 2019
 * Description : contient les outils pour l'affichage du cadre et des effets de l'interface utilisateur
 */
#include <stdio.h>
#include <string.h>
#include  <stdlib.h>


//codes pour les couleurs
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"

#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

#define RESET "\x1B[0m"

#include "outils_affichage.h"



/**
 * [Charvy]
 * Paramètres :  titre, texte, large_fen, tableau_options, taille_tableau 
 * Description : Affiche une fenêtre avec le titre 'titre', du texte 'texte', de largeur "larg_fen" et contenant toutes les options possibles dans tableau_options 
 * taille_tableau est la taille du tableau : tableau_options
*/ 
void afficher_options(char* titre,char* texte,int larg_fen ,char** tableau_options,int taille_tableau)
{

        //"Nettoie" la console
        system("clear");

        //le cadre de la fenêtre sera fait de hashtags sur la largeur
        int nbhashtabgs = larg_fen;

        // Gère le cas où le texte pour une raison ou une autre à cause d'un affichage donné pour une des fonctions du moteur de recherche dépasse les limites fixées de la fenêtre
        if(strlen(texte)>larg_fen)
        {
            nbhashtabgs = strlen(texte)+larg_fen/2;
        }

        for(int i=0; i< nbhashtabgs-strlen(titre) ; i++)
        {
            if(i == (nbhashtabgs-strlen(titre))/2)
            {
                char chars[100];
                strcpy(chars,BOLDRED);	
                strcat(chars," ");
                strcat(chars,titre);
                strcat(chars," ");	
                strcat(chars,RESET);	
                printf("%s",chars);
            }else
            {
                printf("#");

            }
            

        }

        printf("\n"); 

        // une ligne vide 
        printf("|"); 
        for(int i=0; i<nbhashtabgs-1; i++)
        {
            printf(" "); 
        }
        printf("|\n"); 

        // affichage de texte
        printf("|");
        printf("%s",texte); 

        for(int i=0; i< (nbhashtabgs-1-strlen(texte)) ; i++)
        {
            printf(" ");
        }

        printf("|\n"); 



        // une ligne vide
        printf("|"); 
        for(int i=0; i<nbhashtabgs-1; i++)
        {
            printf(" "); 
        }
        printf("|\n"); 


        if(tableau_options != NULL) // prévient le cas limite d'un tableau d'option "null" pour plus de sécurité
        {
            for(int i =0; i<taille_tableau; i++)
            {
                char chars[2000];
                strcpy(chars,"|  ");

                strcat(chars,BOLDBLUE);	
                strcat(chars," [");	

                char str[12];
                sprintf(str, "%d", i);
                strcat(chars,str);	

                strcat(chars,"] ");	
                strcat(chars,RESET);

                strcat(chars,tableau_options[i]);	

                printf("%s",chars);
                
                for(int i=0; i< (nbhashtabgs-strlen(chars) + strlen(BOLDBLUE)+ strlen(RESET)) ; i++)
                {
                    printf(" ");
                }
                printf("|\n"); 

            }
        }


        // ligne vide 
        printf("|"); 
        for(int i=0; i<nbhashtabgs-1; i++)
        {
            printf(" "); 
        }
        printf("|\n"); 

        // ligne vide 
        for(int i=0; i<nbhashtabgs+1; i++)
        {
            printf("#"); 
        }
        printf("\n"); 


}


/**
 * [Charvy]
 * Paramètres :  progression, taille_max, nbChar_barre 
 * Description : Affiche une barre de progression sympa qui change de couleur selon la progression d'une tâche du moteur de recherche (eg : indexation) 
 * taille_max : si nous devons indexer 100 fichiers, 100 correspondrait à la taille maximum taille_max pour cette tâche 
*/ 
void afficher_barre_progression(int progression,int taille_max,int nbChar_barre)
{
    
    //"Nettoie" console
    //system("clear");




    if( !(progression > taille_max))
    {
        
    


        float ratio_taille = (float)nbChar_barre/(float)taille_max; // Garde la taille totale de la barre de progression constante quelque soit la taille maximum (100% de la tâche à effectuer)

        float percent_progression =  ( (float)progression / (float)taille_max ); // pourcentage de progression de la tâche

        int taille_barre =  (int)  ((float)ratio_taille*progression); // calcule la taille de la partie de la barre de progressio reflétant la progression de la tâche relativement à la taille limite de la barre (nbChar_barre)


    //printf("\nPercent progression = %f taille barre = %d / %d char \n",percent_progression,taille_barre , taille_max*ratio_taille);

    // Varie les couleurs de la portion de la barre reflétant la progression en fonction de cette progression
    char* chaine = (char*)malloc(255*sizeof(char));
    strcpy(chaine,"\0");

    if(percent_progression<0.1)
    {
        strcat(chaine,BOLDMAGENTA);
    }
    else if(percent_progression<0.25)
    {
        strcat(chaine,BOLDRED);

    }
    else if(percent_progression<=0.50)
    {
        strcat(chaine,BOLDYELLOW);
    }
    else if(percent_progression<0.80 && percent_progression>0.50)
    {
        strcat(chaine,BOLDGREEN);
    }

    else if(percent_progression>0.80)
    {
        strcat(chaine,BOLDBLUE);
    }


    for(int i=0; i<taille_barre ;i++)
    {
        strcat(chaine,"#" );

    }

    strcat(chaine,RESET);


    for(int i= taille_barre ; i<taille_max*ratio_taille ;i++)
    {
        strcat(chaine,"-" );
    }


    char* Chaine_finale = (char*)malloc(255*sizeof(char));

    char string_progression[12];
    sprintf(string_progression, "%d", progression);
    char string_taille_max[12];
    sprintf(string_taille_max, "%d", taille_max);


    strcpy(Chaine_finale," (");
    strcat(Chaine_finale,string_progression);
    strcat(Chaine_finale,"/");
    strcat(Chaine_finale,string_taille_max);
    strcat(Chaine_finale,")\t");




    strcat(Chaine_finale,chaine);


    printf("\n [ %s ] ",Chaine_finale);



    }
}